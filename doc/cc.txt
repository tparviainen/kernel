
RULEZ OF THUMB
--------------

1. Keep it simple
- Something old
- Something new
    - Think first (at least one week) and study different options before
      taking the next step to implement it!


2. How to comment code?
- Description:
    - Use doxygen tags
    - What the functions does
    - Describe parameters
- Implementation:
    - Why thing are done!
    - From the code you can see what it does i.e. do not comment blocks which
      are self-explanatory!
