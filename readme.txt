Please note that there is no official readme file for this project!

You can browse source tree (from the navigation bar on the left) and it has all the needed files.
The doc-folder contains a PDF/ODP file which has a brief overview about the project.

Q: How to compile / clean own project + intermediate files
A: Go to application specific folder, for example app/at91/template and use
   next commands to compile the kernel + own project.

make rebuild
make clean

The output folder (app/at91/<project>/img) contains flashable binary files (assuming you have needed tools installed).