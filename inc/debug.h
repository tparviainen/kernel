/**
\file
\brief Kernel debug definitions

\author Copyright (c) 2007-2009, Tomi Parviainen
*/

#ifndef _DEBUG_H_
#define _DEBUG_H_


#if defined DEBUG

/**
Asserts that the 'expr' is truth else prints trace and halts code execution
*/
#define ASSERT(expr)                            \
    if (!(expr))                                \
        {                                       \
        irq_disable_core();                     \
        trace_print_dec(__FILE__, __LINE__);    \
        idle();                                 \
        for (;;)                                \
            ; /* NOP */                         \
        }


/**
C99 introduced __func__ string, which contains the name of the current function
*/
#define TR_FUNCTION trace_print(__func__)

#define TR_MEM(a)   /**< Memory manager trace macro */
#define TR_SCH(a)   /**< Scheduler trace macro */
#define TR_TSK(a)   /**< Task trace macro */
#define TR_IRQ(a)   /**< Interrupt trace macro */
#define TR_TME(a)   /**< Timer trace macro */
#define TR_BUF(a)   /**< Buffer trace macro */
#define TR_MBX(a)   /**< Mailbox trace macro */
#define TR_SEM(a)   /**< Semaphore trace macro */
#define TR_SYS(a) a /**< System trace macro */
#define TR_GIO(a)   /**< GPIO trace macro */

#else /* DEBUG */

#define ASSERT(expr)
#define TR_FUNCTION

#define TR_MEM(a)   /**< Memory manager trace macro */
#define TR_SCH(a)   /**< Scheduler trace macro */
#define TR_TSK(a)   /**< Task trace macro */
#define TR_IRQ(a)   /**< Interrupt trace macro */
#define TR_TME(a)   /**< Timer trace macro */
#define TR_BUF(a)   /**< Buffer trace macro */
#define TR_MBX(a)   /**< Mailbox trace macro */
#define TR_SEM(a)   /**< Semaphore trace macro */
#define TR_SYS(a)   /**< System trace macro */
#define TR_GIO(a)   /**< GPIO trace macro */

#endif /* DEBUG */


#endif /* _DEBUG_H_ */
