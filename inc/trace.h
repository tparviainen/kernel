/**
\file
\brief Trace mechanism

This interface provides the functions to print different tracing
messages into trace port for example to track the kernel functionality.

\author Copyright (c) 2007-2008, Tomi Parviainen.
*/

#ifndef _TRACE_H_
#define _TRACE_H_


/*
Function prototype(s)
*/
extern void trace_init(void);
extern void trace_print(const sint8_t* str);
extern void trace_print_diff(void);
extern void trace_print_hex(const sint8_t* str, uint32_t value);
extern void trace_print_dec(const sint8_t* str, uint32_t value);
extern void trace_print_tcb(const tcb_t* task);
extern void trace_print_tcb_all(void);
extern void trace_print_tcb_list(const tcb_t* list);
extern void trace_print_char(const sint8_t chr);


#endif /* _TRACE_H_ */
