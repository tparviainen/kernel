/**
\file
\brief Time handling functions (eg. uptime and timers)

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _TIME_H_
#define _TIME_H_


/**
\brief Structure for time handling
*/
typedef struct {
    uint16_t ms;  /**< 0 - 999  */
    uint8_t sec;  /**< 0 - 59   */
    uint8_t min;  /**< 0 - 59   */
    uint8_t hour; /**< 0 - 23   */
    uint16_t day; /**< 0 - 2^16 */
} time_t;


/**
Function pointer prototype for timer callbacks. Timer callback function
is called in IRQ context.

\param param Parameter for timer callback
*/
typedef void (*timer_fn)(any_t* param);


/**
\brief Timer callback structure
*/
typedef struct timer {
    uint32_t milliseconds;  /**< Delay after the callback is called */
    struct timer* next;     /**< Pointer to next callback object */
    timer_fn func;          /**< Timer function pointer */
    any_t* param;           /**< Timer callback function parameter */
} timer_t;


/*
Function prototypes(s)
*/
extern void time_tick(void);
extern void time_uptime(time_t* uptime);

extern void timer_init(timer_t* timer, timer_fn func, any_t* param);
extern void timer_start(timer_t* timer, uint32_t milliseconds);


#endif /* _TIME_H_ */
