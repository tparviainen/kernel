/**
\file
\brief Task control block (TCB) definition

Task control block is a data structure, which contains the information
needed to manage different tasks in system. Each task in has own TCB.

\author Copyright (c) 2007-2008, Tomi Parviainen.
*/

#ifndef _TASK_CONTROL_BLOCK_H_
#define _TASK_CONTROL_BLOCK_H_


/**
\brief Task Control Block
*/
typedef struct tcb {
    stack_t* stk;        /**< Pointer to the stack of the task */
    const sint8_t* name; /**< Pointer to task identifier (task name) */
    struct tcb* next;    /**< Pointer to next task with same priority */
    struct tcb* prev;    /**< Pointer to previous task with same priority */
    uint32_t time_sleep; /**< The period of time task will be put to sleep */
    uint16_t time_slice; /**< The period of time task is allowed to run */
    uint8_t priority;    /**< Task priority 0 (lowest) - 255 (highest) */
    uint8_t state;       /**< Task state */

#ifdef PROF_SP
    uint8_t* stk_base;   /**< Pointer to the base of the stack */
    uint32_t stk_size;   /**< Size of the stack */
#endif /* PROF_SP */
} tcb_t;


#endif /* _TASK_CONTROL_BLOCK_H_ */
