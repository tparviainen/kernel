/**
\file
\brief Memory management implementation

\author Copyright (c) 2007-2009, Tomi Parviainen
*/

#ifndef _MEMORY_H_
#define _MEMORY_H_


/**
Symbol, which is located at the address where the free memory begins. This
needs to be initialised in linker script. Free memory begins after data and
bss sections.
*/
extern stack_t memory_free_beg;

/**
Symbol, which is located at the address where the free memory ends. This needs
to be initialised in linker script.
*/
extern stack_t memory_free_end;

/**
The base address of the interrupt mode stack
*/
extern stack_t __stack_irq__;

/**
The base address of the supervisor mode stack
*/
extern stack_t __stack_svc__;


/*
Function prototype(s)
*/
extern void memory_init(void);
extern any_t* memory_alloc(uint32_t length);
extern void memory_free(any_t* ptr);
extern void memory_set(any_t* dst, uint8_t chr, uint32_t length);
extern any_t* memory_cpy(any_t* dst, const any_t* src, uint32_t length);


#endif /* _MEMORY_H_ */
