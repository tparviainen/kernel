/**
\file
\brief Idle task implementation

This is the implementation for IDLE task, which is the lowest-priority
task in the system always ready to be executed. Idle task must not wait
any system resources instead it must always be ready to be executed.

\author Copyright (c) 2007-2008, Tomi Parviainen.
*/

#ifndef _TASK_IDLE_H_
#define _TASK_IDLE_H_


/*
Variable prototype(s)
*/
extern const uint8_t task_idle_priority;
extern const uint16_t task_idle_stk_size;
extern const sint8_t* const task_idle_name;


/*
Function prototype(s)
*/
extern void task_idle(any_t* param);
extern void idle(void);


#endif /* _TASK_IDLE_H_ */
