/**
\file
\brief Application specific interface

This file defines the interface what the application specific code must
implement.

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _APP_H_
#define _APP_H_


/*
Function prototypes(s)
*/

/**
Initialisation function for application specific code, after this function
is called the multitasking is started.
*/
extern void app_init(void);


#endif /* _APP_H_ */
