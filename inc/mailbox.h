/**
\file
\brief Mailbox for intertask communication

Mailbox provides an asynchronous inter task communication services meaning
that the sender and the receiver of message do not need to interact with the
message list at the same time. Message stored into list is stored until the
recipient retrieves it. Tasks waiting for messages are awaken in FIFO
order when messages arrive.

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _MAILBOX_H_
#define _MAILBOX_H_


/**
\brief Mailbox structure for intertask communication
*/
typedef struct {
    buffer_t* msg;  /**< Pointer to the messages in mailbox */
    tcb_t* list;    /**< Linked list of tasks waiting for messages to arrive */
} mailbox_t;


/*
Function prototypes(s)
*/
extern void mailbox_init(mailbox_t* mbx, uint32_t length);
extern void mailbox_post(mailbox_t* mbx, any_t* msg);
extern any_t* mailbox_pend(mailbox_t* mbx);


#endif /* _MAILBOX_H_ */
