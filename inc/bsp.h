/**
\file
\brief BSP interface

This file defines the interface what the BSP must implement. Functions
defined in this file are called during the kernel initialisation.

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _BSP_H_
#define _BSP_H_


/*
Function prototypes(s)
*/

/**
This function performs low level initialisation of HW, normally the low level
initialisation covers clock setup (including PLL), memory wait state and maybe
watchdog disabling code.
*/
extern void bsp_init_1(void);

/**
This function performs the second level initialisation of HW, now full
kernel services can be used. Normally the timer tick is initialised here.
*/
extern void bsp_init_2(void);

/**
This is the last step to initialise BSP related tasks and services, after
this function is executed, multitasking will be started and interrupts will
be enabled.
*/
extern void bsp_init_3(void);


#endif /* _BSP_H_ */
