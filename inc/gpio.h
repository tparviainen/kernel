/**
\file
\brief GPIO interface

General purpose input/output (GPIO) interface provides a means to handle
individual GPIO pins in a system. GPIO pins are the interface towards the
outside world.

\ref gpio_group

\author Copyright (c) 2009-2011, Tomi Parviainen
*/


#ifndef _GPIO_H_
#define _GPIO_H_


/** Configure the I/O pin to input */
#define GPIO_MODE_INPUT      0x0001

/** Configure the I/O pin to output */
#define GPIO_MODE_OUTPUT     0x0002

/** Enable the pull-up of the input pin */
#define GPIO_MODE_PULL_UP    0x0004

/** Enable the pull-down of the input pin */
#define GPIO_MODE_PULL_DOWN  0x0008


/**
Function pointer prototype for GPIO callbacks.

\param pin The I/O pin that has changed its status
*/
typedef void (*gpio_fn)(gpio_pin pin);


/*
Function prototypes(s)
*/
extern void gpio_init(void);
extern sint32_t gpio_irq_bind(gpio_pin pin, gpio_fn func);
extern void gpio_irq_unbind(gpio_pin pin);
extern void gpio_irq_enable(gpio_pin pin);
extern void gpio_irq_disable(gpio_pin pin);
extern void gpio_mode_set(gpio_pin pin, uint32_t mode);
extern uint32_t gpio_value_get(gpio_pin pin);
extern void gpio_value_set(gpio_pin pin, boolean_t value);


/**
\defgroup gpio_group Example usage: GPIO
\{

Next examples shows how to configure I/O pin to input and output and how to
enable interrupt generation on the I/O pin on state change.

\code
// Example 1: Pin PA1 configured to output and driven to 1
gpio_value_set(PA1, 1);
gpio_mode_set(PA1, GPIO_MODE_OUTPUT);

// Example 2: Pin PA2 configured to input with interrupt on change
gpio_irq_bind(PA2, &state_change_irq);
gpio_mode_set(PA2, GPIO_MODE_INPUT|GPIO_MODE_PULL_DOWN);
gpio_irq_enable(PA2);

// This function can be used also for other I/O pins
void state_change_irq(gpio_pin pin)
    {
    uint32_t state = gpio_value_get(pin);

    ASSERT(pin == PA2);

    if (state == 0)
        {
        // ...
        }
    else
        {
        // ...
        }

    }
\endcode
\}
*/

#endif /* _GPIO_H_ */
