/**
\file
\brief Profiler

\author Copyright (c) 2007-2009, Tomi Parviainen.
*/

#ifndef _PROFILER_H_
#define _PROFILER_H_

/** Profiler for CPU usage */
#define PROF_CPU_USAGE(a)

/** Profiler for stack pointer */
#define PROF_SP(a)

/** Profiler for program counter */
#define PROF_PC(a)


/*
Function prototype(s)
*/
extern void profiler_cpu_usage(void);
extern void profiler_stack_usage(void);


#endif /* _PROFILER_H_ */
