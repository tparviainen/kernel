/**
\file
\brief Abstracts the functionality of interrupts

Interrupt is an asynchronous signal from hardware indicating the need for
attention. This interface provides the functions needed to use interrupt
services in system.

\author Copyright (c) 2007-2008, Tomi Parviainen
\todo priority of the interrupts to be taken into account
*/

#ifndef _IRQ_H_
#define _IRQ_H_


/**
Function pointer prototype for interrupt callbacks.
*/
typedef void (*irq_fn)(void);


/*
Function prototypes(s)
*/
extern void irq_init(void);
extern irq_fn irq_bind(irq_source irq, irq_fn func);
extern void irq_unbind(irq_source irq);
extern void irq_enable(irq_source irq);
extern void irq_disable(irq_source irq);
extern void irq_handler(void);
extern void irq_enable_core(uint32_t old);
extern uint32_t irq_disable_core(void);
extern uint32_t irq_context(void);


#endif /* _IRQ_H_ */
