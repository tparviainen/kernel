/**
\file
\brief Semaphore for restricting access to shared resources

Semaphore provides a synchronization primitive for restricting access to
shared resources. Tasks are removed in FIFO order from the wait queue i.e.
the task that has been blocked the longest is released first, this
implementation is also known as a strong semaphore implementation.

\ref semaphore_group

\author Copyright (c) 2007-2009, Tomi Parviainen
*/

#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_


/**
\brief Semaphore structure for synchronization
*/
typedef struct {
    sint32_t count; /**< Semaphore count */
    tcb_t* list;    /**< Linked list of tasks waiting for semaphore */
} semaphore_t;


/*
Function prototypes(s)
*/
extern void semaphore_init(semaphore_t* s, sint32_t count);
extern void semaphore_wait(semaphore_t* s);
extern void semaphore_signal(semaphore_t* s);


/**
\defgroup semaphore_group Example usage: Semaphore
\{

Simple example that shows how to create a semaphore and how to initialise it.
After the semaphore is initialised it is ready for usage.

\code
semaphore_t data_ready;

// Initialises semaphore with zero initial resources
semaphore_init(&data_ready, 0);

// Task 1: Blocks the caller if no resources available
semaphore_wait(&data_ready);

// Task 2: Signals the semaphore for the availability of the resource
semaphore_signal(&data_ready);
\endcode
\}
*/

#endif /* _SEMAPHORE_H_ */
