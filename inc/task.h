/**
\file
\brief Task implementation

Task is the execution unit in kernel i.e. a set of program instructions that
are loaded into memory. This interface provides task related functionality.

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _TASK_H_
#define _TASK_H_


/**
\brief Task state definitions
*/
typedef enum
{
    RUNNING,    /**< Currently executing on a CPU */
    READY,      /**< Waiting execution on a CPU */
    BLOCKED     /**< Task is waiting until its resource becomes available */
} task_state;


/**
Function prototype for task

\param param Pointer to any type of data
*/
typedef void (*task_fn)(any_t* param);


/*
Function prototype(s)
*/
extern tcb_t* task_create(task_fn func, any_t* param, uint16_t stk_size, uint8_t priority, const sint8_t* name);
extern void task_yield(void);
extern void task_add(tcb_t* tcb);
extern void task_remove(tcb_t* tcb);
extern void task_set_state(task_state state);
extern tcb_t* task_highest_ready(void);
extern void task_sleep(uint32_t milliseconds);


#endif /* _TASK_H_ */
