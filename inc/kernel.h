/**
\file
\brief Miscellaneous kernel definitions
\author Copyright (c) 2007-2008, Tomi Parviainen
\mainpage

This is a brief documentation about the kernel and the functionality it
provides. Next image shows the high level architecture of the kernel, and
after that there is a list of the fundamental functionalities the kernel
has. The kernel is implemented totally in supervisor mode i.e. it is a
monolithic kernel. Also the application specific code is executed in
supervisor mode.

\image html architecture.jpg "Kernel Architecture"

\section Multitasking
Kernel allows multiple tasks to share common processing resources such as a
CPU. Each task has priority, which is used when the scheduler makes a
decision to take new task into execution. The task with the highest priority
is always guaranteed to be in running state.

\section Pre-emptive
Pre-emptive means that if the task is not in critical section it can be
scheduled out and other task can be scheduled in i.e. task is pre-emptable.
Mostly these context switches occur in case the task has consumed it's
time slice or other higher priority task will be scheduled into running.

\section Scheduling
Scheduling is a key concept in multitasking design. Task scheduling is
implemented in priority based manner i.e. task which has highest priority
is always guaranteed to run. If there are several tasks with the same priority
then each of those is assigned a time slice and after the time slice is
consumed a context switch to another task with the same priority is performed
in round-robin fashion.

\section ITC Inter Task Communication
Inter task communication (ITC) is available for tasks via mailboxes. Tasks can
communicate asynchronously to each other by sending and receiving messages via
mailboxes. The size of the mailbox is defined during the initialisation.

\section Semaphores
A semaphore is a protected variable, which is used for restricting access to
shared resources, such as shared memory or different HW peripherals. The
implementation supports counting semaphores, which is a counter for a set of
available resources, rather than locked / unlocked flag of a single resource.

\section Timers
The resolution of the timer services in kernel is millisecond. Once the timer
expires the callback function is called in IRQ context.

\todo Symmetric multiprocessing (SMP) support
*/

#ifndef _KERNEL_H_
#define _KERNEL_H_


/**
String to identify kernel version
*/
#define KERNEL_VERSION "--== Kernel 1.0 ==--"

/**
The NULL pointer constant
*/
#ifndef NULL
#define NULL  0
#endif // NULL

/**
Logical value for true
*/
#define TRUE  1

/**
Logical value for false
*/
#define FALSE 0

/**
The amount of task priority levels kernel supports. Because the task priority
value in task's TCB is unsigned 8-bit value it means that 256 levels of
priority are supported.

\todo This should be defined by the application code i.e. based on own needs!
*/
#define TASK_PRIORITY_COUNT 8

/**
Time slice value for each task after it will be pre-empted. Time slice value
is decremented on each system tick, once the zero is reached rescheduling will
be performed.
*/
#define TIME_SLICE 20

/*
Function prototype(s)
*/
extern int kernel_main(void);


#endif /* _KERNEL_H_ */
