/**
\file
\brief Circular (ring) buffer implementation

Circular buffer implementation. Client is responsible of ensuring that the
operations are synchronized properly!

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _BUFFER_H_
#define _BUFFER_H_


/**
\brief Buffer structure to hold data
*/
typedef struct {
    uint32_t* buffer;   /**< Buffer array for elements */
    uint32_t length;    /**< Length of the buffer */
    uint32_t start;     /**< Start of valid data */
    uint32_t end;       /**< End of valid data */
} buffer_t;


/*
Function prototype(s)
*/
extern void buffer_init(buffer_t* buffer, uint32_t length);
extern void buffer_add(buffer_t* buffer, any_t* data);
extern any_t* buffer_remove(buffer_t* buffer);
extern boolean_t buffer_empty(const buffer_t* buffer);


#endif /* _BUFFER_H_ */
