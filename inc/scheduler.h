/**
\file
\brief Scheduler implementation

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_


/*
Function prototype(s)
*/
extern void scheduler_start(void);
extern void scheduler_initial_context(stack_t* stack);
extern void scheduler_context_switch(tcb_t* tcb_new);
extern void scheduler_context_switch_irq(tcb_t* tcb_new);
extern void scheduler_tick(void);
extern void scheduler_reschedule(tcb_t* tcb);


#endif /* _SCHEDULER_H_ */
