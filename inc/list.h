/**
\file
\brief Linked list implementation

Linked list implementation for single and double lists with linearly and
circularly linked implementations. Client is responsible of ensuring that
the operations are synchronized properly!

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _LIST_H_
#define _LIST_H_


/*
Function prototype(s)
*/
extern tcb_t* single_add_last(tcb_t* list, tcb_t* p_tcb);
extern void single_add_after(tcb_t* list, tcb_t* tcb);
extern tcb_t* single_add_first(tcb_t* list, tcb_t* tcb);
extern tcb_t* double_add_before(tcb_t* list, tcb_t* p_tcb);
extern tcb_t* double_remove(tcb_t* list);


#endif /* _LIST_H_ */
