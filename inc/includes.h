/**
\file
\brief Master include file

This is a 'master' include file, this file includes all the needed kernel
header files so that the user need to include only this file in order to
use all functionalities from kernel.

\author Copyright (c) 2007-2009, Tomi Parviainen
*/

#ifndef _INCLUDES_H_
#define _INCLUDES_H_


#include "cpu.h"                /* CPU specific definitions */
#include "hw.h"                 /* HW specific definitions */
#include "debug.h"              /* Debug definitions */
#include "profiler.h"           /* Profiler definitions */
#include "task_control_block.h" /* Task Control Block */
#include "kernel.h"             /* Kernel definitions */
#include "time.h"               /* Time handling */
#include "globals.h"            /* Global definitions */
#include "task.h"               /* Task management */
#include "task_idle.h"          /* IDLE task function */
#include "trace.h"              /* Tracing functions */
#include "memory.h"             /* Memory allocation & deallocation */
#include "scheduler.h"          /* Scheduler */
#include "system.h"             /* System functions */
#include "bsp.h"                /* Board support package */
#include "irq.h"                /* IRQ handlind functions */
#include "gpio.h"               /* General Purpose Input/Output interface */
#include "buffer.h"             /* Circular buffer implementation */
#include "mailbox.h"            /* Mailbox implementation */
#include "semaphore.h"          /* Semaphore implementation */
#include "list.h"               /* Linked list implementation */
#include "app.h"                /* Application specific interface */


#endif /* _INCLUDES_H_ */
