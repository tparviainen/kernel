/**
\file
\brief Global variable definitions

This file contains global variables, which are used from different kernel
services. BSP should not use any of these variables directly because these
are meant to be called only from kernel.

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_


/*
Variable prototype(s)
*/
extern tcb_t* g_tcb_ready[TASK_PRIORITY_COUNT];
extern tcb_t* g_tcb_current;
extern tcb_t* g_tcb_reschedule;
extern tcb_t* g_tcb_sleep;
extern time_t g_uptime;
extern timer_t* g_timer_callback;

#ifdef PROF_CPU_USAGE
extern uint32_t g_task_idle_count;
extern uint32_t g_task_idle_count_max;
#endif /* PROF_CPU_USAGE */


/*
Function prototype(s)
*/
extern void globals_init(void);


#endif /* _GLOBALS_H_ */
