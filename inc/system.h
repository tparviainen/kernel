/**
\file
\brief Generic system wide functions

\author Copyright (c) 2007-2008, Tomi Parviainen.
*/

#ifndef _SYSTEM_H_
#define _SYSTEM_H_


/*
Function prototype(s)
*/
extern void system_tick(void);
extern void system_spin_lock(uint32_t* lock);
extern void system_spin_unlock(uint32_t* lock);


#endif /* _SYSTEM_H_ */
