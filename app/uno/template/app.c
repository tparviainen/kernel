/*
Copyright (c) 2015, Tomi Parviainen

Application specific code
*/

#include "includes.h"


/*
Function prototypes(s)
*/
static void app_task(any_t* param);


/*
APP trace enable macro
*/
#define TR_APP(a) a


/*
Application code initialisation. This function is called during the kernel
initialisation just before the multitasking will be started.
*/
void app_init()
    {

    tcb_t* tcb;

    /* TODO: initialisation ... */

    /* TODO: example task created here ... */
    tcb = task_create(app_task, NULL, 128, 1, "TEMPLATE");
    ASSERT(tcb != NULL);

    }


/*
APP task handling ...
*/
void app_task(any_t* param)
    {

    TR_APP(TR_FUNCTION);

    for (;;)
        {
        task_yield();
        //task_sleep(1000);
        }

    }
