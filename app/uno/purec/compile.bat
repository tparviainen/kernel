avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328p -c -o led.o led.c
avr-gcc -mmcu=atmega328p led.o -Wl,-Map=led.map,--cref -o led.elf
avr-objcopy -O ihex -R .eeprom led.elf led.hex
avr-objdump -S led.elf > led.asm
