#include <avr/io.h>
#include <util/delay.h>

enum {
    BLINK_DELAY_MS = 1000,
};

// https://balau82.wordpress.com/2011/03/29/programming-arduino-uno-in-pure-c/
// avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328p -c -o led.o led.c
// avr-gcc -mmcu=atmega328p led.o -o led.elf
// avr-gcc -mmcu=atmega328p led.o -Wl,-Map=led.map,--cref -o led.elf
// -nostdlib data/bss/alustukset puuttuu
// -nostartfiles vektoritaulukko puuttuu
// avr-gcc -mmcu=atmega328p led.o -nostdlib -nostartfiles -nodefaultlibs -Wl,-Map=led.map,--cref -o led.elf
// avr-objcopy -O ihex -R .eeprom led.elf led.hex
// avr-objdump -S led.elf > led.asm

#define kernel_main main

int kernel_main(void) {
    /* set pin 5 of PORTB for output*/
    DDRB |= _BV(DDB5);

    while(1) {
    /* set pin 5 high to turn led on */
    PORTB |= _BV(PORTB5);
    _delay_ms(BLINK_DELAY_MS);

    /* set pin 5 low to turn led off */
    PORTB &= ~_BV(PORTB5);
    _delay_ms(BLINK_DELAY_MS);
    }

    return 0;
}
