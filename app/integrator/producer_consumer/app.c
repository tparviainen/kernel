/*
Copyright (c) 2007-2008, Tomi Parviainen

One solution to producer / consumer problem. The producer's job is to generate
a piece of data, and start again. At the same time the consumer is consuming the
data one piece at a time. The problem is to make sure that the producer won't
try to add data into the buffer if it's full and that the consumer won't try to
remove data from an empty buffer.
*/

#include "includes.h"


/*
Variable declaration(s)
*/
static semaphore_t f;
static semaphore_t e;
static semaphore_t m;


/*
APP trace enable macro
*/
#define TR_APP(a) a


/*
Function prototypes(s)
*/
static void app_producer(any_t* param);
static void app_consumer(any_t* param);


/*
Initialises application specific code
*/
void app_init()
    {

    tcb_t* tcb;
    uint32_t cnt;
    uint32_t pri;

    semaphore_init(&f, 0);  /* Initially, no data in buffer */
    semaphore_init(&e, 8);  /* Initially, num empty slots (size of buffer) */
    semaphore_init(&m, 1);  /* Initially, resource available */

    /* There can be arbitrary amount of producers! */
    for (cnt = 0; cnt < 24; cnt++)
        {
        pri = cnt % TASK_PRIORITY_COUNT;
        tcb = task_create(app_producer, (any_t*)cnt, 128, pri, "PRODUCER");
        ASSERT(tcb != NULL);
        }

    /* There can be arbitrary amount of consumers! */
    for (cnt = 0; cnt < 32; cnt++)
        {
        pri = cnt % TASK_PRIORITY_COUNT;
        tcb = task_create(app_consumer, (any_t*)cnt, 128, pri, "CONSUMER");
        ASSERT(tcb != NULL);
        }

    }


/*
Producer, which is producing data and appending these in a buffer.
*/
void app_producer(any_t* param)
    {

    uint32_t task_cnt = (uint32_t)param;

    for (;;)
        {
        task_sleep(100 + 10 * task_cnt);

        /* Wait until space in buffer */
        semaphore_wait(&e);

        semaphore_wait(&m);
        TR_APP(trace_print("a"));   /* Append */
        semaphore_signal(&m);

        /* Tell consumers there is data in buffer */
        semaphore_signal(&f);
        }

    }


/*
Consumer, which is taking data items out of the buffer and then consumes the
data.
*/
void app_consumer(any_t* param)
    {

    uint32_t task_cnt = (uint32_t)param;

    for (;;)
        {
        /* Wait until data in buffer */
        semaphore_wait(&f);

        semaphore_wait(&m);
        TR_APP(trace_print("t"));   /* Take */
        semaphore_signal(&m);

        /* Tell producer that there is space for data */
        semaphore_signal(&e);

        task_sleep(100 + 10 * task_cnt);
        }

    }
