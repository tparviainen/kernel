/*
Copyright (c) 2007-2008, Tomi Parviainen

Creates maximum amount of tasks and then stays on loop in order to print
the task cnt and task name.

Task count + IDLE + PRODUCER with 128 element (512b) stack
27-Apr-2008: 0x72 (114)
05-May-2008: 0x74 (116)
*/

#include "includes.h"


/*
Variable declaration(s)
*/


/*
APP trace enable macro
*/
#define TR_APP(a) a


/*
Function prototypes(s)
*/
void app_producer(any_t* param);
void app_task(any_t* param);


/*
Initialises application specific code
*/
void app_init()
    {

    tcb_t* tcb;
    uint32_t priority;

    /* Random priority for the producer */
    priority = (TASK_PRIORITY_COUNT / 2);

    tcb = task_create(app_producer, NULL, 128, priority, "PRODUCER");
    ASSERT(tcb != NULL);

    }


/*
Producer, which creates tasks until the out of memory situation
*/
void app_producer(any_t* param)
    {

    tcb_t* tcb;
    uint32_t pri;
    sint32_t cnt = 0;

    while (cnt++ != -1)
        {
        pri = cnt % TASK_PRIORITY_COUNT;
        tcb = task_create(app_task, (any_t*)cnt, 128, pri, "TASK");

        /* Out of memory */
        if (tcb == NULL)
            {
            TR_APP(trace_print_hex("max task count", cnt-1));
            cnt = -1;
            }
        }

    for (;;)
        {
        task_sleep(123);
        }

    }


/*
Application task, prints the task name and count before going to sleep. Once
the task wakes up from sleep it yields CPU to another task.
*/
void app_task(any_t* param)
    {

    uint32_t task_cnt = (uint32_t)param;
    uint8_t overflow_cnt = 0;

    for (;;)
        {
        task_sleep(task_cnt);
        task_yield();

        /* Overflow on purpose and then print the name / cnt of the task
           in order to know that all tasks are still up and running */
        if (overflow_cnt++ == 0)
            {
            TR_APP(trace_print_hex(g_tcb_current->name, task_cnt));
            }
        }

    }
