#!perl -w
use strict;
use Tk::Photo;

die<<USAGE unless @ARGV;
Usage: $0 image.bmp [rows]
USAGE

my $bmp = $ARGV[0];
my $mw = MainWindow->new();
my $photo = $mw->Photo('fullscale', -file => $bmp);

$bmp =~ s/\..+$//;

# Image can contain less rows
my $end = $ARGV[1] ? $ARGV[1] : $photo->height;
if ($end > $photo->height) {
    $end = $photo->height;
}

my $file = "$bmp.h";
open(FILE, ">$file") or die $!;

printf FILE "/*\nCopyright (c) %d, Tomi Parviainen\n*/\n\n", (localtime(time))[5]+1900;
print FILE "/** The width of the image */\nconst uint32_t ${bmp}_bmp_w = ", $photo->width, ";\n\n";
print FILE "/** The height of the image */\nconst uint32_t ${bmp}_bmp_h = ", $end, ";\n\n";
print FILE "/** The data for the image, format = bbbbgggg rrrrbbbb ggggrrrr (3B = 2pix) */\nconst uint8_t ${bmp}_bmp [] =\n    {";

my $offset = 0;

foreach my $y (0 .. $end - 1) {
    foreach my $x (0 .. $photo->width - 1) {
        if (($offset++ % 14) == 0) {
            print FILE "\n   ";
        }
        my @pixel = $photo->get($x, $y);

        if (($offset & 0x1) == 0) {
            printf FILE "%x, 0x%x%x,", $pixel[2]/16, $pixel[1]/16, $pixel[0]/16;
        }
        else {
            printf FILE " 0x%x%x, 0x%x", $pixel[2]/16, $pixel[1]/16, $pixel[0]/16;
        }
    }
}

if ($offset & 1) {
    print FILE "0,";
}

print FILE"\n    };\n";
close(FILE);
