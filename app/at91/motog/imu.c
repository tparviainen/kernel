/*
Copyright (c) 2007-2009, Tomi Parviainen

IMU specific code
*/

#include <math.h>   /* atan2, M_PI, ... */
#ifdef SAM7X256
#include "at91sam7x256.h"
#else /* SAM7S256 */
#include "at91sam7s256.h"
#endif
#include "includes.h"
#include "ars.h"    /* ARS - Attitude (and Heading) Reference Systems */
#include "imu.h"    /* IMU - Inertial Measurement Unit */


/*
Variable declaration(s)
*/
static timer_t start_conversion;
static imu_data_t raw_data;
static gyro_roll_t gyro;
static g_force_t acc;
static semaphore_t data_ready;


/*
Function prototypes(s)
*/
static void imu_conversion_start(any_t* param);
static void imu_conversion_ready(void);
static void imu_task(any_t* param);
static void imu_calculate_rotation(imu_data_t* raw, gyro_roll_t* gyro);
static void imu_calculate_g_forces(imu_data_t* raw, g_force_t* g_f);
static uint32_t imu_get_diff(void);


/*
APP trace enable macro
*/
#define TR_IMU(a)


/*
Initialises AD converters to be able to read sensor data and LCD and
creates a task for handling the received data.
*/
void imu_init()
    {

    tcb_t* tcb;
    irq_fn func;

    memory_set(&acceleration_g, 0x00, sizeof(acceleration_g));
    lean_angle = 0;

    tcb = task_create(imu_task, NULL, 512, 2, "IMU");
    ASSERT(tcb != NULL);

    /* Create a periodic timer to read data from ADC */
    timer_init(&start_conversion, imu_conversion_start, NULL);

    semaphore_init(&data_ready, 0);

    func = irq_bind(ADC, imu_conversion_ready);
    ASSERT(func == NULL);

    irq_enable(ADC);

    }


/*
Prints IMU related data
*/
void imu_values(void)
    {
#ifdef DEBUG
    trace_print_dec("Ax", raw_data.raw_acc_x);
    trace_print_dec("Ay", raw_data.raw_acc_y);
    trace_print_dec("Az", raw_data.raw_acc_z);
    trace_print_dec("Gy", raw_data.raw_gyro_y);
    trace_print_dec("Gx", raw_data.raw_gyro_x);
    trace_print_dec("LA", fabsf(lean_angle));
#endif
    }


/*
Initiates ADC conversion from enabled ADC channels and keeps the periodic
timer running.
*/
void imu_conversion_start(any_t* param)
    {

    TR_IMU(TR_FUNCTION);

    /* Begin analog-to-digital conversion on enabled channels */
    AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;

    /* Periodic timer to initiate the ADC conversion, 100 times in second */
    timer_start(&start_conversion, 1000 / IMU_CONV_RATE);

    }


/*
Interrupt handler for ADC data ready interrupt. Reads the ADC data from HW
and stores it to global structure and signals semaphore to indicate that
data is available in structure.
*/
void imu_conversion_ready()
    {

    TR_IMU(TR_FUNCTION);

    raw_data.raw_acc_x = AT91C_BASE_ADC->ADC_CDR0;
    raw_data.raw_acc_y = AT91C_BASE_ADC->ADC_CDR1;
    raw_data.raw_acc_z = AT91C_BASE_ADC->ADC_CDR2;
    raw_data.raw_gyro_y = AT91C_BASE_ADC->ADC_CDR3;
    raw_data.raw_gyro_x = AT91C_BASE_ADC->ADC_CDR4;

    semaphore_signal(&data_ready);

    }


/*
Calculates the acceleration in G-forces and angle in degrees and posts the
data to mailbox.
*/
void imu_task(any_t* param)
    {

    /* Accelerometer and gyroscope based lean angles */
    float a_angle;
    float g_angle;

    uint16_t sample = 0;

    TR_IMU(TR_FUNCTION);

    /* And so it begins ... after 500 ms */
    timer_start(&start_conversion, 500);

    for (;;)
        {
        /* Wait data from IMU sensor */
        semaphore_wait(&data_ready);

        /* Calculate G-forces */
        imu_calculate_g_forces(&raw_data, &acc);

        // TBD: if ROLL else G_FORCE, only needed in ROLL calculation!

        /* Calculate rotation (values are in radians) */
        imu_calculate_rotation(&raw_data, &gyro);

        /* Calculate the lean angle (rad) based on the accelerometer data */
        a_angle = atan2f(acc.y, acc.z);

// TBD allow user to recalibrate angle -> +- correction here!
// bypass the need to calibrate Z-value!

        /* Predict, update and calculate THE angle */
        ars_predict(gyro.x, imu_get_diff() / 1000.0);
        g_angle = ars_update(a_angle);

        /* Convert radians to degrees */
        g_angle *= (180.0 / M_PI);
        a_angle *= (180.0 / M_PI);

        /* Collects last XYZ samples to an array */
        acceleration_g[sample++] = (sint16_t)g_angle;
        lean_angle = (sint16_t)g_angle;

#if 0
        static uint16_t zero_bias = 0;
        if (lean_angle == 0)
            {
            if (zero_bias++ == IMU_CONV_RATE)
                {
                /* Angle has been stable for a while, time to recalibrate
                   zero bias values */
                zero_bias = 0;

                ZERO_RATE_ADC_X += raw_data.raw_gyro_x;
                ZERO_RATE_ADC_Y += raw_data.raw_gyro_y;

                ZERO_RATE_ADC_X /= 2;
                ZERO_RATE_ADC_Y /= 2;
                }
            }
        else
            {
            zero_bias = 0;
            }
#endif

        if (sample == BUFFER_SIZE)
            {
            /* Circular buffer flipped over */
            sample = 0;
            }

        TR_IMU(trace_print_dec("g_angle", fabsf(g_angle)));
        TR_IMU(trace_print_dec("a_angle", fabsf(a_angle)));
        //trace_print_dec("g_angle", fabsf(g_angle));
        //trace_print_dec("a_angle", fabsf(a_angle));
        }

    }


/*
Calculates the rotation from gyroscope and stores the values in radians to
further processing. The stored data is normalized!
*/
void imu_calculate_rotation(imu_data_t* raw, gyro_roll_t* gyro)
    {

    TR_IMU(TR_FUNCTION);

    /* ADC conversion extend from 0V to ADVREF, which is 3.3V in our board */
    const float ADVREF          = 3.3;

    /* ADC is 10-bit, which means that the maximum value is 2^10 */
    const float ADC_MAX         = 1024.0;

    /* IDG-300 dual axis Gyro has a sensitivity of 2mV/�/s */
    const float SENSITIVITY     = 0.002;

    /* Zero-rate output of IDG-300 is 1.5V (465) however next values are the actual
       measured values from the Gyro for X and Y directions. */
    const float ZERO_RATE_ADC_X = 449;
    const float ZERO_RATE_ADC_Y = 527;

    /* Calculate the angular rates for X/Y directions */
    gyro->x = ((raw->raw_gyro_x - ZERO_RATE_ADC_X)  /* 1. X-rate in ADC */
            * (ADVREF / ADC_MAX))                   /* 2. X-rate in mV */
            / SENSITIVITY                           /* 3. X-rate in degrees */
            * (M_PI / 180.0);                       /* 4. X-rate in radians */

    gyro->y = ((raw->raw_gyro_y - ZERO_RATE_ADC_Y)  /* 1. Y-rate in ADC */
            * (ADVREF / ADC_MAX))                   /* 2. Y-rate in mV */
            / SENSITIVITY                           /* 3. Y-rate in degrees */
            * (M_PI / 180.0);                       /* 4. Y-rate in radians */

    TR_IMU(trace_print_dec("Gyro angle X", 1000 * fabsf(gyro->x)));
    TR_IMU(trace_print_dec("Gyro angle Y", 1000 * fabsf(gyro->y)));

    }


/*
Calculates the G-forces for each axis based on the raw accelerometer data read
from the sensor.
*/
void imu_calculate_g_forces(imu_data_t* raw, g_force_t* g_f)
    {

    TR_IMU(TR_FUNCTION);

    /* ADC conversion extend from 0V to ADVREF, which is 3.3V in our board */
    const float ADVREF          = 3.3;

    /* ADC is 10-bit, which means that the maximum value is 2^10 */
    const float ADC_MAX         = 1024.0;

    /* ADXL330 accelerometer sensitivity (V/g), measured values based on +- 1g
       measurement changes i.e.
       1. Measure the  1g in terms of ADC
       2. Measure the -1g in terms of ADC
       3. Calculate the difference in terms of ADC
       4. Calculate the difference in terms of mV (ADCdiff * ADVREF / ADC_MAX)
       5. Divide the result by 2 ==> axel sensitivity in terms of mV/g */
    const float SENSITIVITY_X   = 0.331; /* 414-417 - 619-623 = 517, typical 0.300 */
    const float SENSITIVITY_Y   = 0.339; /* 403-405 - 613-616 = 508 */
    const float SENSITIVITY_Z   = 0.315; /* 415-417 - 611-612 = 514 */

    /* Zero rate output values, measured values between +-1g and then the
       average of that is the zero rate output value. */
    const float ZERO_RATE_ADC_X = 518; // Typical 512
    const float ZERO_RATE_ADC_Y = 509;
    const float ZERO_RATE_ADC_Z = 514;

    g_f->x = ((raw->raw_acc_x - ZERO_RATE_ADC_X)    /* 1. X-rate in ADC */
           * (ADVREF / ADC_MAX))                    /* 2. X-rate in mV */
           / SENSITIVITY_X;                         /* 3. X-rate in g's */

    g_f->y = ((raw->raw_acc_y - ZERO_RATE_ADC_Y)    /* 1. Y-rate in ADC */
           * (ADVREF / ADC_MAX))                    /* 2. Y-rate in mV */
           / SENSITIVITY_Y;                         /* 3. Y-rate in g's */

    g_f->z = ((raw->raw_acc_z - ZERO_RATE_ADC_Z)    /* 1. Z-rate in ADC */
           * (ADVREF / ADC_MAX))                    /* 2. Z-rate in mV */
           / SENSITIVITY_Z;                         /* 3. Z-rate in g's */

    TR_IMU(trace_print_dec("Acc xg", 1000 * fabsf(g_f->x)));
    TR_IMU(trace_print_dec("Acc yg", 1000 * fabsf(g_f->y)));
    TR_IMU(trace_print_dec("Acc zg", 1000 * fabsf(g_f->z)));

    }


/*
Calculates the time difference in milliseconds between the previous and
current call of this function.
*/
uint32_t imu_get_diff()
    {

    time_t now;
    uint32_t diff;
    static time_t prev = {0, 0, 0, 0, 0};

    time_uptime(&now);

    /* Check whether second boundary is crossed */
    if (now.ms < prev.ms)
        {
        diff = (now.ms + 1000 - prev.ms);
        }
    else
        {
        diff = (now.ms - prev.ms);
        }

    prev = now;

    return diff;

    }
