/**
\file
\brief LCD

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _LCD_H_
#define _LCD_H_


/*
GE8 - Command list
*/
#define DISON     0xAF      /* Display on */
#define DISOFF    0xAE      /* Display off */
#define DISNOR    0xA6      /* Normal display */
#define DISINV    0xA7      /* Inverse display */
#define COMSCN    0xBB      /* Common scan direction */
#define DISCTL    0xCA      /* Display control */
#define SLPIN     0x95      /* Sleep in */
#define SLPOUT    0x94      /* Sleep out */
#define PASET     0x75      /* Page address set */
#define CASET     0x15      /* Column address set */
#define DATCTL    0xBC      /* Data scan direction, etc. */
#define RGBSET8   0xCE      /* 256-color position set */
#define RAMWR     0x5C      /* Writing to memory */
#define RAMRD     0x5D      /* Reading from memory */
#define PTLIN     0xA8      /* Partial display in */
#define PTLOUT    0xA9      /* Partial display out */
#define RMWIN     0xE0      /* Read and modify write */
#define RMWOUT    0xEE      /* End */
#define ASCSET    0xAA      /* Area scroll set */
#define SCSTART   0xAB      /* Scroll start set */
#define OSCON     0xD1      /* Internal oscillation on */
#define OSCOFF    0xD2      /* Internal oscillation off */
#define PWRCTR    0x20      /* Power control */
#define VOLCTR    0x81      /* Electronic volume control */
#define VOLUP     0xD6      /* Increment electronic control by 1 */
#define VOLDOWN   0xD7      /* Decrement electronic control by 1 */
#define TMPGRD    0x82      /* Temperature gradient set */
#define EPCTIN    0xCD      /* Control EEPROM */
#define EPCOUT    0xCC      /* Cancel EEPROM control */
#define EPMWR     0xFC      /* Write into EEPROM */
#define EPMRD     0xFD      /* Read from EEPROM */
#define EPSRRD1   0x7C      /* Read register 1 */
#define EPSRRD2   0x7D      /* Read register 2 */
#define NOP       0x25      /* NOP instruction */


/*
Values for image size
*/
#define MAX_X 130
#define MAX_Y 130
#define IMG_SIZE (MAX_X * MAX_Y * 1.5)

/*
The color palette for 1 pixel is 'rrrrggggbbbb'
*/
#define R(c) ((c >> 8) & 0xF) /* Red */
#define G(c) ((c >> 4) & 0xF) /* Green */
#define B(c) ((c >> 0) & 0xF) /* Blue */


/*
Structure, which contains the updateable screen area
*/
typedef struct {
    /* Screen (RAM memory) buffer parameters */
    const any_t* area;  /* Start of the update area in screen buffer */
    uint16_t width;     /* Pixel width of the box */
    uint16_t height;    /* Pixel height of the box */
    uint16_t skip;      /* The amount of pixel to skip after each transfer */

    /* Display (for example LCD) buffer parameters */
    uint16_t x;         /* The start X-location in display buffer */
    uint16_t y;         /* The start Y-location in display buffer */
} update_area_t;


/*
Function prototypes(s)
*/
extern void lcd_init_controller(void);
extern void lcd_spi_write(uint32_t value, uint32_t cmd);
extern void lcd_draw_pixel(uint32_t x, uint32_t y, uint32_t color);
extern void lcd_draw_line(uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint32_t color);
extern void lcd_copy(update_area_t* src, update_area_t* trgt);

extern void lcd_alpha_blend(update_area_t* src1, update_area_t* src2, update_area_t* trgt, uint32_t alpha);
extern uint32_t lcd_pixel_get(const any_t* ptr, uint32_t offset);
extern void lcd_line_draw(update_area_t* ptr, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color);
extern void lcd_pixel_draw(update_area_t* ptr, uint16_t x, uint16_t y, uint32_t color);
extern void lcd_box_fill(update_area_t* ptr, uint32_t color);
extern void lcd_screen_to_display(update_area_t* ptr);
extern void lcd_clear_screen(void);


#endif /* _LCD_H_ */
