/*
Copyright (c) 2007-2010, Tomi Parviainen

Application specific code
*/

#include "includes.h"

#ifdef SAM7X256
#include "at91sam7x256.h"
#else /* SAM7S256 */
#include "at91sam7s256.h"
#endif

#include "lcd.h"
#include "imu.h"

#include "bmp/numbers_s.h"
#include "bmp/numbers_l.h" /* h = 10 for RUN_RAM */
#include "bmp/direction.h"
#include "bmp/max.h"
#include "bmp/box.h"

/* 21 = 14pix * 1.5B/pix */
#define NUMBERS_S_OFFSET 21

/* 66 = 44pix * 1.5B/pix */
#define NUMBERS_L_OFFSET 66

/* 24 = 16pix * 1.5B/pix */
#define DIRECTION_OFFSET 24

/* 9 = 6pix * 1.5B/pix */
#define MAX_OFFSET 9

#define INITIAL_MAX_ANGLE 20

#define ANGLE_L_X 3
#define ANGLE_L_Y 4
#define ANGLE_R_X 67
#define ANGLE_R_Y 4


/*
Structure that holds the most recent values from the IMU
*/
typedef struct {
    /* Maximum values in period of time */
    sint16_t max_l_p; /* zero or positive */
    sint16_t max_r_p; /* zero or negative */

    /* Maximum values since application started */
    sint16_t max_l; /* zero or positive */
    sint16_t max_r; /* zero or negative */

    /* Current lean angle */
    sint32_t angle;

    /* Bitmask for changed values */
    uint32_t update;
} data_buffer_t;

#define MAX_L_P 0x01
#define MAX_R_P 0x02
#define MAX_L   0x04
#define MAX_R   0x08
#define ANGLE   0x10


/*
Variable declaration(s)
*/
static timer_t update_values;
static data_buffer_t data;
static mailbox_t display_task;
static update_area_t numbers_s;
static update_area_t direction;


/*
Function prototypes(s)
*/
static void app_task(any_t* param);
static void app_switch_change(gpio_pin pin);
static void app_update_values(any_t *param);
static void app_update_direction(sint32_t angle);
static void app_update_numbers_s(uint16_t x, uint16_t y, sint32_t num);


/*
APP trace enable macro
*/
#define TR_APP(a)

/*
Defines the maximum display update frequency in second. Update frequency can
be smaller if there is no data to update to display.
*/
#define DISPLAY_UPDATE_RATE 10


/*
Initialises AD converters to be able to read sensor data and LCD.
*/
void app_init()
    {

    tcb_t* tcb;

    tcb = task_create(app_task, NULL, 512, 1, "MotoG");
    ASSERT(tcb != NULL);

    mailbox_init(&display_task, 1);

    /* Enable peripheral clocks */
#ifdef SAM7X256
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_SPI0);
#else /* SAM7S256 */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_SPI);
#endif

    /* Enable backlight control directly via I/O pin (PWM functionality not
       used) and enable output functionality */
#ifdef SAM7X256
    gpio_value_set(PB20, 1);
    gpio_mode_set(PB20, GPIO_MODE_OUTPUT);
#endif

    /* Initialise SPI bus i.e. disable the PIO from controlling the pin and
       multiplex functionality to SPI. MISO cannot be used with the LCD because
       the controller does not support data read in serial mode! */
#ifdef SAM7X256
    AT91C_BASE_PIOA->PIO_PDR = (1 << 18)  /* SCLK - SCL */
                             | (1 << 17)  /* MOSI - SI */
                             | (1 << 16)  /* MISO - NC */
                             | (1 << 12); /* CS   - CS */
    AT91C_BASE_PIOA->PIO_ASR = (1 << 18)
                             | (1 << 17)
                             | (1 << 16)
                             | (1 << 12);
#else /* SAM7S256 */
    AT91C_BASE_PIOA->PIO_PDR = (1 << 14)  /* SCLK - SCL */
                             | (1 << 13)  /* MOSI - SI */
                             | (1 << 12)  /* MISO - NC */
                             | (1 << 11); /* CS   - CS */
    AT91C_BASE_PIOA->PIO_ASR = (1 << 14)
                             | (1 << 13)
                             | (1 << 12)
                             | (1 << 11);
#endif

    /* Enable the SPI to transfer and receive data */
#ifdef SAM7X256
    AT91C_BASE_SPI0->SPI_CR = AT91C_SPI_SPIEN;
#else /* SAM7S256 */
    AT91C_BASE_SPI->SPI_CR  = AT91C_SPI_SPIEN;
#endif

    /* Master mode, fixed peripheral select, mode fault detection disabled
       peripheral 0 selected i.e. 0 at offset 16 */
#ifdef SAM7X256
    AT91C_BASE_SPI0->SPI_MR = AT91C_SPI_MSTR
#else /* SAM7S256 */
    AT91C_BASE_SPI->SPI_MR  = AT91C_SPI_MSTR
#endif
                            | AT91C_SPI_PS_FIXED
                            | AT91C_SPI_MODFDIS
                            | (AT91C_SPI_PCS & (0 << 16));

    /* The inactive state value of SPCK is logic level one, 9 bits per transfer
       6MHz SPI clock, delay SPCK after CS asserted, delay between consecutive
       transfers */
#ifdef SAM7X256
    AT91C_BASE_SPI0->SPI_CSR[0] = AT91C_SPI_CPOL
#else /* SAM7S256 */
    AT91C_BASE_SPI->SPI_CSR[0]  = AT91C_SPI_CPOL
#endif
                                | AT91C_SPI_BITS_9
                                | (AT91C_SPI_SCBR & (8 << 8)) /* 6 MHz */
                                | (AT91C_SPI_DLYBS & (1 << 16))
                                | (AT91C_SPI_DLYBCT & (1 << 24));

    /* Set the conversion parameters i.e. maximum startup and hold times */
    AT91C_BASE_ADC->ADC_MR = AT91C_ADC_TRGEN_DIS
                           | AT91C_ADC_LOWRES_10_BIT
                           | AT91C_ADC_SLEEP_NORMAL_MODE
                           | (AT91C_ADC_PRESCAL & (0xF << 8))
                           | (AT91C_ADC_STARTUP & (0x1F << 16))
                           | (AT91C_ADC_SHTIM & (0xF << 24));

    /* Hardware initialisation (VRef is not connected from sensor, however the
       output from the Vref is 1.23V i.e. IDG-300 reference voltage!):
       - AD0 -> X-Rate (PB27)
       - AD1 -> Y-Rate (PB28)
       - AD2 -> Z-Axis (PB29)
       - AD3 -> Y-Axis (PB30)
       - AD4 -> X-Axis
     */

    /* Disable the PIO from controlling the IO pins, AD4 is not behind PIO
       controller thus it is automatically AD pin */
#ifdef SAM7X256
    AT91C_BASE_PIOB->PIO_PDR = (1 << 27)
                             | (1 << 28)
                             | (1 << 29)
                             | (1 << 30);
#else /* SAM7S256 */
    AT91C_BASE_PIOA->PIO_PDR = (1 << 17)
                             | (1 << 18)
                             | (1 << 19)
                             | (1 << 20);
#endif

    /* Enable ADC channels */
    AT91C_BASE_ADC->ADC_CHER = AT91C_ADC_CH0
                             | AT91C_ADC_CH1
                             | AT91C_ADC_CH2
                             | AT91C_ADC_CH3
                             | AT91C_ADC_CH4;

    /* Enable the End Of Conversion (EOC) IRQ from the last used channel.
       The assumption here is that conversion of the enabled channels is
       performed starting from the lowest enabled channel towards the last
       enabled channel -> when IRQ occurs the conversion of each channel
       is ready. */
    AT91C_BASE_ADC->ADC_IER = AT91C_ADC_CH4;

    memory_set(&data, 0x00, sizeof(data));

    data.max_r = -INITIAL_MAX_ANGLE;
    data.max_l = +INITIAL_MAX_ANGLE;

    imu_init();

    }


/*
Handles the events from the outside world i.e. from I/O pins. Please note that
I/O pin causes events when the key is pressed and when it is released, thus
this function gets called twice when key is pressed!

\param pin The I/O pin that caused the event
*/
void app_switch_change(gpio_pin pin)
    {

    TR_APP(trace_print_dec("pinid", pin));
    TR_APP(trace_print_hex("value", gpio_value_get(pin)));

#ifdef SAM7X256
    switch (pin)
        {
        case PB24:
            /* Reset the maximum values and force update */
            data.update = MAX_L|MAX_R;
            data.max_r = -INITIAL_MAX_ANGLE;
            data.max_l = +INITIAL_MAX_ANGLE;
            break;

        case PB25:
            //lcd_clear_screen();
            if (gpio_value_get(pin)==0)
                imu_values();
            break;

        default:
            break;
        }
#else /* SAM7S256 */
    // TBD
#endif

    }


/*
Reads the latest values from IMU and if the minimum / maximum values are
changed then posts a message to mailbox to initiate display update. This
function is executed in interrupt context.
*/
void app_update_values(any_t *param)
    {

    sint16_t max_r = 0;
    sint16_t max_l = 0;

    timer_start(&update_values, 1000 / DISPLAY_UPDATE_RATE);

    /* Calculate the maximum values for period of time. Buffer either contains
       uninitialised (0) values or +- values for lean angle (including 0). */
    for (uint16_t cnt = 0; cnt < BUFFER_SIZE; cnt++)
        {
        sint16_t value = acceleration_g[cnt];

        if (value > max_l)
            {
            max_l = value;
            }
        else if (value < max_r)
            {
            max_r = value;
            }
        }

    /* New maximum value for this period of time */
    if (data.max_l_p != max_l)
        {
        data.update |= MAX_L_P;
        data.max_l_p = max_l;

        if (max_l > data.max_l)
            {
            /* New absolute maximum value */
            data.update |= MAX_L;
            data.max_l = max_l;
            }
        }

    if (data.max_r_p != max_r)
        {
        data.update |= MAX_R_P;
        data.max_r_p = max_r;

        if (max_r < data.max_r)
            {
            data.update |= MAX_R;
            data.max_r = max_r;
            }
        }

    if (data.angle != lean_angle)
        {
        data.update |= ANGLE;
        data.angle = lean_angle;
        }

    if (data.update != 0)
        {
        /* The content of the display need to be updated */
        mailbox_post(&display_task, (any_t*)0xFEED);
        }

    }


/*
*/
void app_task(any_t* param)
    {

    sint32_t p_angle = 0;
    /* Defined as static in order to not to consume stack, instead these
       variables goes to .bss area. */
    static update_area_t numbers_l;
    static update_area_t max;
    static update_area_t box;

    TR_APP(TR_FUNCTION);

    lcd_init_controller();

    numbers_s.area = NULL;
    numbers_s.width = numbers_s_img_w / 10;
    numbers_s.height = numbers_s_img_h;
    numbers_s.skip = numbers_s_img_w - numbers_s_img_w / 10;

    numbers_l.area = NULL;
    numbers_l.width = numbers_l_img_w / 10;
    numbers_l.height = numbers_l_img_h;
    numbers_l.skip = numbers_l_img_w - numbers_l_img_w / 10;
    numbers_l.y = 31;

    direction.area = NULL;
    direction.width = direction_img_w / 3;
    direction.height = direction_img_h;
    direction.skip = direction_img_w - direction_img_w / 3;
    direction.y = 45;

    /* Max text below the screen */
    max.area = max_img;
    max.width = max_img_w;
    max.height = max_img_h;
    max.skip = 0;
    max.x = 38;
    max.y = 108;

    /* Middle block on top of screen */
    box.area = &box_img[0 * MAX_OFFSET];
    box.width = 6;
    box.height = 8;
    box.skip = box_img_w - 6;
    box.x = 62;
    box.y = 12;

#ifdef SAM7X256
    gpio_irq_bind(PB24, &app_switch_change);
    gpio_mode_set(PB24, GPIO_MODE_INPUT|GPIO_MODE_PULL_DOWN);
    gpio_irq_enable(PB24);

    gpio_irq_bind(PB25, &app_switch_change);
    gpio_mode_set(PB25, GPIO_MODE_INPUT|GPIO_MODE_PULL_DOWN);
    gpio_irq_enable(PB25);
#else /* SAM7S256 */
    // TBD - if switches supported!
    app_switch_change(1);
#endif

    timer_init(&update_values, app_update_values, NULL);

    /* Trigger immediate update to all values on display */
    data.update = 0xFF;

    task_sleep(2000);

    lcd_clear_screen();

    lcd_screen_to_display(&max);
    lcd_screen_to_display(&box);

    timer_start(&update_values, 1000 / DISPLAY_UPDATE_RATE);

    for (;;)
        {
        uint32_t update;
        sint32_t c_angle;
        uint32_t irq_bits;

        /* Wait until there is data that needs to be drawn on display */
        mailbox_pend(&display_task);

        /* Determine what values need to be updated */
        irq_bits = irq_disable_core();
        c_angle = data.angle;
        update = data.update;
        data.update = 0;
        irq_enable_core(irq_bits);

        if (update & ANGLE)
            {
            sint32_t tens, ones;

            if (c_angle < 0)
                {
                tens = -c_angle / 10;
                ones = -c_angle - tens * 10;
                }
            else
                {
                tens = c_angle / 10;
                ones = c_angle - tens * 10;
                }

            /* Max angle that can be shown on display is 99 degrees */
            if (tens > 9)
                {
                tens = 9;
                }

            numbers_l.x = 20; /* Position on the display */
            numbers_l.area = &numbers_l_img[tens * NUMBERS_L_OFFSET];
            lcd_screen_to_display(&numbers_l);

            numbers_l.x += numbers_l_img_w / 10;
            numbers_l.area = &numbers_l_img[ones * NUMBERS_L_OFFSET];
            lcd_screen_to_display(&numbers_l);

            p_angle = c_angle;

            app_update_direction(c_angle);
            }

        if (update & MAX_L_P)
            {
            /* Number of blocks on screen */
            static sint16_t nob_on_screen = 0;

            sint16_t nob = 0;
            sint16_t val = data.max_l_p;

            /* Common max value from left and right directions! */
            sint16_t max = (data.max_l > -data.max_r) ? data.max_l : -data.max_r;

            TR_APP(trace_print_dec("MAX_L_P", val));
            app_update_numbers_s(4, 6, val);

            /* Number of blocks to be shown on display */
            val = val ? (val - 1) : 0;
            nob = ((val * 5) / max) + 1; /* 1 - 5 */

            if (val == 0)
                {
                nob = 0;
                }

            ASSERT(val <= max);
            ASSERT(nob <= 5);

            /* Check the need to clear / add blocks */
            if (nob != nob_on_screen)
                {
                TR_APP(trace_print_dec("MAX_L_P <cb>", nob));
                TR_APP(trace_print_dec("MAX_L_P <os>", nob_on_screen));

                if (nob > nob_on_screen)
                    {
                    /* Add memory blocks to display */
                    box.area = &box_img[0 * MAX_OFFSET];

                    for (sint16_t tmp = nob_on_screen; tmp != nob; tmp++)
                        {
                        box.x = 56 - 6 * tmp;
                        box.y = 11 - tmp;
                        box.height = 10 + tmp * 2;
                        lcd_screen_to_display(&box);
                        }
                    }
                else
                    {
                    /* Clear memory blocks from display */
                    box.area = &box_img[1 * MAX_OFFSET];

                    for (sint16_t tmp = nob_on_screen; tmp != nob; tmp--)
                        {
                        box.x = 56 - 6 * (tmp - 1);
                        box.y = 12 - tmp;
                        box.height = 10 + (tmp - 1) * 2;
                        lcd_screen_to_display(&box);
                        }
                    }

                nob_on_screen = nob;
                }
            }

        if (update & MAX_R_P)
            {
            //TR_APP(trace_print("MAX_R_P"));
            //app_update_numbers_s(98, 6, -data.max_r_p);

            /* Number of blocks on screen */
            static sint16_t nob_on_screen = 0;

            sint16_t nob = 0;
            sint16_t val = -data.max_r_p;

            /* Common max value from left and right directions! */
            sint16_t max = (data.max_l > -data.max_r) ? data.max_l : -data.max_r;

            TR_APP(trace_print_dec("MAX_R_P", val));
            app_update_numbers_s(98, 6, val);

            /* Number of blocks to be shown on display */
            val = val ? (val - 1) : 0;
            nob = ((val * 5) / max) + 1; /* 1 - 5 */

            if (val == 0)
                {
                nob = 0;
                }

            ASSERT(val <= max);
            ASSERT(nob <= 5);

            /* Check the need to clear / add blocks */
            if (nob != nob_on_screen)
                {
                TR_APP(trace_print_dec("MAX_R_P <cb>", nob));
                TR_APP(trace_print_dec("MAX_R_P <os>", nob_on_screen));

                if (nob > nob_on_screen)
                    {
                    /* Add memory blocks to display */
                    box.area = &box_img[0 * MAX_OFFSET];

                    for (sint16_t tmp = nob_on_screen; tmp != nob; tmp++)
                        {
                        box.x = 68 + 6 * tmp;
                        box.y = 11 - tmp;
                        box.height = 10 + tmp * 2;
                        lcd_screen_to_display(&box);
                        }
                    }
                else
                    {
                    /* Clear memory blocks from display */
                    box.area = &box_img[1 * MAX_OFFSET];

                    for (sint16_t tmp = nob_on_screen; tmp != nob; tmp--)
                        {
                        // 5 = 34, 4 = 40, 3 = 46, 2 = 52, 1 = 58
                        box.x = 68 + 6 * (tmp - 1);
                        box.y = 12 - tmp;
                        box.height = 10 + (tmp - 1) * 2;
                        lcd_screen_to_display(&box);
                        }
                    }

                nob_on_screen = nob;
                }

            }

        if (update & MAX_L)
            {
            TR_APP(trace_print("MAX_L"));
            app_update_numbers_s(4, 106, data.max_l);
            }

        if (update & MAX_R)
            {
            TR_APP(trace_print("MAX_R"));
            app_update_numbers_s(98, 106, -data.max_r);
            }
        }

    }


/*
Draws direction logo on the display. In case the direction changed from left
to right then the left direction is removed from display before right logo
is drawn on the display.

\param angle Current lean angle angle
*/
void app_update_direction(sint32_t angle)
    {

    #define DIR_L 0x1
    #define DIR_R 0x2

    static uint32_t on_screen = 0;

    uint32_t set = 0;
    uint32_t clr = 0;

    if (angle < 0)      /* Right */
        {
        if (on_screen & DIR_L)
            {
            clr |= DIR_L;
            }
        if ((on_screen & DIR_R) == 0)
            {
            on_screen = DIR_R;
            set |= DIR_R;
            }
        }
    else if (angle > 0) /* Left */
        {
        if (on_screen & DIR_R)
            {
            clr |= DIR_R;
            }
        if ((on_screen & DIR_L) == 0)
            {
            on_screen = DIR_L;
            set |= DIR_L;
            }
        }
    else                /* Empty */
        {
        if (on_screen & DIR_L)
            {
            clr |= DIR_L;
            }
        if (on_screen & DIR_R)
            {
            clr |= DIR_R;
            }
        on_screen = 0;
        }

    if (clr)
        {
        direction.area = &direction_img[1 * DIRECTION_OFFSET];

        if (clr & DIR_L)
            {
            TR_APP(trace_print("CLR L"));
            direction.x = 2; /* Position on the display */
            lcd_screen_to_display(&direction);
            }
        if (clr & DIR_R)
            {
            TR_APP(trace_print("CLR R"));
            direction.x = 112; /* Position on the display */
            lcd_screen_to_display(&direction);
            }
        }

    if (set)
        {
        if (set & DIR_L)
            {
            TR_APP(trace_print("SET L"));
            direction.x = 2; /* Position on the display */
            direction.area = &direction_img[0 * DIRECTION_OFFSET];
            lcd_screen_to_display(&direction);
            }
        if (set & DIR_R)
            {
            TR_APP(trace_print("SET R"));
            direction.x = 112; /* Position on the display */
            direction.area = &direction_img[2 * DIRECTION_OFFSET];
            lcd_screen_to_display(&direction);
            }
        }

    }


/*
Displays the given number in LCD. The maximum value for the number to be shown
is 99.
*/
void app_update_numbers_s(uint16_t x, uint16_t y, sint32_t num)
    {

    sint32_t tens, ones;

    /* Only positive numbers are allowed */
    ASSERT(num >= 0);

    if (num > 99)
        {
        num = 99;
        }

    tens = num / 10;
    ones = num - tens * 10;

    /* Display first number */
    numbers_s.x = x;
    numbers_s.y = y;
    numbers_s.area = &numbers_s_img[tens * NUMBERS_S_OFFSET];
    lcd_screen_to_display(&numbers_s);

    /* Display second number */
    numbers_s.x += numbers_s_img_w / 10;
    numbers_s.area = &numbers_s_img[ones * NUMBERS_S_OFFSET];
    lcd_screen_to_display(&numbers_s);

    }
