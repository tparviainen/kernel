<map version="0.8.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1218872124953" ID="Freemind_Link_652264911" MODIFIED="1218872141875" TEXT="MotoG">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1218872179171" ID="Freemind_Link_147756421" MODIFIED="1218872764500" POSITION="right" TEXT="IMU5 Data">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1218872202265" ID="Freemind_Link_883155627" MODIFIED="1218876316250" TEXT="periodic timer">
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#990000" CREATED="1218876568781" ID="Freemind_Link_1263070814" MODIFIED="1218895178687" TEXT="100Hz">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1218875175843" ID="Freemind_Link_980371352" MODIFIED="1218875175859" TEXT="signaling">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1218872266265" ID="Freemind_Link_983610620" MODIFIED="1218876157718" TEXT="semaphore">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1218874605328" ID="Freemind_Link_918173088" MODIFIED="1218875860140" TEXT="data overwrite">
<arrowlink DESTINATION="Freemind_Link_761507098" ENDARROW="Default" ENDINCLINATION="90;0;" ID="Freemind_Arrow_Link_1162267352" STARTARROW="None" STARTINCLINATION="90;0;"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="clanbomber"/>
<node COLOR="#990000" CREATED="1218875897578" ID="Freemind_Link_175623486" MODIFIED="1218876198156" TEXT="cannot happen if separate high &#xa;priority task for calculation!">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1218872768156" ID="_" MODIFIED="1218872770953" POSITION="left" TEXT="Lean angle">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1218874672609" ID="Freemind_Link_1221187994" MODIFIED="1218874676640" TEXT="max values">
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1218872773562" ID="Freemind_Link_164783643" MODIFIED="1218872775640" POSITION="left" TEXT="G-forces">
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1218872853812" ID="Freemind_Link_1314047859" MODIFIED="1218872856718" POSITION="right" TEXT="Architecture">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1218872931234" ID="Freemind_Link_881740619" MODIFIED="1218875299796" TEXT="tasks">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1218875285281" ID="Freemind_Link_289079093" MODIFIED="1218886446703" TEXT="idle">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1219343659453" ID="Freemind_Link_691700541" MODIFIED="1219343676828" TEXT="always present">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node COLOR="#990000" CREATED="1218875287312" ID="Freemind_Link_538968746" MODIFIED="1218886451343" TEXT="motog">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
<node COLOR="#990000" CREATED="1218875739703" ID="Freemind_Link_761507098" MODIFIED="1218886454703" TEXT="calculation">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node COLOR="#111111" CREATED="1218875792750" ID="Freemind_Link_1549003099" MODIFIED="1218875795000" TEXT="lean angle"/>
<node COLOR="#111111" CREATED="1218876028328" ID="Freemind_Link_285380928" MODIFIED="1218876029843" TEXT="g-force"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1218872934796" ID="Freemind_Link_799154131" MODIFIED="1218875991078" TEXT="simple state machine">
<arrowlink DESTINATION="Freemind_Link_538968746" ENDARROW="Default" ENDINCLINATION="72;0;" ID="Freemind_Arrow_Link_206884795" STARTARROW="None" STARTINCLINATION="72;0;"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1218874381343" ID="Freemind_Link_1202683440" MODIFIED="1218874388906" TEXT="lean angle">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1218874390609" ID="Freemind_Link_632647540" MODIFIED="1218874393484" TEXT="g-forces">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1218873050203" ID="Freemind_Link_1750895378" MODIFIED="1218873054875" POSITION="right" TEXT="Graphics">
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
</node>
<node COLOR="#0033ff" CREATED="1218876367109" ID="Freemind_Link_1738613506" MODIFIED="1218876384093" POSITION="left" TEXT="Display change">
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
</node>
</node>
</map>
