/*
Copyright (c) 2007-2009, Tomi Parviainen

Application specific code
*/

#include "includes.h"
#include "at91sam7x256.h"
#include "lcd.h"
#include "imu.h"

#include "bmp\numbers_s.h"
#include "bmp\numbers_l.h"

/* 21 = 14pix * 1.5B/pix */
#define NUMBERS_S_OFFSET 21

/* 66 = 44pix * 1.5B/pix */
#define NUMBERS_L_OFFSET 66

#define ANGLE_L_X 3
#define ANGLE_L_Y 4
#define ANGLE_R_X 67
#define ANGLE_R_Y 4


/*
Structure that holds the most recent maximum values from the IMU
*/
typedef struct {
    /* Maximum values in period of time */
    sint16_t max_l_p; /* zero or positive */
    sint16_t max_r_p; /* zero or negative */

    /* Maximum values since application started */
    sint16_t max_l; /* zero or positive */
    sint16_t max_r; /* zero or negative */

    /* Current lean angle */
    sint32_t angle;

    uint32_t update;
} data_buffer_t;

#define MAX_L_P 0x01
#define MAX_R_P 0x02
#define MAX_L   0x04
#define MAX_R   0x08
#define ANGLE   0x10


/*
Variable declaration(s)
*/
static timer_t update_values;
static data_buffer_t data;
static mailbox_t display_task;
static update_area_t numbers_s_img;


/*
Function prototypes(s)
*/
static void app_task(any_t* param);
static void app_update_values(any_t *param);
static void app_update_numbers_s(uint16_t x, uint16_t y, sint32_t num);


/*
APP trace enable macro
*/
#define TR_APP(a) a

/*
Defines the maximum display update frequency in seconds. Update frequency can
be smaller if there is no data to update to display.
*/
#define DISPLAY_UPDATE_RATE 10


/*
Initialises AD converters to be able to read sensor data and LCD.
*/
void app_init()
    {

    tcb_t* tcb;

    tcb = task_create(app_task, NULL, 512, 1, "MotoG");
    ASSERT(tcb != NULL);

    mailbox_init(&display_task, 1);

    /* Enable peripheral clocks */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_ADC)
                             | (1 << AT91C_ID_PIOA)
                             | (1 << AT91C_ID_PIOB)
                             | (1 << AT91C_ID_SPI0);

    /* Enable backlight control directly via PB20 IO (PWM functionality not
       used) and enable output functionality */
    AT91C_BASE_PIOB->PIO_SODR = (1 << 20);
    AT91C_BASE_PIOB->PIO_OER = (1 << 20);

    /* Enable LCD i.e. release it from RESET, this is controlled via PA2.
       Please note that RESET is active on LOW level! */
    AT91C_BASE_PIOA->PIO_SODR = (1 << 2);
    AT91C_BASE_PIOA->PIO_OER = (1 << 2);

    /* Initialise SPI bus i.e. disable the PIO from controlling the pin and
       multiplex functionality to SPI. MISO cannot be used with the LCD because
       the controller does not support data read in serial mode! */
    AT91C_BASE_PIOA->PIO_PDR = (1 << 18)  /* SCLK - SCL */
                             | (1 << 17)  /* MOSI - SI */
                             | (1 << 16)  /* MISO - NC */
                             | (1 << 12); /* CS   - CS */
    AT91C_BASE_PIOA->PIO_ASR = (1 << 18)
                             | (1 << 17)
                             | (1 << 16)
                             | (1 << 12);

    /* Enable the SPI to transfer and receive data */
    AT91C_BASE_SPI0->SPI_CR = AT91C_SPI_SPIEN;

    /* Master mode, fixed peripheral select, mode fault detection disabled
       peripheral 0 selected i.e. 0 at offset 16 */
    AT91C_BASE_SPI0->SPI_MR = AT91C_SPI_MSTR
                            | AT91C_SPI_PS_FIXED
                            | AT91C_SPI_MODFDIS
                            | (AT91C_SPI_PCS & (0 << 16));

    /* The inactive state value of SPCK is logic level one, 9 bits per transfer
       6MHz SPI clock, delay SPCK after CS asserted, delay between consecutive
       transfers */
    AT91C_BASE_SPI0->SPI_CSR[0] = AT91C_SPI_CPOL
                                | AT91C_SPI_BITS_9
                                | (AT91C_SPI_SCBR & (8 << 8)) /* 6 MHz */
                                | (AT91C_SPI_DLYBS & (1 << 16))
                                | (AT91C_SPI_DLYBCT & (1 << 24));

    /* Set the conversion parameters i.e. maximum startup and hold times */
    AT91C_BASE_ADC->ADC_MR = AT91C_ADC_TRGEN_DIS
                           | AT91C_ADC_LOWRES_10_BIT
                           | AT91C_ADC_SLEEP_NORMAL_MODE
                           | (AT91C_ADC_PRESCAL & (0xF << 8))
                           | (AT91C_ADC_STARTUP & (0x1F << 16))
                           | (AT91C_ADC_SHTIM & (0xF << 24));

    /* Hardware initialisation (VRef is not connected from sensor, however the
       output from the Vref is 1.23V i.e. IDG-300 reference voltage!):
       - AD0 -> X-Rate (PB27)
       - AD1 -> Y-Rate (PB28)
       - AD2 -> Z-Axis (PB29)
       - AD3 -> Y-Axis (PB30)
       - AD4 -> X-Axis
     */

    /* Disable the PIO from controlling the IO pins, AD4 is not behind PIO
       controller thus it is automatically AD pin */
    AT91C_BASE_PIOB->PIO_PDR = (1 << 27)
                             | (1 << 28)
                             | (1 << 29)
                             | (1 << 30);

    /* Enable ADC channels */
    AT91C_BASE_ADC->ADC_CHER = AT91C_ADC_CH0
                             | AT91C_ADC_CH1
                             | AT91C_ADC_CH2
                             | AT91C_ADC_CH3
                             | AT91C_ADC_CH4;

    /* Enable the End Of Conversion (EOC) IRQ from the last used channel.
       The assumption here is that conversion of the enabled channels is
       performed starting from the lowest enabled channel towards the last
       enabled channel -> when IRQ occurs the conversion of each channel
       is ready. */
    AT91C_BASE_ADC->ADC_IER = AT91C_ADC_CH4;

    memory_set(&data, 0x00, sizeof(data));

    data.max_r = -20;
    data.max_l = +20;

    imu_init();

    }


/*
Reads the latest values from IMU and if the minimum / maximum values are
changed then posts a message to mailbox to initiate display update.
*/
void app_update_values(any_t *param)
    {

    sint16_t max_r = 0;
    sint16_t max_l = 0;

    timer_start(&update_values, 1000 / DISPLAY_UPDATE_RATE);

    /* Calculate the maximum values for period of time. Buffer either contains
       uninitialised (0) values or +- values for lean angle */
    for (uint16_t cnt = 0; cnt < BUFFER_SIZE; cnt++)
        {
        sint16_t value = acceleration_g[cnt];

        if (value > max_l)
            {
            max_l = value;
            }
        else if (value < max_r)
            {
            max_r = value;
            }
        }

    /* New maximum value for this period of time */
    if (data.max_l_p != max_l)
        {
        data.update |= MAX_L_P;
        data.max_l_p = max_l;

        if (max_l > data.max_l)
            {
            /* New absolute maximum value */
            data.update |= MAX_L;
            data.max_l = max_l;
            }
        }

    if (data.max_r_p != max_r)
        {
        data.update |= MAX_R_P;
        data.max_r_p = max_r;

        if (max_r < data.max_r)
            {
            data.update |= MAX_R;
            data.max_r = max_r;
            }
        }

    if (data.angle != lean_angle)
        {
        data.update |= ANGLE;
        data.angle = lean_angle;
        }

    if (data.update != 0)
        {
        /* The content of the display need to be updated */
        mailbox_post(&display_task, (any_t*)0xF00D);
        }

    }


/*
*/
void app_task(any_t* param)
    {

    //update_area_t numbers_s_img;
    update_area_t numbers_l_img;

    TR_APP(TR_FUNCTION);

    lcd_init_controller();
    lcd_clear_screen();

    task_sleep(500);

    numbers_s_img.area = NULL;
    numbers_s_img.width = 14;
    numbers_s_img.height = 18;
    numbers_s_img.skip = 126;

    numbers_l_img.area = NULL;
    numbers_l_img.width = 44;
    numbers_l_img.height = 68 / 2;
    numbers_l_img.skip = 396;

    timer_init(&update_values, app_update_values, NULL);
    timer_start(&update_values, 1000 / DISPLAY_UPDATE_RATE);

    data.update = 0xFF;

    sint32_t p_angle = 0;

    for (;;)
        {
        uint32_t update;
        uint32_t irq_bits;

        /* Wait until there is data to update onto display */
        mailbox_pend(&display_task);

        /* Read what values are updated */
        irq_bits = irq_disable_core();
        update = data.update;
        data.update = 0;
        irq_enable_core(irq_bits);

        if (update & ANGLE)
            {
            sint32_t c_angle = data.angle;
            sint32_t tens, ones;

            if (c_angle < 0)
                {
                tens = -c_angle / 10;
                ones = -c_angle - tens * 10;
                }
            else
                {
                tens = c_angle / 10;
                ones = c_angle - tens * 10;
                }
trace_print("ANGLE");
            // pos = 21,31
            numbers_l_img.x = 21;
            numbers_l_img.y = 31;
            numbers_l_img.area = &numbers_l_bmp[tens * NUMBERS_L_OFFSET];
            lcd_screen_to_display(&numbers_l_img);

            numbers_l_img.x = 21 + 44;
            numbers_l_img.area = &numbers_l_bmp[ones * NUMBERS_L_OFFSET];
            lcd_screen_to_display(&numbers_l_img);

            p_angle = c_angle;
            }

        if (update & MAX_L_P)
            {
/*
            sint32_t max = data.max_l_p;
            sint32_t tens, ones;

            tens = max / 10;
            ones = max - tens * 10;
*/
trace_print("LP");
            // pos = 4,6
            // app_update_numbers_s(x, y, num);
            app_update_numbers_s(4, 6, data.max_l_p);
/*
            numbers_s_img.x = 4;
            numbers_s_img.y = 6;
            numbers_s_img.area = &numbers_s_bmp[tens * NUMBERS_S_OFFSET];
            lcd_screen_to_display(&numbers_s_img);

            numbers_s_img.x = 4 + 14;
            numbers_s_img.area = &numbers_s_bmp[ones * NUMBERS_S_OFFSET];
            lcd_screen_to_display(&numbers_s_img);
*/
            }

        if (update & MAX_R_P)
            {
            sint32_t max = -data.max_r_p;
            sint32_t tens, ones;

            tens = max / 10;
            ones = max - tens * 10;
trace_print("RP");
            // pos = 98,6
            numbers_s_img.x = 98;
            numbers_s_img.y = 6;
            numbers_s_img.area = &numbers_s_bmp[tens * NUMBERS_S_OFFSET];
            lcd_screen_to_display(&numbers_s_img);

            numbers_s_img.x = 98 + 14;
            numbers_s_img.area = &numbers_s_bmp[ones * NUMBERS_S_OFFSET];
            lcd_screen_to_display(&numbers_s_img);
            }

        if (update & MAX_L)
            {
            sint32_t max = data.max_l;
            sint32_t tens, ones;

            tens = max / 10;
            ones = max - tens * 10;
trace_print("L");
            // pos = 4,106
            numbers_s_img.x = 4;
            numbers_s_img.y = 106;
            numbers_s_img.area = &numbers_s_bmp[tens * NUMBERS_S_OFFSET];
            lcd_screen_to_display(&numbers_s_img);

            numbers_s_img.x = 4 + 14;
            numbers_s_img.area = &numbers_s_bmp[ones * NUMBERS_S_OFFSET];
            lcd_screen_to_display(&numbers_s_img);
            }

        if (update & MAX_R)
            {
            sint32_t max = -data.max_r;
            sint32_t tens, ones;

            tens = max / 10;
            ones = max - tens * 10;
trace_print("R");
            // pos = 98,106
            numbers_s_img.x = 98;
            numbers_s_img.y = 106;
            numbers_s_img.area = &numbers_s_bmp[tens * NUMBERS_S_OFFSET];
            lcd_screen_to_display(&numbers_s_img);

            numbers_s_img.x = 98 + 14;
            numbers_s_img.area = &numbers_s_bmp[ones * NUMBERS_S_OFFSET];
            lcd_screen_to_display(&numbers_s_img);
            }

        }

    }


/*
*/
void app_update_numbers_s(uint16_t x, uint16_t y, sint32_t num)
    {

    sint32_t tens, ones;

    /* Only positive numbers are allowed */
    ASSERT(num > 0);

    tens = num / 10;
    ones = num - tens * 10;

    /* Display first number */
    numbers_s_img.x = x;
    numbers_s_img.y = y;
    numbers_s_img.area = &numbers_s_bmp[tens * NUMBERS_S_OFFSET];
    lcd_screen_to_display(&numbers_s_img);

    /* Display second number (offset to first one is 14 pixel) */
    numbers_s_img.x = x + 14;
    numbers_s_img.area = &numbers_s_bmp[ones * NUMBERS_S_OFFSET];
    lcd_screen_to_display(&numbers_s_img);

    }
