/*
Copyright (c) 2007-2009, Tomi Parviainen

Application specific code
*/

#include "includes.h"
#include "at91sam7x256.h"
#include "lcd.h"
#include "imu.h"

#include "bmp\angle_l.h"
#include "bmp\angle_r.h"
#include "bmp\middle.h"
#include "bmp\numbers.h"

#define ANGLE_W 60
#define ANGLE_H 42
#define ANGLE_L_X 3
#define ANGLE_L_Y 4
#define ANGLE_R_X 67
#define ANGLE_R_Y 4


/*
Structure that holds the most recent maximum values from the IMU
*/
typedef struct {
    /* Maximum values in period of time */
    sint16_t max_l_p; /* zero or positive */
    sint16_t max_r_p; /* zero or negative */

    /* Maximum values since application started */
    sint16_t max_l; /* zero or positive */
    sint16_t max_r; /* zero or negative */

    /* Current lean angle */
    sint32_t angle;

    uint32_t update;
} data_buffer_t;

#define MAX_L_P 0x01
#define MAX_R_P 0x02
#define MAX_L   0x04
#define MAX_R   0x08
#define ANGLE   0x10


/*
Variable declaration(s)
*/
static timer_t update_values;
static data_buffer_t data;
static mailbox_t display_task;


/*
Function prototypes(s)
*/
static void app_task(any_t* param);
static void app_update_values(any_t *param);


/*
APP trace enable macro
*/
#define TR_APP(a)

/*
Defines the maximum display update frequency in seconds. Update frequency can
be smaller if there is no data to update to display.
*/
#define DISPLAY_UPDATE_RATE 10


/*
Initialises AD converters to be able to read sensor data and LCD.
*/
void app_init()
    {

    tcb_t* tcb;

    tcb = task_create(app_task, NULL, 512, 1, "MotoG");
    ASSERT(tcb != NULL);

    mailbox_init(&display_task, 1);

    /* Enable peripheral clocks */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_ADC)
                             | (1 << AT91C_ID_PIOA)
                             | (1 << AT91C_ID_PIOB)
                             | (1 << AT91C_ID_SPI0);

    /* Enable backlight control directly via PB20 IO (PWM functionality not
       used) and enable output functionality */
    AT91C_BASE_PIOB->PIO_SODR = (1 << 20);
    AT91C_BASE_PIOB->PIO_OER = (1 << 20);

    /* Enable LCD i.e. release it from RESET, this is controlled via PA2.
       Please note that RESET is active on LOW level! */
    AT91C_BASE_PIOA->PIO_SODR = (1 << 2);
    AT91C_BASE_PIOA->PIO_OER = (1 << 2);

    /* Initialise SPI bus i.e. disable the PIO from controlling the pin and
       multiplex functionality to SPI. MISO cannot be used with the LCD because
       the controller does not support data read in serial mode! */
    AT91C_BASE_PIOA->PIO_PDR = (1 << 18)  /* SCLK - SCL */
                             | (1 << 17)  /* MOSI - SI */
                             | (1 << 16)  /* MISO - NC */
                             | (1 << 12); /* CS   - CS */
    AT91C_BASE_PIOA->PIO_ASR = (1 << 18)
                             | (1 << 17)
                             | (1 << 16)
                             | (1 << 12);

    /* Enable the SPI to transfer and receive data */
    AT91C_BASE_SPI0->SPI_CR = AT91C_SPI_SPIEN;

    /* Master mode, fixed peripheral select, mode fault detection disabled
       peripheral 0 selected i.e. 0 at offset 16 */
    AT91C_BASE_SPI0->SPI_MR = AT91C_SPI_MSTR
                            | AT91C_SPI_PS_FIXED
                            | AT91C_SPI_MODFDIS
                            | (AT91C_SPI_PCS & (0 << 16));

    /* The inactive state value of SPCK is logic level one, 9 bits per transfer
       6MHz SPI clock, delay SPCK after CS asserted, delay between consecutive
       transfers */
    AT91C_BASE_SPI0->SPI_CSR[0] = AT91C_SPI_CPOL
                                | AT91C_SPI_BITS_9
                                | (AT91C_SPI_SCBR & (8 << 8)) /* 6 MHz */
                                | (AT91C_SPI_DLYBS & (1 << 16))
                                | (AT91C_SPI_DLYBCT & (1 << 24));

    /* Set the conversion parameters i.e. maximum startup and hold times */
    AT91C_BASE_ADC->ADC_MR = AT91C_ADC_TRGEN_DIS
                           | AT91C_ADC_LOWRES_10_BIT
                           | AT91C_ADC_SLEEP_NORMAL_MODE
                           | (AT91C_ADC_PRESCAL & (0xF << 8))
                           | (AT91C_ADC_STARTUP & (0x1F << 16))
                           | (AT91C_ADC_SHTIM & (0xF << 24));

    /* Hardware initialisation (VRef is not connected from sensor, however the
       output from the Vref is 1.23V i.e. IDG-300 reference voltage!):
       - AD0 -> X-Rate (PB27)
       - AD1 -> Y-Rate (PB28)
       - AD2 -> Z-Axis (PB29)
       - AD3 -> Y-Axis (PB30)
       - AD4 -> X-Axis
     */

    /* Disable the PIO from controlling the IO pins, AD4 is not behind PIO
       controller thus it is automatically AD pin */
    AT91C_BASE_PIOB->PIO_PDR = (1 << 27)
                             | (1 << 28)
                             | (1 << 29)
                             | (1 << 30);

    /* Enable ADC channels */
    AT91C_BASE_ADC->ADC_CHER = AT91C_ADC_CH0
                             | AT91C_ADC_CH1
                             | AT91C_ADC_CH2
                             | AT91C_ADC_CH3
                             | AT91C_ADC_CH4;

    /* Enable the End Of Conversion (EOC) IRQ from the last used channel.
       The assumption here is that conversion of the enabled channels is
       performed starting from the lowest enabled channel towards the last
       enabled channel -> when IRQ occurs the conversion of each channel
       is ready. */
    AT91C_BASE_ADC->ADC_IER = AT91C_ADC_CH4;

    memory_set(&data, 0x00, sizeof(data));

    data.max_r = -20;
    data.max_l = +20;

    imu_init();

    }


/*
Reads the latest values from IMU and if the minimum / maximum values are
changed then posts a message to mailbox to initiate display update.
*/
void app_update_values(any_t *param)
    {

    sint16_t max_r = 0;
    sint16_t max_l = 0;

    timer_start(&update_values, 1000 / DISPLAY_UPDATE_RATE);

    /* Calculate the maximum values for period of time. Buffer either contains
       uninitialised (0) values or +- values for lean angle */
    for (uint16_t cnt = 0; cnt < BUFFER_SIZE; cnt++)
        {
        sint16_t value = acceleration_g[cnt];

        if (value > max_l)
            {
            max_l = value;
            }
        else if (value < max_r)
            {
            max_r = value;
            }
        }

    /* New maximum value for this period of time */
    if (data.max_l_p != max_l)
        {
        data.update |= MAX_L_P;
        data.max_l_p = max_l;

        if (max_l > data.max_l)
            {
            /* New absolute maximum value */
            data.update |= MAX_L;
            data.max_l = max_l;
            }
        }

    if (data.max_r_p != max_r)
        {
        data.update |= MAX_R_P;
        data.max_r_p = max_r;

        if (max_r < data.max_r)
            {
            data.update |= MAX_R;
            data.max_r = max_r;
            }
        }

    if (data.angle != lean_angle)
        {
        data.update |= ANGLE;
        data.angle = lean_angle;
        }

    if (data.update != 0)
        {
        /* The content of the display need to be updated */
        mailbox_post(&display_task, (any_t*)0xF00D);
        }

    }


/*
*/
void app_task(any_t* param)
    {

    //update_area_t screen;
    update_area_t l_angle_img, l_angle_buf;
    update_area_t r_angle_img, r_angle_buf;
    update_area_t middle;
    update_area_t numbers_img;

/*
    update_area_t numbers_s_img, numbers_s_buf;

    numbers_s_img.area = numbers_bmp[NUM * xyz];
    numbers_s_buf.area = memory_alloc(30 * 18 * 1.5);
*/

    TR_APP(TR_FUNCTION);

    lcd_init_controller();
#if 0
    /* Allocate/initialise RAM for screen buffer */
    screen.area = memory_alloc(IMG_SIZE);
    ASSERT(screen.area != NULL);

    memory_set((any_t*)screen.area, 0x0, IMG_SIZE);
    trace_print_hex("area", (uint32_t)screen.area);

    screen.width = MAX_X;
    screen.height = MAX_Y;
    screen.skip = 0;
    screen.x = 0;
    screen.y = 0;
#endif

    lcd_clear_screen();

    task_sleep(500);

    numbers_img.area = numbers_bmp;
    numbers_img.width = 245;
    numbers_img.height = 30;
    numbers_img.skip = 0;
    numbers_img.x = 0;
    numbers_img.y = 0;

    l_angle_img.area = angle_l;
    l_angle_img.width = ANGLE_W;
    l_angle_img.height = ANGLE_H;
    l_angle_img.skip = 0;
    l_angle_img.x = 0;
    l_angle_img.y = 0;

    l_angle_buf.area = memory_alloc(ANGLE_W * ANGLE_H);
    ASSERT(l_angle_buf.area != NULL);
    l_angle_buf.width = ANGLE_W;
    l_angle_buf.height = ANGLE_H;
    l_angle_buf.skip = 0;

    l_angle_buf.x = 0;  /* Location in target buffer */
    l_angle_buf.y = 0;
    lcd_copy(&l_angle_img, &l_angle_buf);
    l_angle_buf.x = ANGLE_L_X;  /* Location in display buffer */
    l_angle_buf.y = ANGLE_L_Y;
    lcd_screen_to_display(&l_angle_buf);


    r_angle_img.area = angle_r;
    r_angle_img.width = ANGLE_W;
    r_angle_img.height = ANGLE_H;
    r_angle_img.skip = 0;
    r_angle_img.x = 0;
    r_angle_img.y = 0;

    r_angle_buf.area = memory_alloc(ANGLE_W * ANGLE_H);
    ASSERT(r_angle_buf.area != NULL);
    r_angle_buf.width = ANGLE_W;
    r_angle_buf.height = ANGLE_H;
    r_angle_buf.skip = 0;

    r_angle_buf.x = 0;  /* Location in target buffer */
    r_angle_buf.y = 0;
    lcd_copy(&r_angle_img, &r_angle_buf);
    r_angle_buf.x = ANGLE_R_X; /* Location in display buffer */
    r_angle_buf.y = ANGLE_R_Y;
    lcd_screen_to_display(&r_angle_buf);


    middle.area = bmp_middle;
    middle.width = 4;
    middle.height = ANGLE_H;
    middle.skip = 0;
    middle.x = 63;
    middle.y = 4;
    lcd_screen_to_display(&middle);


//    lcd_line_draw(&screen, 0, MAX_Y / 2, MAX_X - 1, MAX_Y / 2, 0xFFF);
//    lcd_screen_to_display(&screen);

    timer_init(&update_values, app_update_values, NULL);
    timer_start(&update_values, 1000 / DISPLAY_UPDATE_RATE);

//    lcd_copy(&l_angle, &screen);

    sint32_t p_angle = 0;

    for (;;)
        {
//        uint32_t mul = 1;
//        uint8_t sp = 0;
        uint32_t update;
        uint32_t irq_bits;

        /* Wait until there is data to update onto display */
        mailbox_pend(&display_task);

        irq_bits = irq_disable_core();
        update = data.update;
        data.update = 0;
        irq_enable_core(irq_bits);

        if (update & ANGLE)
            {
            sint32_t c_angle = data.angle;

            if (c_angle < 0 || p_angle < 0) /* Right */
                {
                sint16_t length = ANGLE_W * ((float)c_angle / (float)data.max_r);
                if (c_angle > 0)
                    {
                    length = 0;
                    }
                length = length & 0xFFFFFFFE;

                memory_set((any_t*)r_angle_buf.area, 0x0, ANGLE_W * ANGLE_H * 1.5);
                r_angle_buf.x = 0;  /* Location in the target buffer */
                r_angle_buf.y = 0;
                r_angle_img.width = length;
                r_angle_img.skip = ANGLE_W - length;
                lcd_copy(&r_angle_img, &r_angle_buf);
                r_angle_buf.x = ANGLE_R_X; /* Location in the display buffer */
                r_angle_buf.y = ANGLE_R_Y;
                r_angle_img.width = ANGLE_W;
                r_angle_img.skip = 0;
                lcd_screen_to_display(&r_angle_buf);
                }

            if (c_angle > 0 || p_angle > 0) /* Left */
                {
                sint16_t length = ANGLE_W * ((float)c_angle / (float)data.max_l);
                if (c_angle < 0)
                    {
                    length = 0;
                    }
                length = length & 0xFFFFFFFE;

                l_angle_buf.x = 0;  /* Location in the target buffer */
                l_angle_buf.y = 0;
                lcd_copy(&l_angle_img, &l_angle_buf);
                l_angle_buf.x = ANGLE_L_X; /* Location in the display buffer */
                l_angle_buf.y = ANGLE_L_Y;

                l_angle_buf.width = ANGLE_W - length;
                l_angle_buf.skip = length;
                lcd_box_fill(&l_angle_buf, 0x00);
                l_angle_buf.width = ANGLE_W;
                l_angle_buf.skip = 0;
                lcd_screen_to_display(&l_angle_buf);
                }

            p_angle = c_angle;

            /* Update numbers */
            if (c_angle < 0)
                {
                c_angle = -c_angle;
                }

            trace_print_dec("c_angle", c_angle);

            }

        if (0) //update & MAX_L_P)
            {
            sint16_t length = data.max_l;
            if (length < 20)
                {
                length = 20;
                }
            length = ANGLE_W * (float)(data.max_l_p / (float)length);
            length = length & 0xFFFFFFFE;

            l_angle_buf.x = 0;  /* Location in the target buffer */
            l_angle_buf.y = 0;
            lcd_copy(&l_angle_img, &l_angle_buf);
            l_angle_buf.x = ANGLE_L_X; /* Location in the display buffer */
            l_angle_buf.y = ANGLE_L_Y;

            l_angle_buf.width = ANGLE_W - length;
            l_angle_buf.skip = length;
            lcd_box_fill(&l_angle_buf, 0x00);
            l_angle_buf.width = ANGLE_W;
            l_angle_buf.skip = 0;
            lcd_screen_to_display(&l_angle_buf);
            }

        if (0) //update & MAX_R_P)
            {
            sint16_t length = data.max_r;
            if (length > -20)
                {
                length = -20;
                }
            length = ANGLE_W - ANGLE_W * (float)(data.max_r_p / (float)length);

            memory_set((any_t*)r_angle_buf.area, 0x0, ANGLE_W * ANGLE_H * 1.5);
            r_angle_buf.x = 0;  /* Location in the target buffer */
            r_angle_buf.y = 0;
            r_angle_img.width = ANGLE_W - length;
            r_angle_img.skip = length;
            lcd_copy(&r_angle_img, &r_angle_buf);
            r_angle_buf.x = ANGLE_R_X; /* Location in the display buffer */
            r_angle_buf.y = ANGLE_R_Y;
            r_angle_img.width = ANGLE_W;
            r_angle_img.skip = 0;
            lcd_screen_to_display(&r_angle_buf);
            }

/*
        trace_print("");

        trace_print_dec("lmp", data.max_l_p);
        trace_print_dec("rmp", data.max_r_p * (-1));
        trace_print_dec("lma", data.max_l);
        trace_print_dec("rma", data.max_r * (-1));
        if (data.angle < 0) mul = -1;
        trace_print_dec("cav", data.angle * mul);
*/
        /* Update the content of screen buffer */
        //memory_set((any_t*)screen.area, 0x0, IMG_SIZE);

        /*
        rmp = 0
        rma = 20
        */
/*
        if (data.max_r > -20)
            {
            sp = ANGLE_W - ANGLE_W * (float)(data.max_r_p / -20.0);
            }
        else
            {
            sp = ANGLE_W - ANGLE_W * (float)(data.max_r_p / (float)data.max_r);
            }

trace_print_dec("sp", sp);

        r_angle_img.width = ANGLE_W - sp;
        r_angle_img.skip = 0 + sp;
*/
        //lcd_copy(&r_angle_img, &screen);

        //lcd_screen_to_display(&screen);
        }

    }


#if 0
/*
*/
void app_task(any_t* param)
    {

    uint32_t cnt = 0;
    update_area_t screen;
//    update_area_t l_angle;
//    update_area_t r_angle;

    TR_APP(TR_FUNCTION);

    lcd_init_controller();

    /* Allocate/initialise RAM for screen buffer */
    screen.area = memory_alloc(IMG_SIZE);
    ASSERT(screen.area != NULL);
    memory_set((any_t*)screen.area, 0x0, IMG_SIZE);
    trace_print_hex("area", (uint32_t)screen.area);
    screen.width = MAX_X;
    screen.height = MAX_Y;
    screen.skip = 0;
    screen.x = 0;
    screen.y = 0;
#if 0
    l_angle.area = angle_l;
    l_angle.width = ANGLE_W;
    l_angle.height = ANGLE_H;
    l_angle.skip = 0;
    l_angle.x = 5;
    l_angle.y = 4;

    r_angle.area = angle_r;
    r_angle.width = ANGLE_W;
    r_angle.height = ANGLE_H;
    r_angle.skip = 0;
    r_angle.x = 65;
    r_angle.y = 4;

    lcd_copy(&l_angle, &screen);
    lcd_copy(&r_angle, &screen);
#endif

    lcd_line_draw(&screen, 0, MAX_Y / 2, MAX_X - 1, MAX_Y / 2, 0xFFF);

    lcd_screen_to_display(&screen);

    for (;;)
        {
        /* Shift the table */
        if (++cnt == MAX_X)
            {
            cnt = 0;
            memory_set((any_t*)screen.area, 0x0, IMG_SIZE);
            lcd_line_draw(&screen, 0, MAX_Y / 2, MAX_X - 1, MAX_Y / 2, 0xFFF);
            }

        semaphore_wait(&data_ready);

        lcd_pixel_draw(&screen, cnt, data.raw_acc_x / 8, 0xF00);
        lcd_pixel_draw(&screen, cnt, data.raw_acc_y / 8, 0x0F0);
        lcd_pixel_draw(&screen, cnt, data.raw_acc_z / 8, 0x00F);

        lcd_screen_to_display(&screen);
        }

    }
#endif
