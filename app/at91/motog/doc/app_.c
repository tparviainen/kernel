/*
Copyright (c) 2007-2008, Tomi Parviainen

Application specific code
*/

#include <math.h> /* atan2, M_PI, ... */
#include "includes.h"
#include "at91sam7x256.h"
#include "lcd.h"
#include "ars.h"
//#include "bmp\acceleration.h"


/*
Inertia Measurement Unit (IMU) data structure. RAW data from sensor is stored
to this structure.

Ratiometric - the output sensitivity (or scale factor) varies proportionally
              to the supply voltage.

Non-ratiometric - the scale factor is calibrated at the factory and is
                  nominally independent of supply voltage.

IMU 5 Degrees of Freedom:
- IDG-300 - dual axis gyro (non-ratiometric)
- ADXL330 - accelerometer (ratiometric)
*/
typedef struct {
    uint32_t raw_acc_x;     /* 0x000 - 0x400 = 10-bit raw data */
    uint32_t raw_acc_y;
    uint32_t raw_acc_z;
    uint32_t raw_gyro_x;    /* roll */
    uint32_t raw_gyro_y;    /* pitch */
    uint32_t raw_gyro_z;    /* yaw */
} imu_data_t;


/*
Structure, which holds the object's acceleration expressed in Gs
*/
typedef struct {
    float x;
    float y;
    float z;
} g_force_t;


/*
Structure, which holds the object's angular rate expressed in radians
*/
typedef struct {
    float x;
    float y;
    float z;
} gyro_roll_t;


/*
Variable declaration(s)
*/
static imu_data_t data;
static semaphore_t data_ready;


/*
Function prototypes(s)
*/
static void app_task(any_t* param);
static void app_adc_conversion_ready(void);
static void app_lean_angle(update_area_t* screen);
static void app_calculate_g_forces(imu_data_t* raw, g_force_t* g_f);
void app_gyro_roll(imu_data_t* raw, gyro_roll_t* gyro);
uint32_t app_get_diff(void);


/*
APP trace enable macro
*/
#define TR_APP(a)


/*
Initialises AD converters to be able to read sensor data and LCD.
*/
void app_init()
    {

    tcb_t* tcb;
    irq_fn func;

    tcb = task_create(app_task, NULL, 512, 1, "MotoG");
    ASSERT(tcb != NULL);

    /* Enable peripheral clocks */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_ADC)
                             | (1 << AT91C_ID_PIOA)
                             | (1 << AT91C_ID_PIOB)
                             | (1 << AT91C_ID_SPI0);

    /* Enable backlight control directly via PB20 IO (PWM functionality not
       used) and enable output functionality */
    AT91C_BASE_PIOB->PIO_SODR = (1 << 20);
    AT91C_BASE_PIOB->PIO_OER = (1 << 20);

    /* Enable LCD i.e. release it from RESET, this is controlled via PA2.
       Please note that RESET is active on LOW level! */
    AT91C_BASE_PIOA->PIO_SODR = (1 << 2);
    AT91C_BASE_PIOA->PIO_OER = (1 << 2);

    /* Initialise SPI bus i.e. disable the PIO from controlling the pin and
       multiplex functionality to SPI. MISO cannot be used with the LCD because
       the controller does not support data read in serial mode! */
    AT91C_BASE_PIOA->PIO_PDR = (1 << 18)  /* SCLK - SCL */
                             | (1 << 17)  /* MOSI - SI */
                             | (1 << 16)  /* MISO - NC */
                             | (1 << 12); /* CS   - CS */
    AT91C_BASE_PIOA->PIO_ASR = (1 << 18)
                             | (1 << 17)
                             | (1 << 16)
                             | (1 << 12);

    /* Enable the SPI to transfer and receive data */
    AT91C_BASE_SPI0->SPI_CR = AT91C_SPI_SPIEN;

    /* Master mode, fixed peripheral select, mode fault detection disabled
       peripheral 0 selected i.e. 0 at offset 16 */
    AT91C_BASE_SPI0->SPI_MR = AT91C_SPI_MSTR
                            | AT91C_SPI_PS_FIXED
                            | AT91C_SPI_MODFDIS
                            | (AT91C_SPI_PCS & (0 << 16));

    /* The inactive state value of SPCK is logic level one, 9 bits per transfer
       6MHz SPI clock, delay SPCK after CS asserted, delay between consecutive
       transfers */
    AT91C_BASE_SPI0->SPI_CSR[0] = AT91C_SPI_CPOL
                                | AT91C_SPI_BITS_9
                                | (AT91C_SPI_SCBR & (8 << 8)) /* 6 MHz */
                                | (AT91C_SPI_DLYBS & (1 << 16))
                                | (AT91C_SPI_DLYBCT & (1 << 24));

    /* Set the conversion parameters i.e. maximum startup and hold times */
    AT91C_BASE_ADC->ADC_MR = AT91C_ADC_TRGEN_DIS
                           | AT91C_ADC_LOWRES_10_BIT
                           | AT91C_ADC_SLEEP_NORMAL_MODE
                           | (AT91C_ADC_PRESCAL & (0xF << 8))
                           | (AT91C_ADC_STARTUP & (0x1F << 16))
                           | (AT91C_ADC_SHTIM & (0xF << 24));

    /* Hardware initialisation (VRef is not connected from sensor, however the
       output from the Vref is 1.23V i.e. IDG-300 reference voltage!):
       - AD0 -> X-Rate (PB27)
       - AD1 -> Y-Rate (PB28)
       - AD2 -> Z-Axis (PB29)
       - AD3 -> Y-Axis (PB30)
       - AD4 -> X-Axis
     */

    /* Disable the PIO from controlling the IO pins, AD4 is not behind PIO
       controller thus it is automatically AD pin */
    AT91C_BASE_PIOB->PIO_PDR = (1 << 27)
                             | (1 << 28)
                             | (1 << 29)
                             | (1 << 30);

    /* Enable ADC channels */
    AT91C_BASE_ADC->ADC_CHER = AT91C_ADC_CH0
                             | AT91C_ADC_CH1
                             | AT91C_ADC_CH2
                             | AT91C_ADC_CH3
                             | AT91C_ADC_CH4;

    semaphore_init(&data_ready, 0);

    func = irq_bind(ADC, app_adc_conversion_ready);
    ASSERT(func == NULL);

    /* Enable the End Of Conversion (EOC) IRQ from the last used channel.
       The assumption here is that conversion of the enabled channels is
       performed starting from the lowest enabled channel towards the last
       enabled channel -> when IRQ occurs the conversion of each channel
       is ready. */
    AT91C_BASE_ADC->ADC_IER = AT91C_ADC_CH4;

    irq_enable(ADC);

    }


/*
*/
void app_task(any_t* param)
    {

    uint32_t cnt = 0;
    update_area_t screen;

    TR_APP(TR_FUNCTION);

    lcd_init_controller();

    /* Allocate/initialise RAM for screen buffer */
    screen.area = memory_alloc(IMG_SIZE);
    ASSERT(screen.area != NULL);
    memory_set((any_t*)screen.area, 0x0, IMG_SIZE);
    trace_print_hex("area", (uint32_t)screen.area);
    screen.width = MAX_X;
    screen.height = MAX_Y;
    screen.skip = 0;
    screen.x = 0;
    screen.y = 0;

    lcd_line_draw(&screen, 0, MAX_Y / 2, MAX_X - 1, MAX_Y / 2, 0xFFF);

    lcd_screen_to_display(&screen);

    for (;;)
        {
        task_sleep(100);

        /* Begin analog-to-digital conversion on enabled channels */
        AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;

        /* Shift the table */
        if (++cnt == MAX_X)
            {
            cnt = 0;
            memory_set((any_t*)screen.area, 0x0, IMG_SIZE);
            lcd_line_draw(&screen, 0, MAX_Y / 2, MAX_X - 1, MAX_Y / 2, 0xFFF);
            }

        semaphore_wait(&data_ready);

        /* Data available 0x000 - 0x400
           0g = 1.5V, 300mV/g => +- 2g = 0.9V - 2.1V
        */

        lcd_pixel_draw(&screen, cnt, data.raw_acc_x / 8, 0xF00);
        lcd_pixel_draw(&screen, cnt, data.raw_acc_y / 8, 0x0F0);
        lcd_pixel_draw(&screen, cnt, data.raw_acc_z / 8, 0x00F);

        lcd_screen_to_display(&screen);

if (1)
        app_lean_angle(&screen);
        }

    }


/*
Interrupt handler for ADC data ready interrupt. Reads the ADC data from HW
and stores it to global structure and signals semaphore to indicate that
data is available in structure.
*/
void app_adc_conversion_ready()
    {

    TR_APP(TR_FUNCTION);

    data.raw_acc_x = AT91C_BASE_ADC->ADC_CDR0;
    data.raw_acc_y = AT91C_BASE_ADC->ADC_CDR1;
    data.raw_acc_z = AT91C_BASE_ADC->ADC_CDR2;
    data.raw_gyro_x = AT91C_BASE_ADC->ADC_CDR3;
    data.raw_gyro_y = AT91C_BASE_ADC->ADC_CDR4;

    TR_APP(trace_print_dec("Raw Acc x", data.raw_acc_x));
    TR_APP(trace_print_dec("Raw Acc y", data.raw_acc_y));
    TR_APP(trace_print_dec("Raw Acc z", data.raw_acc_z));
    TR_APP(trace_print_dec("Raw Gyro x", data.raw_gyro_x));
    TR_APP(trace_print_dec("Raw Gyro y", data.raw_gyro_y));

    semaphore_signal(&data_ready);

    }

/*
*/
void app_lean_angle(update_area_t* screen)
    {

    /* Calculated data in radians */
    g_force_t acc;
    gyro_roll_t gyro;

    /* Accelerometer and Gyroscope calculated lean angles */
    float a_lean, g_lean;

    for (;;)
        {
        /* Begin analog-to-digital conversion on enabled channels */
        AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;

        /* Delay can be removed if the display update is called to consume time */
        task_sleep(1000 / 50);

        semaphore_wait(&data_ready);

        /* Convert the raw data to radians */
        app_gyro_roll(&data, &gyro);
        app_calculate_g_forces(&data, &acc);

        /* Calculate the lean angle (rad) based on the accelerometer data */
        //a_lean = -(atan2f(-acc.z, acc.y) - (M_PI / 2.0));
        a_lean = -(atan2f(-acc.z, acc.y) - (M_PI / 2.0));
#if 0
        if (a_lean < 0)
            trace_print_char('-');
        else
            trace_print_char('+');
        TR_APP(trace_print_dec("a_lean", 1000 * fabsf(a_lean)));
#endif

        ars_predict(gyro.y, app_get_diff() / 1000.0);
        g_lean = ars_update(a_lean);
#if 0
        /* This is needed (?) in order to correct the negative a_lean-value
           for positive before angle (0-360) calculation! */
        if (a_lean < 0)
            {
            a_lean = 2.0 * M_PI + a_lean;
            }
#endif
        /* Convert radians to degrees */
        a_lean *= (180.0 / M_PI);
        g_lean *= (180.0 / M_PI);

        if (a_lean < 0)
            trace_print_char('-');
        else
            trace_print_char('+');
        trace_print_dec("A", fabsf(a_lean));
        trace_print_char(' ');

        if (g_lean < 0)
            trace_print_char('-');
        else
            trace_print_char('+');
        trace_print_dec("G", fabsf(g_lean));
        trace_print_char(' ');

        trace_print_char(0xd);

#if 0
        trace_print_dec("G", g_lean);
        trace_print_dec(" A", a_lean);
        trace_print_char(' ');
        trace_print_char(' ');
        trace_print_char(0xd);
#endif
        }

    }


/*
Calculates the time difference in ms between the previous call of this
function and the current call.
*/
uint32_t app_get_diff()
    {

    time_t now;
    uint32_t diff;
    static time_t prev = {0, 0, 0, 0, 0};

    time_uptime(&now);

    /* Check whether second boundary is crossed */
    if (now.ms < prev.ms)
        {
        diff = (now.ms + 1000 - prev.ms);
        }
    else
        {
        diff = (now.ms - prev.ms);
        }

    prev = now;

    return diff;

    }


/*
Calculates the G-forces for each axis based on the raw accelerometer data read
from the sensor.
*/
void app_calculate_g_forces(imu_data_t* raw, g_force_t* g_f)
    {

    TR_APP(TR_FUNCTION);

    /* This macro defines 1g force at VCC change in ADC input. The formula 
       calculates 1g change in change of ADC value using direct proportion.

       Sensitivity (ratiometric, Vs=3.0V) = 300mV / g
       Zero g bias level (ratiometric, Vs=3.0V) = 1.5V
       Zero g bias level in ADC = 0x200
    */
    #define ONE_G_ADC_VALUE ((0x200 * 0.3) / 1.65)

    /* Assumption here is that each channel has same sensitivity and zero
       bias level, but in reality those are not the same! */
    g_f->x = (long)(raw->raw_acc_x - 0x200) / ONE_G_ADC_VALUE;
    g_f->y = (long)(raw->raw_acc_y - 0x200) / ONE_G_ADC_VALUE;
    g_f->z = (long)(raw->raw_acc_z - 0x200) / ONE_G_ADC_VALUE;
#if 0
    if (g_f->x < 0)
        trace_print_char('-');
    else
        trace_print_char('+');
    TR_APP(trace_print_dec("1k Acc xg", 1000 * fabsf(g_f->x)));

    if (g_f->y < 0)
        trace_print_char('-');
    else
        trace_print_char('+');
    TR_APP(trace_print_dec("1k Acc yg", 1000 * fabsf(g_f->y)));

    if (g_f->z < 0)
        trace_print_char('-');
    else
        trace_print_char('+');
    TR_APP(trace_print_dec("1k Acc zg", 1000 * fabsf(g_f->z)));
#endif
    }


/*
Calculates the rotation from gyroscope and stores the values in radians to 
further processing.
*/
void app_gyro_roll(imu_data_t* raw, gyro_roll_t* gyro)
    {

    TR_APP(TR_FUNCTION);

#if 1
    /* ADC conversion extend from 0V to ADVREF, which is 3.3V in our board */
    const float ADVREF          = 3.3;

    /* ADC is 10-bit, which means that the maximum value is 2^10 */
    const float ADC_MAX         = 1024.0;

    /* IDG-300 dual axis Gyro has a sensitivity of 2mV/�/s */
    const float SENSITIVITY     = 0.002;

    /* Zero-rate output of IDG-300 is 1.5V (465) however next values are 
       the actual measured values from the Gyro for X and Y directions */
    const float ZERO_RATE_ADC_X = 530;
    const float ZERO_RATE_ADC_Y = 448;

    /* Calculate the angular rates for X/Y directions */
    gyro->x = ((raw->raw_gyro_x - ZERO_RATE_ADC_X)  /* 1. X-rate in ADC */
            * (ADVREF / ADC_MAX))                   /* 2. X-rate in mV */
            / SENSITIVITY;                           /* 3. X-rate in degrees */
//            * (M_PI / 180.0);                       /* 4. X-rate in radians */

    gyro->y = ((raw->raw_gyro_y - ZERO_RATE_ADC_Y)  /* 1. Y-rate in ADC */
            * (ADVREF / ADC_MAX))                   /* 2. Y-rate in mV */
            / SENSITIVITY;                           /* 3. Y-rate in degrees */
//            * (M_PI / 180.0);                       /* 4. Y-rate in radians */

#else
    const float Vref = 3.3;

    gyro->x = ((Vref / 0x400) * raw->raw_gyro_x - (Vref / 0x400) * 530) / 0.002 * (M_PI / 180.0);
    gyro->y = ((Vref / 0x400) * raw->raw_gyro_y - (Vref / 0x400) * 448) / 0.002 * (M_PI / 180.0);

#endif
#if 0
    if (gyro->x < 0)
        trace_print_char('-');
    else
        trace_print_char('+');
    TR_APP(trace_print_dec("Gyro x", fabsf(gyro->x) * (180.0 / M_PI)));

    if (gyro->y < 0)
        trace_print_char('-');
    else
        trace_print_char('+');
    TR_APP(trace_print_dec("Gyro y", fabsf(gyro->y) * (180.0 / M_PI)));
#endif
    }
