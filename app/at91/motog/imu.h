/**
\file
\brief Inertial Measurement Unit

\author Copyright (c) 2008, Tomi Parviainen
*/

#ifndef _IMU_H_
#define _IMU_H_

/*
Defines the size of the buffer for angle values. The buffer length in seconds
can be calculated with the formula: BUFFER_SIZE / IMU_CONV_RATE = xx SECONDS
*/
#define BUFFER_SIZE 400

/*
Defines the IMU conversion rate in Hz.
*/
#define IMU_CONV_RATE 100

/*
Variable declaration(s)
*/
sint16_t lean_angle;
sint16_t acceleration_g[BUFFER_SIZE];


/*
Defines the state what the IMU is currently measuring
*/
typedef enum
{
    LEAN_ANGLE = 0,
    G_FORCE
} motog_state;


/*
Inertia Measurement Unit (IMU) data structure. RAW data from sensor is stored
to this structure.

Ratiometric - the output sensitivity (or scale factor) varies proportionally
              to the supply voltage.

Non-ratiometric - the scale factor is calibrated at the factory and is
                  nominally independent of supply voltage.

IMU 5 Degrees of Freedom:
- IDG-300 - dual axis gyro (non-ratiometric)
- ADXL330 - accelerometer (ratiometric)
*/
typedef struct {
    uint32_t raw_acc_x;     /* 0x000 - 0x400 = 10-bit raw data */
    uint32_t raw_acc_y;
    uint32_t raw_acc_z;
    uint32_t raw_gyro_x;    /* roll */
    uint32_t raw_gyro_y;    /* pitch */
    uint32_t raw_gyro_z;    /* yaw */
} imu_data_t;


/*
Structure, which holds the object's acceleration expressed in g's
*/
typedef struct {
    float x;
    float y;
    float z;
} g_force_t;


/*
Structure, which holds the object's angular rate expressed in radians
*/
typedef struct {
    float x;
    float y;
    float z;
} gyro_roll_t;


/*
Function prototypes(s)
*/
extern void imu_init(void);
extern void imu_values(void);


#endif /* _APP_H_ */
