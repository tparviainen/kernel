/*
Copyright (c) 2007-2010, Tomi Parviainen

LCD GE8 controller implementation
*/

#include "includes.h"
#ifdef SAM7X256
#include "at91sam7x256.h"
#else /* SAM7S256 */
#include "at91sam7s256.h"
#endif
#include "lcd.h"

#ifdef RUN_ROM
#include "bmp/startup_logo.h"
#endif


/*
LCD trace enable macro
*/
#define TR_LCD(a)


/*
Screen buffer address
*/
static uint8_t* screen_buffer;


/*
Initialises LCD controller according to EPSON S1D15G00 controller
specification, page 54. Page 57 on new specification.
*/
void lcd_init_controller()
    {

    /* Perform hardware reset, please note that RESET is active on LOW
       level! */
#ifdef SAM7X256
    gpio_value_set(PA2, 0);
    gpio_mode_set(PA2, GPIO_MODE_OUTPUT);
    task_sleep(1);
    gpio_value_set(PA2, 1);
#else /* SAM7S256 */
    gpio_value_set(PA16, 0);
    gpio_mode_set(PA16, GPIO_MODE_OUTPUT);
    task_sleep(1);
    gpio_value_set(PA16, 1);
#endif
    task_sleep(1);

    /* DISPLAY SETTING */
    /* Display control */
    lcd_spi_write(DISCTL, 1);
    lcd_spi_write(0x00, 0);
    lcd_spi_write(0x20, 0); /* 1/132 duty */
    lcd_spi_write(0x1A, 0);

    /* Common scan direction */
    lcd_spi_write(COMSCN, 1);
#ifdef SAM7X256
    lcd_spi_write(0x00, 0); /* 1 -> 68, 69 -> 132 */
#else /* SAM7S256 */
    lcd_spi_write(0x01, 0); /* 1 -> 68, 132 -> 69 */
#endif

    /* Oscillation ON */
    lcd_spi_write(OSCON, 1);
    /* task_sleep(100); / * ??? */

    /* Sleep-out */
    lcd_spi_write(SLPOUT, 1);

    /* POWER SUPPLY SETTING */
    /* Electronic volume control */
    lcd_spi_write(VOLCTR, 1);
    lcd_spi_write(0x1F, 0);
    lcd_spi_write(0x03, 0);

    /* Temperature gradient set */
    lcd_spi_write(TMPGRD, 1);
    lcd_spi_write(0x00, 0);

    /* Power control */
    lcd_spi_write(PWRCTR, 1);
    lcd_spi_write(0x0F, 0);

    /* DISPLAY SETTING 2 */
    /* Normal rotation of display */
#ifdef SAM7X256
    lcd_spi_write(DISNOR, 1);
#else /* SAM7S256 */
    lcd_spi_write(DISINV, 1);
#endif

    /* Partial-out */
    lcd_spi_write(PTLOUT, 1);

    /* DISPLAY SETTING 3 */
    /* Data control */
    lcd_spi_write(DATCTL, 1);
    lcd_spi_write(0x00, 0); /* Can be used to rotate display */
    lcd_spi_write(0x01, 0); /* 1 = bbbbgggg, rrrrbbbb, ggggrrrr = 2pix */
    lcd_spi_write(0x02, 0); /* 16-gray scale display */

    lcd_clear_screen();

#ifdef RUN_ROM
    /* Splash screen */
    update_area_t startup_logo;
    startup_logo.area = startup_logo_img;
    startup_logo.width = startup_logo_img_w;
    startup_logo.height = startup_logo_img_h;
    startup_logo.skip = 0;
    startup_logo.x = 0;
    startup_logo.y = 0;
    lcd_screen_to_display(&startup_logo);
#endif

    /* Wait (approximately 100ms) */
    task_sleep(100);

    /* Display ON */
    lcd_spi_write(DISON, 1);

    }


/*
Writes command or data into SPI bus

\param value Value to be written into LCD controller
\param cmd If TRUE, then value means command else value means data
*/
void lcd_spi_write(uint32_t value, uint32_t cmd)
    {

    /* In case this is a data transfer 8th bit needs to be set */
    if (cmd == 0)
        {
        value |= 0x100;
        }

    /* Ensure the old value has been transferred to serializer before
       writing new value into transmit data register */
#ifdef SAM7X256
    while ((AT91C_BASE_SPI0->SPI_SR & AT91C_SPI_TDRE) == 0)
        ; /* NOP */
#else /* SAM7S256 */
    while ((AT91C_BASE_SPI->SPI_SR & AT91C_SPI_TDRE) == 0)
        ; /* NOP */
#endif

#ifdef SAM7X256
    AT91C_BASE_SPI0->SPI_TDR = value;
#else /* SAM7S256 */
    AT91C_BASE_SPI->SPI_TDR = value;
#endif

    }


/*
Draws pixel into screen buffer

\param x X-coordinate
\param y Y-coordinate
\param color Pixel color in 12-bit 'rrrrggggbbbb' format
*/
void lcd_draw_pixel(uint32_t x, uint32_t y, uint32_t color)
    {

    uint32_t pos;
    uint32_t odd;

    pos = x + y * 130;

    /* Check if pointing to odd/even pixel */
    if (pos & 1)
        {
        odd = TRUE;
        }
    else
        {
        odd = FALSE;
        }

    /* Calculate the position in 8-bit array */
    pos *= 1.5;

    /* Internally the color palette for 2 pixel is:

       76543210
       bbbbgggg EVEN pixel
       rrrrbbbb
       ggggrrrr ODD pixel
    */
    if (odd)
        {
        /* ODD pixel handling */
        screen_buffer[pos] = (screen_buffer[pos] & 0xF0) |
                             (color & 0x0F);            /* bbbb */

        screen_buffer[pos + 1] = ((color & 0xF0) |      /* gggg */
                                  (color >> 8 & 0x0F)); /* rrrr */
        }
    else
        {
        /* EVEN pixel handling */
        screen_buffer[pos] = ((color << 4 & 0xF0) |     /* bbbb */
                              (color >> 4 & 0x0F));     /* gggg */

        screen_buffer[pos + 1] = (screen_buffer[pos + 1] & 0x0F) |
                                 (color >> 4 & 0xF0);   /* rrrr */
        }

    }


/*
*/
void lcd_draw_line(uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint32_t color)
    {

    int dy = y1 - y0;
    int dx = x1 - x0;
    int stepx, stepy;

    if (dy < 0) { dy = -dy;  stepy = -1; } else { stepy = 1; }
    if (dx < 0) { dx = -dx;  stepx = -1; } else { stepx = 1; }

    dy <<= 1;        // dy is now 2*dy
    dx <<= 1;        // dx is now 2*dx

    lcd_draw_pixel(x0, y0, color);

    if (dx > dy)
        {
        int fraction = dy - (dx >> 1);  // same as 2*dy - dx

        while (x0 != x1)
            {
            if (fraction >= 0)
                {
                y0 += stepy;
                fraction -= dx;    // same as fraction -= 2*dx
                }

            x0 += stepx;
            fraction += dy;    // same as fraction -= 2*dy
            lcd_draw_pixel(x0, y0, color);
            }
        }
    else
        {
        int fraction = dx - (dy >> 1);

        while (y0 != y1)
            {
            if (fraction >= 0)
                {
                x0 += stepx;
                fraction -= dy;
                }

            y0 += stepy;
            fraction += dx;
            lcd_draw_pixel(x0, y0, color);
            }
        }

    }


/*
Performs alpha blending between the source areas and stores the results to
target area.

\param src1 Source area (alpha value 0 means fully opaque area)
\param src2 Source area (alpha value 0 means fully transparent area)
\param trgt Target area
\param alpha Color transparency, 0 - fully transparent, 100 - fully opaque
*/
void lcd_alpha_blend(update_area_t* src1, update_area_t* src2, update_area_t* trgt, uint32_t alpha)
    {

    uint16_t x;
    uint16_t y;
    uint32_t p1, p2;
    uint32_t color;
    const uint32_t w = src1->width;
    const uint32_t h = src1->height;
    const uint8_t* area1 = src1->area;
    const uint8_t* area2 = src2->area;

    for (y = 0; y < h; y++)
        {
        for (x = 0; x < w; x++)
            {
            /* Read color value of pixels */
            p1 = lcd_pixel_get(area1, x + y * (w + src1->skip));
            p2 = lcd_pixel_get(area2, x + y * (w + src2->skip));

            /* Value = Value1(1.0 - Alpha) + Value2(Alpha) */
            color  = ((R(p1) * (100 - alpha) + R(p2) * alpha) / 100) << 8;
            color |= ((G(p1) * (100 - alpha) + G(p2) * alpha) / 100) << 4;
            color |= ((B(p1) * (100 - alpha) + B(p2) * alpha) / 100) << 0;

            lcd_pixel_draw(trgt, x + trgt->x, y + trgt->y, color);
            }
        }

    }


/*
*/
void lcd_copy(update_area_t* src, update_area_t* trgt)
    {

    uint16_t x;
    uint16_t y;
    uint32_t p1;
    const uint32_t w = src->width;
    const uint32_t h = src->height;
    const uint8_t* area = src->area;
    const uint16_t offset_x = src->x;
    const uint16_t offset_y = src->y;

    for (y = 0; y < h; y++)
        {
        for (x = 0; x < w; x++)
            {
            /* Read color value of pixels */
            p1 = lcd_pixel_get(area, x + y * (w + src->skip));
            lcd_pixel_draw(trgt, offset_x + x + trgt->x, offset_y + y + trgt->y, p1);
            }
        }

    }


/*
*/
uint32_t lcd_pixel_get(const any_t* ptr, uint32_t offset)
    {

    uint32_t color;
    uint32_t byte_pos;
    const uint8_t* area = ptr;

    /* Internally the color palette for 2 pixel is:
       'rrrrggggbbbb'

       76543210
       bbbbgggg EVEN pixel
       rrrrbbbb
       ggggrrrr ODD pixel
    */

    byte_pos = offset * 1.5;

    if (offset & 0x1)
        {
        /* ODD pixel */
        color = area[byte_pos] & 0x0F;              /* bbbb */
        byte_pos++;
        color = color |
                 (area[byte_pos] & 0xF0) |          /* gggg */
                ((area[byte_pos] & 0x0F) << 8);     /* rrrr */
        }
    else
        {
        /* EVEN pixel */
        color = ((area[byte_pos] & 0xF0) >> 4) |    /* bbbb */
                ((area[byte_pos] & 0x0F) << 4);     /* gggg */
        byte_pos++;
        color = color |
                ((area[byte_pos] & 0xF0) << 4);     /* rrrr */
        }

    return color;

    }


/*
Bresenham's line algorithm (from wikipedia).
*/
void lcd_line_draw(update_area_t* ptr, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color)
    {

    sint32_t slope;
    sint32_t dx, dy;
    sint32_t incE, incNE, d, x, y;

    /* Reverse lines where x1 > x2 */
    if (x1 > x2)
        {
        lcd_line_draw(ptr, x2, y2, x1, y1, color);
        return;
        }

    /* Calculate the lengths of x and y axis */
    dx = x2 - x1;
    dy = y2 - y1;

    /* Adjust y-increment for negatively sloped lines */
    if (dy < 0)
        {
        slope = -1;
        dy = -dy;
        }
    else
        {
        slope = 1;
        }

    /* Bresenham constants */
    incE = (dy << 1);           /* 2 * dy */
    incNE = incE - (dx << 1);   /* 2 * dy - 2 * dx */
    d = incE - dx;              /* 2 * dy - dx */
    y = y1;

    /* Blit */
    for (x = x1; x <= x2; x++)
        {
        lcd_pixel_draw(ptr, x, y, color);

        if (d <= 0)
            {
            d += incE;
            }
        else
            {
            d += incNE;
            y += slope;
            }
        }

    }


/*
Draws a pixel into screen with specified color.

\param ptr Pointer to screen area
\param x X-coordinate in screen area
\param y Y-coordinate in screen area
\param color Pixel color
*/
void lcd_pixel_draw(update_area_t* ptr, uint16_t x, uint16_t y, uint32_t color)
    {

    uint8_t* p;
    uint32_t pos;
    uint32_t odd;

    /* Width + skip is the length of one line */
    pos = x + y * (ptr->width + ptr->skip);

    /* Check whether updating ODD/EVEN pixel */
    if (pos & 0x1)
        {
        odd = TRUE;
        }
    else
        {
        odd = FALSE;
        }

    /* Calculate the correct byte position, in ODD pixels this points to
       pixel where previous color (rrrr....) is also present! */
    pos *= 1.5;

    /* Internally the color palette for 2 pixel is:

       76543210
       bbbbgggg EVEN pixel
       rrrrbbbb
       ggggrrrr ODD pixel
    */
    p = (any_t*)ptr->area;

    if (odd)
        {
        p[pos] = (p[pos] & 0xF0) | B(color);        /* ....bbbb */
        pos++;
        p[pos] = (G(color) << 4) | R(color);        /* ggggrrrr */
        }
    else
        {
        p[pos] = (B(color) << 4) | G(color);        /* bbbbgggg */
        pos++;
        p[pos] = (R(color) << 4) | (p[pos] & 0x0F); /* rrrr.... */
        }

    }


/*
Fills an area of screen buffer with specified color. The width of the area
must be a multiple of 2.

\param ptr Update area
\param color Color to be used for filling the area
*/
void lcd_box_fill(update_area_t* ptr, uint32_t color)
    {

    uint16_t x;
    uint16_t y;
    uint8_t* p = (any_t*)ptr->area; /* remove const */
    uint32_t w = ptr->width;
    uint32_t h = ptr->height;
    uint32_t s = ptr->skip;

    TR_LCD(TR_FUNCTION);
    ASSERT((w & 1) == 0);
    ASSERT((s & 1) == 0);

    /* Each skipped pixel means 1.5-byte offset */
    s *= 1.5;

    /* 2 pixels (3-bytes) is always filled with one iteration */
    w /= 2;

    /* Internally the color palette for 2 pixel is:

       76543210
       bbbbgggg EVEN pixel
       rrrrbbbb
       ggggrrrr ODD pixel
    */
    for (y = 0; y < h; y++)
        {
        for (x = 0; x < w; x++)
            {
            *p++ = (B(color) << 4) | G(color);  /* bbbbgggg */
            *p++ = (R(color) << 4) | B(color);  /* rrrrbbbb */
            *p++ = (G(color) << 4) | R(color);  /* ggggrrrr */
            }

        /* Skip the rest of this line */
        p += s;
        }

    }


/*
Updates the screen buffer to display, the updateable screen area width and
skip area must be divisible by 2. This function allows part of the screen
buffer be updated to display.

\param ptr Pointer to parameter area
*/
void lcd_screen_to_display(update_area_t* ptr)
    {

    uint16_t x;
    uint16_t y;
    const uint8_t* p = ptr->area;
    uint32_t w = ptr->width;
    uint32_t h = ptr->height;
    uint32_t s = ptr->skip;

    TR_LCD(TR_FUNCTION);
    TR_LCD(trace_print_hex("p", (uint32_t)p));
    TR_LCD(trace_print_hex("w", (uint32_t)w));
    TR_LCD(trace_print_hex("h", (uint32_t)h));
    TR_LCD(trace_print_hex("s", (uint32_t)s));
    TR_LCD(trace_print_hex("x", (uint32_t)ptr->x));
    TR_LCD(trace_print_hex("y", (uint32_t)ptr->y));
    ASSERT((w & 1) == 0);
    ASSERT((s & 1) == 0);

    /* Column address set, with display's internal offset corrected! */
    lcd_spi_write(CASET, 1);
    lcd_spi_write(ptr->x + 0, 0);
    lcd_spi_write(ptr->x + 0 + w - 1, 0);

    /* Page address set, with display's internal offset corrected! */
    lcd_spi_write(PASET, 1);
    lcd_spi_write(ptr->y + 2, 0);
    lcd_spi_write(ptr->y + 2 + h - 1, 0);

    /* RAM write */
    lcd_spi_write(RAMWR, 1);

    /* 12-bit transfers means that 1.5 bytes are consumed to each pixel */
    w *= 1.5;
    s *= 1.5;

    TR_LCD(trace_print_hex("w", (uint32_t)w));
    TR_LCD(trace_print_hex("s", (uint32_t)s));

    for (y = 0; y < h; y++)
        {
        for (x = 0; x < w; x++)
            {
            lcd_spi_write(*p++, 0);
            }

        /* Skip the rest of this line */
        p += s;
        }

    }


/*
*/
void lcd_clear_screen()
    {

    /* Column address set, with display's internal offset corrected! */
    lcd_spi_write(CASET, 1);
    lcd_spi_write(0, 0);
    lcd_spi_write(MAX_X - 1, 0);

    /* Page address set, with display's internal offset corrected! */
    lcd_spi_write(PASET, 1);
    lcd_spi_write(2, 0);
    lcd_spi_write(2 + MAX_Y - 1, 0);

    /* RAM write */
    lcd_spi_write(RAMWR, 1);

    /* 12-bit transfers means that 1.5 bytes are consumed to each pixel */

    for (uint16_t y = 0; y < MAX_Y; y++)
        {
        for (uint16_t x = 0; x < MAX_X * 1.5; x++)
            {
            lcd_spi_write(0x00, 0);
            }
        }

    }
