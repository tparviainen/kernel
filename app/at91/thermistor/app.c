/*
Copyright (c) 2007-2010, Tomi Parviainen
*/

#include "includes.h"
#include "at91sam7x256.h"

/*
Function prototypes(s)
*/
static void app_thermistor(any_t* param);


/*
APP trace enable macro
*/
#define TR_APP(a) a


/*
Creates a task for temperature sensor handling.
*/
void app_init()
    {

    tcb_t* tcb;

    /* Enable peripheral clock for ADC */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_ADC);

    /* Set the conversion parameters as proposed by Olimex in their code
       example i.e. maximum startup and hold times */
    AT91C_BASE_ADC->ADC_MR = AT91C_ADC_TRGEN_DIS
                           | AT91C_ADC_LOWRES_10_BIT
                           | AT91C_ADC_SLEEP_NORMAL_MODE
                           | (AT91C_ADC_PRESCAL & (0xF << 8))
                           | (AT91C_ADC_STARTUP & (0x1F << 16))
                           | (AT91C_ADC_SHTIM & (0xF << 24));

    tcb = task_create(app_thermistor, 0, 512, 1, "THERMISTOR");
    ASSERT(tcb != NULL);

    }


/*
Task, which initialises temperature thermistor and reads temperature
value once in a second.
*/
void app_thermistor(any_t* param)
    {

    uint32_t adc_data;

    TR_APP(trace_print("-> app_thermistor"));

    /* Enable ADC channel 5 i.e. the channel where temperature thermistor
       is connected */
    AT91C_BASE_ADC->ADC_CHER = AT91C_ADC_CH5;

    for (;;)
        {
        /* Begin analog-to-digital conversion on enabled channels */
        AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;

        /* 1 sec read period */
        task_sleep(1000);

        /* Wait conversion to complete */
        while ((AT91C_BASE_ADC->ADC_SR & AT91C_ADC_EOC5) == 0)
            ; /* NOP */

        adc_data = AT91C_BASE_ADC->ADC_CDR5;
        adc_data *= 10; /* Be able to print decimals i.e XX.x �C */
        adc_data /= 19; /* 1�C change means 19 bit change in ADC */

        TR_APP(trace_print_dec("temperature", adc_data)); /* x 10 */
        }

    }
