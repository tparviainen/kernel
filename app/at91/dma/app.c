/*
Copyright (c) 2007-2011, Tomi Parviainen

Application specific code
*/

#include "includes.h"
#include "at91sam7x256.h"


/*
Function prototypes(s)
*/
static void app_task(any_t* param);
static void timer_callback(any_t* param);
static void dma_eoc(void);


/*
APP trace enable macro
*/
#define TR_APP(a) a


/*
Variable declaration(s)
*/
static timer_t timer;
static semaphore_t data_ready;
static uint16_t *adc_data = NULL;
static const uint16_t TIMER_PERIOD = 100;
static const uint16_t SAMPLE_COUNT = 10;


/*
Application code initialisation. This function is called during the kernel
initialisation just before the multitasking will be started.
*/
void app_init()
    {

    tcb_t* tcb;

    /* Enable peripheral clock for ADC */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_ADC);

    /* Set the conversion parameters as proposed by Olimex in their code
       example i.e. maximum startup and hold times */
    AT91C_BASE_ADC->ADC_MR = AT91C_ADC_TRGEN_DIS
                           | AT91C_ADC_LOWRES_10_BIT
                           | AT91C_ADC_SLEEP_NORMAL_MODE
                           | (AT91C_ADC_PRESCAL & (0xF << 8))
                           | (AT91C_ADC_STARTUP & (0x1F << 16))
                           | (AT91C_ADC_SHTIM & (0xF << 24));

    tcb = task_create(app_task, NULL, 512, 1, "DMA");
    ASSERT(tcb != NULL);

    timer_init(&timer, timer_callback, NULL);
    semaphore_init(&data_ready, 0);

    }


/*
Timer interrupt that takes care of restarting ADC conversion. Normally this should be done
using hardware trigger i.e. timer counters (TIOA output of the timer counter channel X)
instead of controlling this by SW.
*/
void timer_callback(any_t* param)
    {

    timer_start(&timer, TIMER_PERIOD);

    /* Restart analog-to-digital conversion on enabled channels */
    AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;

    }


/*
DMA end of conversion interrupt, data is available in the buffer and application is ready to
use it. In real life example also PDC_RNPR (Receive Next Pointer Register) should be used to
give application time to read the data from the primary buffer.
*/
void dma_eoc()
    {
    trace_print("dma_eoc");

    uint32_t sr = AT91C_BASE_ADC->ADC_SR;
    trace_print_hex("ADC_SR", sr);

    if (sr & AT91C_ADC_ENDRX)
        {
        ASSERT(adc_data != NULL);
        AT91C_BASE_PDC_ADC->PDC_RPR = (uint32_t)adc_data;
        AT91C_BASE_PDC_ADC->PDC_RCR = SAMPLE_COUNT;

        semaphore_signal(&data_ready);
        }

    }


/*
Initialises ADC for data transfer from trimmer and waits buffer to fill and then prints
the content of the buffer.
*/
void app_task(any_t* param)
    {

    adc_data = memory_alloc(SAMPLE_COUNT * 2);
    memory_set(adc_data, 0x00, SAMPLE_COUNT * 2);

    TR_APP(trace_print_hex("-> app_task", (uint32_t)param));

    /* Enable ADC channel 6 i.e. the channel where the trimmer is */
    AT91C_BASE_ADC->ADC_CHER = AT91C_ADC_CH6;

    /* Number of transfers */
    AT91C_BASE_PDC_ADC->PDC_RCR = SAMPLE_COUNT;

    /* Memory base address */
    AT91C_BASE_PDC_ADC->PDC_RPR = (uint32_t)adc_data;

    /* Enable transfer */
    AT91C_BASE_PDC_ADC->PDC_PTCR = AT91C_PDC_RXTEN;

    /* Enable end of conversion interrupt on HW block level */
    AT91C_BASE_ADC->ADC_IER = AT91C_ADC_ENDRX;

    irq_bind(ADC, dma_eoc);
    irq_enable(ADC);

    timer_start(&timer, TIMER_PERIOD);

    for (;;)
        {
        trace_print("\nWaiting buffer to fill ...");
        semaphore_wait(&data_ready);

        for (uint32_t cnt = 0; cnt < SAMPLE_COUNT; cnt++)
            trace_print_hex("adc_data", adc_data[cnt]);
        }

    }
