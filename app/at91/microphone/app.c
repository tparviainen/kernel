/*
Copyright (c) 2010, Tomi Parviainen

Application specific code
*/

#include "includes.h"
#include "at91sam7x256.h"


/*
Function prototypes(s)
*/
static void app_task(any_t* param);


/*
APP trace enable macro
*/
#define TR_APP(a) a


/*
Application code initialisation. This function is called during the kernel
initialisation just before the multitasking will be started.
*/
void app_init()
    {

    tcb_t* tcb;

    /* Enable peripheral clock for ADC */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_ADC)
                             | (1 << AT91C_ID_PWMC);

    /* Disable the PIO from controlling the pin and assign pin to peripheral
       A functionality i.e. PWM0 */
    AT91C_BASE_PIOB->PIO_PDR = (1 << 19);
    AT91C_BASE_PIOB->PIO_ASR = (1 << 19);

    /* Configure clock generator (MCK). CLKA and CLKB are switched OFF */
    AT91C_BASE_PWMC->PWMC_MR = (AT91C_PWMC_DIVA & (0 << 0))     /* DIVA */
                             | (AT91C_PWMC_PREA & (0 << 8))     /* PREA */
                             | (AT91C_PWMC_DIVB & (0 << 16))    /* DIVB */
                             | (AT91C_PWMC_PREB & (0 << 16));   /* PREB */

    /* Selection of the clock for channel, configuration of the waveform
       alignment and the output waveform polarity */
    AT91C_BASE_PWMC_CH0->PWMC_CMR = (AT91C_PWMC_CPRE & (0 << 0))
                                  | (AT91C_PWMC_CALG & (0 << 8))
                                  | (AT91C_PWMC_CPOL & (0 << 9))
                                  | (AT91C_PWMC_CPD & (0 << 10));

    /* Configuration of the period for channel. From specification: For example, if the
       user sets 15 (in decimal) in PWM_CPRDx, the user is able to set a value between
       1 up to 14 in PWM_CDTYx Register */
    AT91C_BASE_PWMC_CH0->PWMC_CPRDR = 1024;

    /* Set the conversion parameters as proposed by Olimex in their code
       example i.e. maximum startup and hold times */
    AT91C_BASE_ADC->ADC_MR = AT91C_ADC_TRGEN_DIS
                           | AT91C_ADC_LOWRES_10_BIT
                           | AT91C_ADC_SLEEP_NORMAL_MODE
                           | (AT91C_ADC_PRESCAL & (0xF << 8))
                           | (AT91C_ADC_STARTUP & (0x1F << 16))
                           | (AT91C_ADC_SHTIM & (0xF << 24));

    /* Enable ADC channel 7 i.e. the channel where microphone is connected */
    AT91C_BASE_ADC->ADC_CHER = AT91C_ADC_CH7;

    tcb = task_create(app_task, NULL, 512, 1, "MIC");
    ASSERT(tcb != NULL);

    }


/*
Reads data from AD-converter (microphone) and generates PWM data for speaker
*/
void app_task(any_t* param)
    {

    uint32_t adc_data;

    TR_APP(trace_print_hex("-> app_task", (uint32_t)param));

    /* Enable the PWM channel */
    AT91C_BASE_PWMC->PWMC_ENA = AT91C_PWMC_CHID0;

    /* Begin analog-to-digital conversion on enabled channels */
    AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;

    for (;;)
        {
        /* Wait conversion to complete */
        while ((AT91C_BASE_ADC->ADC_SR & AT91C_ADC_EOC7) == 0)
            ; /* NOP */

        /* 10 bit = 0 - 1023, when in IDLE the value is in the middle i.e. 0x200 +- 0x10 */
        adc_data = AT91C_BASE_ADC->ADC_CDR7;

        /* Begin analog-to-digital conversion on enabled channels */
        AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;

        /* Prevent overwriting the PWM_CUPD register value */
        while ((AT91C_BASE_PWMC->PWMC_ISR & AT91C_PWMC_CHID0) == 0)
            ; /* NOP */

        /* Change the duty cycle of the output */
        AT91C_BASE_PWMC_CH0->PWMC_CUPDR = adc_data;
        }

    }
