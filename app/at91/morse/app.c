/*
Copyright (c) 2007-2011, Tomi Parviainen

Application specific code. Please use UPPERCASE letters when communicating from
terminal to development board!
*/

#include "includes.h"
#include "at91sam7x256.h"
#include "morse.h"


/*
Function prototypes(s)
*/
static void app_task(any_t* param);
static void app_usart_irq(void);


/*
Variable declaration(s)
*/
static uint8_t cnt;
static sint8_t* str;
static mailbox_t mbx;


/*
APP trace enable macro
*/
#define TR_APP(a)


/*
Creates a task for MORSE code application.
*/
void app_init()
    {

    tcb_t* tcb;

    tcb = task_create(app_task, NULL, 512, 1, "MORSE");
    ASSERT(tcb != NULL);

    }


/*
Morse code generator task, which initialises variables and Morse code
generation.
*/
void app_task(any_t* param)
    {

    TR_APP(TR_FUNCTION);

    /* Initialise Morse code generator before usage */
    morse_init();
    morse_transmit("TSP 2007-2011");

    cnt = 0; /* Only changed in IRQ context */
    str = memory_alloc(256);
    mailbox_init(&mbx, 1);

    irq_bind(US0, app_usart_irq);
    irq_enable(US0);

    /* Enable RX ready interrupt on HW block level */
    AT91C_BASE_US0->US_IER = AT91C_US_RXRDY;

    for (;;)
        {
        /* Wait a message to arrive to mailbox, it is posted from RX IRQ */
        mailbox_pend(&mbx);

        /* Generates Morse code from the characters in array. Please note
           that during Morse code generation the array can be filled with
           new characters, even though it is not entirely safe because the
           NULL character can be overwritten! */
        morse_transmit(&str[0]);
        }

    }


/*
Stores the received character into array and in case enter was pressed
then posts a message to mailbox so that the Morse code will be generated
in task context.
*/
void app_usart_irq()
    {

    TR_APP(TR_FUNCTION);

    /* Store character to string array */
    str[cnt] = AT91C_BASE_US0->US_RHR;

    /* Echo the character back to sender */
    trace_print_char(str[cnt]);

    /* If ENTER was pressed, time to initiate Morse code generation */
    if (str[cnt++] == 0xD)
        {
        str[--cnt] = '\0';
        cnt = 0;

        /* Send a message to mailbox to start processing the string */
        mailbox_post(&mbx, (any_t*)TRUE);
        }

    }
