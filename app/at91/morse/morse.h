/**
\file
\brief MORSE

\author Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _MORSE_H_
#define _MORSE_H_


/*
The speed of the encoding (ms), normal speed is about 60-120 characters in
minute.
*/
#define DOT_DELAY 100


/*
Function prototypes(s)
*/
extern void morse_init(void);
extern void morse_transmit(const sint8_t* str);


#endif /* _LCD_H_ */
