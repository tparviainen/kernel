/*
Copyright (c) 2007-2011, Tomi Parviainen

Morse code implementation
*/

#include "includes.h"
#include "at91sam7x256.h"
#include "morse.h"


/*
MORSE trace enable macro
*/
#define TR_MORSE(a) a


/*
Variable declaration(s)
*/
static timer_t timer;
static uint32_t lock;


/*
50 sample sine wave table. Calculation formula:
sampleVal = SIN(2 * PI * (sampleNo / 50)) * 512 + 512
*/
static const uint16_t sine_wave[] =
    {
    512, 576, 639, 700, 759, 813, 862, 907, 944, 975, 999, 1015, 1023, 1023, 1015,
    999, 975, 944, 907, 862, 813, 759, 700, 639, 576, 512,  448,  385,  324,  265,
    211, 162, 117,  80,  49,  25,   9,   1,   1,   9,  25,   49,   80,  117,  162,
    211, 265, 324, 385, 448,
    };


/*
Function prototypes(s)
*/
static void encode(const sint8_t* str);
static void timer_callback(any_t* param);


/*
Morse codes for numbers
*/
static const sint8_t* const number[] =
    {
    "-----",    /* 0 */
    ".----",    /* 1 */
    "..---",    /* 2 */
    "...--",    /* 3 */
    "....-",    /* 4 */
    ".....",    /* 5 */
    "-....",    /* 6 */
    "--...",    /* 7 */
    "---..",    /* 8 */
    "----.",    /* 9 */
    };


/*
Morse codes for English alphabets
*/
static const sint8_t* const alphabet[] =
    {
    ".-",       /* A */
    "-...",     /* B */
    "-.-.",     /* C */
    "-..",      /* D */
    ".",        /* E */
    "..-.",     /* F */
    "--.",      /* G */
    "....",     /* H */
    "..",       /* I */
    ".---",     /* J */
    "-.-",      /* K */
    ".-..",     /* L */
    "--",       /* M */
    "-.",       /* N */
    "---",      /* O */
    ".--.",     /* P */
    "--.-",     /* Q */
    ".-.",      /* R */
    "...",      /* S */
    "-",        /* T */
    "..-",      /* U */
    "...-",     /* V */
    ".--",      /* W */
    "-..-",     /* X */
    "-.--",     /* Y */
    "--..",     /* Z */
    };


/*
Morse codes for punctuation characters
*/
static const sint8_t* const punctuation[] =
    {
    ". .-.-.-",   /* . */
    ", --..--",   /* , */
    "? ..--..",   /* ? */
    "' .----.",   /* ' */
    "! -.-.--",   /* ! */
    "/ -..-.",    /* / */
    "( -.--.",    /* ( */
    ") -.--.-",   /* ) */
    "& .-...",    /* & */
    ": ---...",   /* : */
    "; -.-.-.",   /* ; */
    "= -...-",    /* = */
    "+ .-.-.",    /* + */
    "- -....-",   /* - */
    "_ ..--.-",   /* _ */
    "\" .-..-.",  /* " */
    "$ ...-..-",  /* $ */
    "@ .--.-.",   /* @ */

    /* Non-English extensions to the Morse code */
    "Ä .-.-",     /* Ä */
    "Ö ---.",     /* Ö */

    "",           /* Terminator, always last entry in array */
    };


/*
Initialises morse code generator. This uses the SAM7-EX256 board's speaker
and the default connection of the board.
*/
void morse_init()
    {

    /* Enable peripheral clock for PWM */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_PWMC);

    /* Disable the PIO from controlling the pin and assign pin to peripheral
       A functionality i.e. PWM0 */
    AT91C_BASE_PIOB->PIO_PDR = (1 << 19);
    AT91C_BASE_PIOB->PIO_ASR = (1 << 19);

    /* Configure clock generator (MCK). CLKA and CLKB are switched OFF */
    AT91C_BASE_PWMC->PWMC_MR = (AT91C_PWMC_DIVA & (0 << 0))     /* DIVA */
                             | (AT91C_PWMC_PREA & (0 << 8))     /* PREA */
                             | (AT91C_PWMC_DIVB & (0 << 16))    /* DIVB */
                             | (AT91C_PWMC_PREB & (0 << 16));   /* PREB */

    /* Selection of the clock for channel, configuration of the waveform
       alignment and the output waveform polarity */
    AT91C_BASE_PWMC_CH0->PWMC_CMR = (AT91C_PWMC_CPRE & (0 << 0))
                                  | (AT91C_PWMC_CALG & (1 << 8))
                                  | (AT91C_PWMC_CPOL & (0 << 9))
                                  | (AT91C_PWMC_CPD & (0 << 10));

    /* Configuration of the period for channel. From specification: For example, if the
       user sets 15 (in decimal) in PWM_CPRDx, the user is able to set a value between
       1 up to 14 in PWM_CDTYx Register */
    AT91C_BASE_PWMC_CH0->PWMC_CPRDR = 1024;

    timer_init(&timer, timer_callback, &lock);

    }


/*
Transmits a Morse code

\param str String to be transmitted via Morse codes
*/
void morse_transmit(const sint8_t* str)
    {

    sint8_t chr;
    uint32_t cnt;
    const sint8_t* code;

    trace_print(str);

    /* Go through all the characters in the string */
    while (*str != NULL)
        {
        chr = *str++;

        if (chr >= '0' && chr <= '9')
            {
            code = number[chr - '0'];
            }
        else if (chr >= 'A' && chr <= 'Z')
            {
            code = alphabet[chr - 'A'];
            }
        else
            {
            /* Punctuation */
            cnt = 0;
            code = NULL;

            /* Go through punctuation characters */
            while (punctuation[cnt][0] != NULL)
                {
                if (punctuation[cnt][0] == chr)
                    {
                    code = &punctuation[cnt][2];

                    /* break the while loop */
                    break;
                    }

                cnt++;
                }
            }

        /* Encode letter in case one was found */
        if (code != NULL)
            {
            trace_print_char(chr);
            trace_print_char(' ');

            encode(code);

            /* Short gap (between letters) */
            task_sleep(DOT_DELAY * 3);
            }

        if (chr == ' ')
            {
            /* Medium gap (between words) */
            task_sleep(DOT_DELAY * 7);
            }
        }

    trace_print("");

    }


/*
Encodes a Morse code strings (dots and dashes) into Morse code

\param str String to be encoded
*/
void encode(const sint8_t* str)
    {

    sint8_t chr;
    uint16_t cnt;

    trace_print(str);

    while (*str != NULL)
        {
        chr = *str++;
        lock = TRUE;

        if (chr == '.') /* . DOT */
            {
            timer_start(&timer, DOT_DELAY);
            }
        else            /* - DASH */
            {
            timer_start(&timer, DOT_DELAY * 3);
            }

        /* Enable the PWM channel */
        AT91C_BASE_PWMC->PWMC_ENA = AT91C_PWMC_CHID0;

        /* Generate Morse code until lock is released */
        while (lock == TRUE)
            {
            /* Generate always full amplitude */
            for (cnt = 0; cnt < (sizeof(sine_wave) / sizeof(uint16_t)); cnt++)
                {
                /* Prevent overwriting the PWM_CUPD register value */
                while ((AT91C_BASE_PWMC->PWMC_ISR & AT91C_PWMC_CHID0) == 0)
                    ; /* NOP */

                /* Change the duty cycle of the output */
                AT91C_BASE_PWMC_CH0->PWMC_CUPDR = sine_wave[cnt++];
                }
            }

        /* Disable the PWM channel */
        AT91C_BASE_PWMC->PWMC_DIS = AT91C_PWMC_CHID0;

        /* Intra-character gap (between the dots and dashes within a
           character) */
        task_sleep(DOT_DELAY);
        }

    }


/*
Releases the lock, so that PWM generation will be stopped

\param param Pointer to lock to be released
*/
void timer_callback(any_t* param)
    {

    uint32_t* lock = param;

    *lock = FALSE;

    }
