/*
Copyright (c) 2007-2010, Tomi Parviainen

Application specific code
*/

#include "includes.h"
#include "at91sam7x256.h"


/*
Function prototypes(s)
*/
static void app_task(any_t* param);


/*
APP trace enable macro
*/
#define TR_APP(a) a


/*
Application code initialisation. This function is called during the kernel
initialisation just before the multitasking will be started.
*/
void app_init()
    {

    tcb_t* tcb;

    /* TODO: initialisation ... */

    /* TODO: example task created here ... */
    tcb = task_create(app_task, NULL, 512, 1, "TEMPLATE");
    ASSERT(tcb != NULL);

    }


/*
TODO: Application specific task ...
*/
void app_task(any_t* param)
    {

    uint32_t task_id;
    uint32_t sleep_sec;

    task_id = (uint32_t)param;

    TR_APP(trace_print_hex("-> app_task", task_id));

    sleep_sec = 0;

    for (;;)
        {
        task_sleep(1000);
        TR_APP(trace_print_dec("APP_TASK", ++sleep_sec));
        }

    }
