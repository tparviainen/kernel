/*
Copyright (c) 2007-2008, Tomi Parviainen
*/

#include "includes.h"


/**
\fn void idle(void)
Idle task implementation, which puts the ARM into a low power state and
stops it executing further until an interrupt occurs
*/

/** The priority of the idle task */
const uint8_t task_idle_priority = 0;

/** Amount of 'stack' sized elements to be allocated for stack */
const uint16_t task_idle_stk_size = 512 / sizeof(stack_t);

/** The name of the idle task */
const sint8_t* const task_idle_name = "IDLE";

/**
IDLE task entrypoint

\param param NULL i.e. not used at the moment
*/
void task_idle(any_t* param)
    {

    TR_TSK(trace_print_hex("-> task_idle", (uint32_t)param));

    for (;;)
        {
#ifdef PROF_CPU_USAGE
        g_task_idle_count++;
#endif /* PROF_CPU_USAGE */

        idle();
        }

    }
