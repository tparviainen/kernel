/*
Copyright (c) 2007-2008, Tomi Parviainen

Please note that most of the UART initialisation is bypassed here because
this is meant to be used only for QEMU tracing purposes and those are not
needed there.
*/

#include "includes.h"


/*
Variable declaration(s)
*/
static uint32_t* trace_ptr;


/**
Initialises tracing system for dedicated trace port
*/
void trace_init()
    {

    trace_ptr = (uint32_t*)0x16000000;

    TR_SYS(trace_print(KERNEL_VERSION));

    }


/**
Prints character into trace port

\param chr Character to be printed
*/
void trace_print_char(const sint8_t chr)
    {

    *trace_ptr = (uint8_t)chr;

    }
