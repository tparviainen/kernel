/**
\file
\brief Hardware specific definitions

This file contains hardware specific definitions for example the list
of main interrupts the system supports.

\author Copyright (c) 2007-2009, Tomi Parviainen
*/

#ifndef _HW_H_
#define _HW_H_


/**
Interrupt source list
*/
typedef enum
{
    SOFTINT     = 0,
    UARTINT0,
    UARTINT1,
    KBDINT,
    MOUSEINT,
    TIMERINT0,
    TIMERINT1,
    TIMERINT2,
    RTCINT,
    LM_LLINT0,
    LM_LLINT1,
    CLCDCINT    = 22,
    MMCIINT0,
    MMCIINT1,
    AACIINT,
    CPPLDINT,
    ETH_INT,
    TS_PENINT
} irq_source;


/**
Supported GPIO pins in Integrator
*/
typedef enum
{
    NOT_DEFINED = 0
} gpio_pin;


/*
The amount of IRQ sources in this hardware (size of the IRQ array)
*/
#define IRQ_COUNT 28


/*
HW register mapping
*/

/* Primary Interrupt Controller */
#define PIC_IRQ_STATUS      (*(volatile unsigned int*) 0x14000000)
#define PIC_IRQ_ENABLESET   (*(volatile unsigned int*) 0x14000008)
#define PIC_IRQ_ENABLECLR   (*(volatile unsigned int*) 0x1400000C)

/* Counter/Timer Interfaces */
#define TIMER_1_LOAD        (*(volatile unsigned int*) 0x13000100)
#define TIMER_1_CONTROL     (*(volatile unsigned int*) 0x13000108)
#define TIMER_1_INT_CLR     (*(volatile unsigned int*) 0x1300010C)
#define TIMER_1_BG_LOAD     (*(volatile unsigned int*) 0x13000118)

#endif /* _HW_H_ */
