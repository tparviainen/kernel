/*
Copyright (c) 2007-2015, Tomi Parviainen
*/

#include "includes.h"


/**
\fn void irq_enable_core(uint32_t old)
Returns the control bits to CPSR i.e. enables interrupts if those were
disabled based on the old control bits
\param old Control bits to be returned into CPSR

\fn uint32_t irq_disable_core(void)
Disables interrupts from core level
\return Original control bits from CPSR

\fn uint32_t irq_context(void)
Checks whether the caller is executing code in interrupt context or in task
context.
\return 1 if the caller's context is IRQ otherwise returns 0
*/


/*
Variable declaration(s)
*/
static irq_fn irq_vector_table[IRQ_COUNT];


/**
Initialises interrupt controller HW and global variables related to interrupt
handling.
*/
void irq_init()
    {

    uint32_t cnt;

    for (cnt = 0; cnt < IRQ_COUNT; cnt++)
        {
        irq_vector_table[cnt] = NULL;
        }

    }


/**
Binds interrupt to function and returns the old function, which was bound
to this interrupt line i.e. makes it possible to chain interrupts

\param irq Interrupt to be bound for function
\param func Function to be called when interrupt occurs
\return Previously bound interrupt handler
*/
irq_fn irq_bind(irq_source irq, irq_fn func)
    {

    irq_fn old_func;

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));
    ASSERT(func != NULL);

    old_func = irq_vector_table[irq];
    irq_vector_table[irq] = func;

    /* Can return NULL */
    return old_func;

    }


/**
Unbinds interrupt i.e. clears the callback function

\pre Interrupt needs to be disabled before unbind
\param irq Interrupt to be unbound
*/
void irq_unbind(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));

    /* Interrupt should be disabled by the caller before unbind */
    ASSERT((PIC_IRQ_ENABLESET & (1 << (uint32_t)irq)) == 0);

    irq_vector_table[irq] = NULL;

    }


/**
Enables the interrupt

\pre Interrupt needs to be bound before enable
\param irq Interrupt to be enabled
*/
void irq_enable(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));
    ASSERT(irq_vector_table[irq] != NULL);

    PIC_IRQ_ENABLESET = (1 << (uint32_t)irq);

    }


/**
Disables the interrupt

\param irq Interrupt to be disabled
*/
void irq_disable(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));

    PIC_IRQ_ENABLECLR = (uint32_t)irq;

    }


/**
Interrupt handler, checks the interrupt source and calls interrupt handler
which is responsible for clearing the peripheral interrupt
*/
void irq_handler()
    {

    irq_fn func;
    uint32_t status;
    uint32_t source;

    TR_IRQ(TR_FUNCTION);

    /* Determine the source of IRQ */
    status = PIC_IRQ_STATUS;
    source = IRQ_COUNT;

    TR_IRQ(trace_print_hex("status", status));

    while (source--)
        {
        if (status & (1 << source))
            {
            func = irq_vector_table[source];
            ASSERT(func != NULL);

            (*func)();
            }
        }

    }
