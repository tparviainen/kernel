/*
Copyright (c) 2007-2008, Tomi Parviainen
*/

/* Start of a code section */
.text
.code 32

/* Make 'system_spin_lock' globally visible */
.global system_spin_lock
.func   system_spin_lock
system_spin_lock:
/*  r0      lock */
    mov     r1, #1
spin:
    swp     r1, r1, [r0]
    cmp     r1, #0
    bne     spin            /* If the lock is not free, spin */
    mov     pc, lr
.endfunc

/* Make 'system_spin_lock' globally visible */
.global system_spin_unlock
.func   system_spin_unlock
system_spin_unlock:
/*  r0      lock */
    mov     r1, #0
    swp     r1, r1, [r0]
    mov     pc, lr
.endfunc

/* The end of this source code file */
.end
