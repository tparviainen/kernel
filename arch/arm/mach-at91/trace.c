/*
Copyright (c) 2007-2010, Tomi Parviainen
*/

#include "includes.h"
#ifdef SAM7X256
#include "at91sam7x256.h"
#else /* SAM7S256 */
#include "at91sam7s256.h"
#endif


/**
Initialises tracing system for dedicated trace port
*/
void trace_init()
    {

#ifdef SAM7X256
    /* Multiplex the USART 0 to RS-232 connector i.e. disable PIO from
       controlling the corresponding data pins (RXD0/TXD0) */
    AT91C_BASE_PIOA->PIO_PDR = (1 << 0) | (1 << 1);

    /* Assign the I/O lines to the peripheral A function */
    AT91C_BASE_PIOA->PIO_ASR = (1 << 0) | (1 << 1);

    /* Enable peripheral (USART 0) clock */
    AT91C_BASE_PMC->PMC_PCER = 1 << AT91C_ID_US0;

    /* Reset/disable transmitter i.e. stop communication immediately */
    AT91C_BASE_US0->US_CR = AT91C_US_RSTTX | AT91C_US_TXDIS;

    /* Set USART baud rate generator for 115.2k = 48M/(115.2k*16) in
       asynchronous mode */
    AT91C_BASE_US0->US_BRGR = 26;

    /* Set USART configuration */
    AT91C_BASE_US0->US_MR = AT91C_US_CHRL_8_BITS
                          | AT91C_US_PAR_NONE
                          | AT91C_US_NBSTOP_1_BIT;

    /* Enable USART 0 transmitter and receiver */
    AT91C_BASE_US0->US_CR = AT91C_US_TXEN | AT91C_US_RXEN;

    TR_SYS(trace_print(KERNEL_VERSION));
#endif

    }


/**
Prints character into trace port

\param chr Character to be printed
*/
void trace_print_char(const sint8_t chr)
    {

#ifdef SAM7X256
    /* Ensure transmit hold register is empty */
    while ((AT91C_BASE_US0->US_CSR & AT91C_US_TXRDY) == 0)
        ; /* NOP */

    /* Write next character to be transmitted */
    AT91C_BASE_US0->US_THR = chr;
#endif

    }
