/*
Copyright (c) 2007-2015, Tomi Parviainen
*/

#include "includes.h"
#ifdef SAM7X256
#include "at91sam7x256.h"
#else /* SAM7S256 */
#include "at91sam7s256.h"
#endif


/**
\fn void irq_enable_core(uint32_t old)
Returns the control bits to CPSR i.e. enables interrupts if those were
disabled based on the old control bits
\param old Control bits to be returned into CPSR

\fn uint32_t irq_disable_core(void)
Disables interrupts from core level
\return Original control bits from CPSR

\fn uint32_t irq_context(void)
Checks whether the caller is executing code in interrupt context or in task
context.
\return 1 if the caller's context is IRQ otherwise returns 0
*/


/*
Function prototype(s)
*/
static void irq_spurious(void);


/**
Initialises interrupt controller HW and global variables related to interrupt
handling.
*/
void irq_init()
    {

    uint8_t i;

    /* Perform 8 IT acknowledge (write any value in EOICR) */
    for (i = 0; i < 8 ; i++) {
        AT91C_BASE_AIC->AIC_EOICR = 0;
    }

    AT91C_BASE_AIC->AIC_SPU = (uint32_t)&irq_spurious;

    /* Enable the Debug mode */
    AT91C_BASE_AIC->AIC_DCR = AT91C_AIC_DCR_PROT;

    }


/**
Binds interrupt to function and returns the old function, which was bound
to this interrupt line i.e. makes it possible to chain interrupts

\param irq Interrupt to be bound for function
\param func Function to be called when interrupt occurs
\return Previously bound interrupt handler
*/
irq_fn irq_bind(irq_source irq, irq_fn func)
    {

    irq_fn old_func;

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));
    ASSERT(func != NULL);

    old_func = (irq_fn)AT91C_BASE_AIC->AIC_SVR[irq];

    AT91C_BASE_AIC->AIC_SVR[irq] = (uint32_t)func;

    /* Can return NULL */
    return old_func;

    }


/**
Unbinds interrupt i.e. clears the callback function

\pre Interrupt needs to be disabled before unbind
\param irq Interrupt to be unbound
*/
void irq_unbind(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));

    /* Interrupt should be disabled by the caller before unbind */
    ASSERT(AT91C_BASE_AIC->AIC_SVR[irq] == 0);

    AT91C_BASE_AIC->AIC_SVR[irq] = NULL;

    }


/**
Enables the interrupt

\pre Interrupt needs to be bound before enable
\param irq Interrupt to be enabled
*/
void irq_enable(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));
    ASSERT(AT91C_BASE_AIC->AIC_SVR[irq] != NULL);

    AT91C_BASE_AIC->AIC_IECR = (1 << irq);

    }


/**
Disables the interrupt

\param irq Interrupt to be disabled
*/
void irq_disable(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));

    AT91C_BASE_AIC->AIC_IDCR = (1 << irq);

    }


/**
Interrupt handler, checks the interrupt source and calls interrupt handler
which is responsible for clearing the peripheral interrupt
*/
void irq_handler()
    {

    irq_fn func;

    TR_IRQ(TR_FUNCTION);

    /* Read the highest priority interrupt handler address and execute
       the registered function */
    func = (irq_fn)AT91C_BASE_AIC->AIC_IVR;
    ASSERT(func != NULL);
    (*func)();

    /* Indicate that the interrupt treatment is complete */
    AT91C_BASE_AIC->AIC_EOICR = 0;

    }


/*
Spurious interrupt handler i.e. nIRQ was asserted, but no longer present
when AIC_IVR is read and because of that AIC_IVR returns the address of
spurious handler.
*/
void irq_spurious()
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("AIC_ISR", AT91C_BASE_AIC->AIC_ISR));
    TR_IRQ(trace_print_hex("AIC_IPR", AT91C_BASE_AIC->AIC_IPR));
    TR_IRQ(trace_print_hex("AIC_IMR", AT91C_BASE_AIC->AIC_IMR));

    }
