/*
Copyright (c) 2009, Tomi Parviainen
*/

#include "includes.h"
#ifdef SAM7X256
#include "at91sam7x256.h"
#else /* SAM7S256 */
#include "at91sam7s256.h"
#endif


/*
Macro that determines the base address of the PIOC based on the pin number
*/
#ifdef SAM7X256
#define PIO(pin) ((pin < PB0) ? AT91C_BASE_PIOA : AT91C_BASE_PIOB)
#else /* SAM7S256 */
#define PIO(pin) AT91C_BASE_PIOA
#endif

/*
Macro that determines the physical pin numbers (0-30 in SAM7)
*/
#ifdef SAM7X256
#define PIN(pin) ((pin < PB0) ? pin : (pin - PB0))
#else /* SAM7S256 */
#define PIN(pin) pin
#endif


/*
Variable declaration(s)
*/
static gpio_fn gpio_fn_table[GPIO_COUNT];


/*
Function prototype(s)
*/
static void gpio_irq_pioa(void);
#ifdef SAM7X256
static void gpio_irq_piob(void);
#endif


/**
Initialises GPIO controller. At reset, all I/O lines are automatically
configured as input with the programmable pull-up enabled.
*/
void gpio_init(void)
    {

    irq_fn func;
    uint32_t cnt;

    TR_GIO(TR_FUNCTION);

    for (cnt = 0; cnt < GPIO_COUNT; cnt++)
        {
        gpio_fn_table[cnt] = NULL;
        }

    func = irq_bind(PIOA, gpio_irq_pioa);
    ASSERT(func == NULL);
#ifdef SAM7X256
    func = irq_bind(PIOB, gpio_irq_piob);
    ASSERT(func == NULL);
#endif

    /* Enable PIO controller clocks */
#ifdef SAM7X256
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_PIOA)
                             | (1 << AT91C_ID_PIOB);
#else /* SAM7S256 */
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_PIOA);
#endif

    /* Flush ISR status registers because the first read of the register may
       read a different value as input changes may have occurred. */
    AT91C_BASE_PIOA->PIO_ISR;
#ifdef SAM7X256
    AT91C_BASE_PIOB->PIO_ISR;
#endif

    irq_enable(PIOA);
#ifdef SAM7X256
    irq_enable(PIOB);
#endif

    }


/**
Binds a function to specific GPIO pin. When the GPIO changes its state
and interrupt is enabled then the function is called.

\param pin The pin to where the functions will be bound to
\param func Callback function, executed when pin state change detected
*/
sint32_t gpio_irq_bind(gpio_pin pin, gpio_fn func)
    {

    sint32_t ret = 0;

    TR_GIO(TR_FUNCTION);
    TR_GIO(trace_print_dec("pin", (uint32_t)pin));
    TR_GIO(trace_print_hex("func", (uint32_t)func));
    ASSERT(func != NULL);
    ASSERT(pin < GPIO_COUNT);

    if (gpio_fn_table[pin] == NULL)
        {
        gpio_fn_table[pin] = func;
        }
    else
        {
        ret = -1;
        }

    return ret;

    }


/**
Unbinds the interrupt handler function of an I/O pin.

\param pin
*/
void gpio_irq_unbind(gpio_pin pin)
    {

    TR_GIO(TR_FUNCTION);
    ASSERT(pin < GPIO_COUNT);

    gpio_fn_table[pin] = NULL;

    }


/**
Enables the interrupt generation of an I/O pin.

\param pin
*/
void gpio_irq_enable(gpio_pin pin)
    {

    AT91PS_PIO pioc = PIO(pin); /* Parallel I/O Controller (A/B) */
    uint32_t io_pin = PIN(pin); /* Physical I/O pin */

    TR_GIO(TR_FUNCTION);
    ASSERT(gpio_fn_table[pin] != NULL);

    /* Enable the Input Change Interrupt on the I/O line */
    pioc->PIO_IER = (1 << io_pin);

    }


/**
Disables the interrupt generation of an I/O pin.

\param pin
*/
void gpio_irq_disable(gpio_pin pin)
    {

    AT91PS_PIO pioc = PIO(pin); /* Parallel I/O Controller (A/B) */
    uint32_t io_pin = PIN(pin); /* Physical I/O pin */

    TR_GIO(TR_FUNCTION);

    /* Disable the Input Change Interrupt on the I/O line */
    pioc->PIO_IDR = (1 << io_pin);

    }


/**
Configures the I/O pin.

\param pin The I/O pin to be configured
\param mode The configuration values for I/O pin
*/
void gpio_mode_set(gpio_pin pin, uint32_t mode)
    {

    AT91PS_PIO pioc = PIO(pin); /* Parallel I/O Controller (A/B) */
    uint32_t io_pin = PIN(pin); /* Physical I/O pin */

    TR_GIO(TR_FUNCTION);
    TR_GIO(trace_print_dec("pin", (uint32_t)pin));
    TR_GIO(trace_print_hex("mode", (uint32_t)mode));

    io_pin = (1 << io_pin);

    if (mode & GPIO_MODE_OUTPUT)
        {
        /* Enable the output on the I/O line */
        pioc->PIO_OER = io_pin;
        }
    else
        {
        ASSERT(mode & GPIO_MODE_INPUT);

        /* Disable the output on the I/O line -> input */
        pioc->PIO_ODR = io_pin;

        /* Enable the input glitch filter on the I/O line */
        pioc->PIO_IFER = io_pin;

        if (mode & GPIO_MODE_PULL_UP)
            {
            /* Enable the pull up resistor on the I/O line */
            pioc->PIO_PPUER = io_pin;
            }
        else
            {
            ASSERT(mode & GPIO_MODE_PULL_DOWN);

            /* Disable the pull up resistor on the I/O line */
            pioc->PIO_PPUDR = io_pin;
            }
        }

    /* Enable the PIO to control the corresponding pin */
    pioc->PIO_PER = io_pin;

    }


/**
Reads the level of the I/O line regardless of the configuration (input/output)

\param pin The I/O pin
\return The value of the I/O pin (1/0)
*/
uint32_t gpio_value_get(gpio_pin pin)
    {

    AT91PS_PIO pioc = PIO(pin); /* Parallel I/O Controller (A/B) */
    uint32_t io_pin = PIN(pin); /* Physical I/O pin */

    return pioc->PIO_PDSR & (1 << io_pin);

    }


/**
Sets the data to be driven on the I/O line.

\param pin The I/O pin to be driven
\param value The value (1/0) to be driven on the I/O line
*/
void gpio_value_set(gpio_pin pin, boolean_t value)
    {

    AT91PS_PIO pioc = PIO(pin); /* Parallel I/O Controller (A/B) */
    uint32_t io_pin = PIN(pin); /* Physical I/O pin */

    io_pin = (1 << io_pin);

    if (value != 0)
        {
        /* Set the data to be driven on the I/O line */
        pioc->PIO_SODR = io_pin;
        }
    else
        {
        /* Clear the data to be driven on the I/O line */
        pioc->PIO_CODR = io_pin;
        }

    }


/*
Parallel I/O Controller A IRQ handler function. Detects the source of the
interrupt and calls the function in case interrupt is enabled for that I/O
pin. This function is executed only in interrupt context.
*/
void gpio_irq_pioa()
    {

    gpio_fn func;
    uint32_t source;
    uint32_t status = AT91C_BASE_PIOA->PIO_ISR;
    uint32_t mask = AT91C_BASE_PIOA->PIO_IMR;

    TR_GIO(TR_FUNCTION);
    TR_GIO(trace_print_hex("sta", status));
    TR_GIO(trace_print_hex("mask", mask));

    /* The amount of IRQ sources is 31 not 32! */
    for(source = 0; source < 31; source++)
        {
        if (mask & (1 << source))
            {
            if (status & (1 << source))
                {
                func = gpio_fn_table[source];
                ASSERT(func != NULL);

                (*func)(source);
                }
            }
        }

    }


#ifdef SAM7X256
/*
Parallel I/O Controller B IRQ handler function. Detects the source of the
interrupt and calls the function in case interrupt is enabled for that I/O
pin. This function is executed only in interrupt context.
*/
void gpio_irq_piob()
    {

    gpio_fn func;
    uint32_t source;
    uint32_t status = AT91C_BASE_PIOB->PIO_ISR;
    uint32_t mask = AT91C_BASE_PIOB->PIO_IMR;

    TR_GIO(TR_FUNCTION);
    TR_GIO(trace_print_hex("sta", status));
    TR_GIO(trace_print_hex("mask", mask));

    /* The amount of IRQ sources is 31 not 32! */
    for(source = 0; source < 31; source++)
        {
        if (mask & (1 << source))
            {
            if (status & (1 << source))
                {
                func = gpio_fn_table[PB0 + source];
                ASSERT(func != NULL);

                (*func)(PB0 + source);
                }
            }
        }

    }
#endif

