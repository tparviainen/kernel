/**
\file
\brief Hardware specific definitions

This file contains hardware specific definitions for example the list
of main interrupts / GPIOs the system supports.

\author Copyright (c) 2007-2009, Tomi Parviainen
*/

#ifndef _HW_H_
#define _HW_H_


/**
Interrupt source list
*/
typedef enum
{
    FIQ = 0,
    SYS,
    PIOA,
#ifdef SAM7X256
    PIOB,
    SPI0,
    SPI1,
#else /* SAM7S256 */
    RESERVED_3,
    ADC,
    SPI,
#endif
    US0,
    US1,
    SSC,
    TWI,
    PWMC,
    UDP,
    TC0,
    TC1,
    TC2,
#ifdef SAM7X256
    CAN,
    EMAC,
    ADC,
#else /* SAM7S256 */
#endif
    IRQ0 = 30,
    IRQ1
} irq_source;


/**
PIO controllers (PIOA and PIOB) I/O lines
*/
typedef enum
{
    PA0 = 0,    /* PIO Controller A */
    PA1,
    PA2,
    PA3,
    PA4,
    PA5,
    PA6,
    PA7,
    PA8,
    PA9,
    PA10,
    PA11,
    PA12,
    PA13,
    PA14,
    PA15,
    PA16,
    PA17,
    PA18,
    PA19,
    PA20,
    PA21,
    PA22,
    PA23,
    PA24,
    PA25,
    PA26,
    PA27,
    PA28,
    PA29,
    PA30,

#ifdef SAM7X256
    PB0,        /* PIO Controller B */
    PB1,
    PB2,
    PB3,
    PB4,
    PB5,
    PB6,
    PB7,
    PB8,
    PB9,
    PB10,
    PB11,
    PB12,
    PB13,
    PB14,
    PB15,
    PB16,
    PB17,
    PB18,
    PB19,
    PB20,
    PB21,
    PB22,
    PB23,
    PB24,
    PB25,
    PB26,
    PB27,
    PB28,
    PB29,
    PB30,
#else /* SAM7S256 */
    PA31,
#endif

    GPIO_COUNT
} gpio_pin;


#endif /* _HW_H_ */
