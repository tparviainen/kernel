/**
\file
\brief CPU specific definitions for ARM processor

Copyright (c) 2007-2008, Tomi Parviainen
*/

#ifndef _CPU_H_
#define _CPU_H_

#include <stdint.h>

#define kernel_main main
#define memory_free_beg __heap_start
#define memory_free_end __heap_end

/*
Data types
*/
typedef unsigned char   boolean_t;  /**< boolean data type */
typedef char            sint8_t;    /**< signed 8-bit */
//typedef unsigned char   uint8_t;    /**< unsigned 8-bit */
typedef short           sint16_t;   /**< signed 16-bit */
//typedef unsigned short  uint16_t;   /**< unsigned 16-bit */
typedef int             sint32_t;   /**< signed 32-bit */
//typedef unsigned int    uint32_t;   /**< unsigned 32-bit */
typedef void            any_t;      /**< equivalent to void */
typedef uint8_t         stack_t;    /**< stack type */


#endif /* _CPU_H_ */
