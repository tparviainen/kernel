/*
Copyright (c) 2015, Tomi Parviainen

http://www.nongnu.org/avr-libc/user-manual/group__util__setbaud.html
*/

#include "includes.h"
#include <avr/io.h>
#define BAUD 9600
#include <util/setbaud.h>


/**
Initialises tracing system for dedicated trace port
*/
void trace_init()
    {

    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~_BV(U2X0);
#endif

    /* Enable receiver and transmitter */
    UCSR0B |= _BV(RXEN0);
    UCSR0B |= _BV(TXEN0);

    TR_SYS(trace_print(KERNEL_VERSION));

    }


/**
Prints character into trace port

\param chr Character to be printed
*/
void trace_print_char(const sint8_t chr)
    {

    /* Ensure USART data register is empty. If UDREn is one, the buffer 
       is empty, and therefore ready to be written. */
    loop_until_bit_is_set(UCSR0A, UDRE0);

    /* Write next character to be transmitted */
    UDR0 = chr;

    }
