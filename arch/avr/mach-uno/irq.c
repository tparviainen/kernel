/*
Copyright (c) 2015, Tomi Parviainen
*/

#include "includes.h"
#include <avr/io.h>
#include <avr/interrupt.h>


/**
\fn void irq_enable_core(uint32_t old)
Returns the control bits to CPSR i.e. enables interrupts if those were
disabled based on the old control bits
\param old Control bits to be returned into CPSR

\fn uint32_t irq_disable_core(void)
Disables interrupts from core level
\return Original control bits from CPSR

\fn uint32_t irq_context(void)
Checks whether the caller is executing code in interrupt context or in task
context.
\return 1 if the caller's context is IRQ otherwise returns 0
*/


/*
Variable declaration(s)
*/
static irq_fn irq_vector_table[IRQ_COUNT];


/*
Function prototype(s)
*/
static void irq_spurious(void);


ISR(INT0_vect) {
    irq_fn func;

    func = irq_vector_table[INT0];
    ASSERT(func != NULL);

    (*func)();
}

ISR(INT1_vect) {
    irq_fn func;

    func = irq_vector_table[INT1];
    ASSERT(func != NULL);

    (*func)();
}


/**
Initialises interrupt controller HW and global variables related to interrupt
handling.
*/
void irq_init()
    {

    uint32_t cnt;

    for (cnt = 0; cnt < IRQ_COUNT; cnt++)
        {
        irq_vector_table[cnt] = NULL;
        }

    }


/**
Binds interrupt to function and returns the old function, which was bound
to this interrupt line i.e. makes it possible to chain interrupts

\param irq Interrupt to be bound for function
\param func Function to be called when interrupt occurs
\return Previously bound interrupt handler
*/
irq_fn irq_bind(irq_source irq, irq_fn func)
    {

    irq_fn old_func;

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));
    ASSERT(func != NULL);

    old_func = irq_vector_table[irq];
    irq_vector_table[irq] = func;

    /* Can return NULL */
    return old_func;

    }


/**
Unbinds interrupt i.e. clears the callback function

\pre Interrupt needs to be disabled before unbind
\param irq Interrupt to be unbound
*/
void irq_unbind(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));

    /* Interrupt should be disabled by the caller before unbind */
    //ASSERT((PIC_IRQ_ENABLESET & (1 << (uint32_t)irq)) == 0);

    irq_vector_table[irq] = NULL;

    }


/**
Enables the interrupt

\pre Interrupt needs to be bound before enable
\param irq Interrupt to be enabled
*/
void irq_enable(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));
    ASSERT(irq_vector_table[irq] != NULL);

    switch (irq) {
        case INT0:
            EIMSK |= _BV(INT0);
            break;

        case INT1:
            EIMSK |= _BV(INT1);
            break;

        default:
            break;
    }

    }


/**
Disables the interrupt

\param irq Interrupt to be disabled
*/
void irq_disable(irq_source irq)
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("irq", (uint32_t)irq));

    switch (irq) {
        case INT0:
            EIMSK &= ~_BV(INT0);
            break;

        case INT1:
            EIMSK &= ~_BV(INT1);
            break;

        default:
            break;
    }

    }


/**
Interrupt handler, checks the interrupt source and calls interrupt handler
which is responsible for clearing the peripheral interrupt
*/
void irq_handler()
    {
#if 0
    irq_fn func;

    TR_IRQ(TR_FUNCTION);

    /* Read the highest priority interrupt handler address and execute
       the registered function */
    func = (irq_fn)AT91C_BASE_AIC->AIC_IVR;
    ASSERT(func != NULL);
    (*func)();

    /* Indicate that the interrupt treatment is complete */
    AT91C_BASE_AIC->AIC_EOICR = 0;
#endif
    irq_spurious();

    }


/*
Spurious interrupt handler i.e. nIRQ was asserted, but no longer present
when AIC_IVR is read and because of that AIC_IVR returns the address of
spurious handler.
*/
void irq_spurious()
    {

    TR_IRQ(TR_FUNCTION);
    TR_IRQ(trace_print_hex("AIC_ISR", AT91C_BASE_AIC->AIC_ISR));
    TR_IRQ(trace_print_hex("AIC_IPR", AT91C_BASE_AIC->AIC_IPR));
    TR_IRQ(trace_print_hex("AIC_IMR", AT91C_BASE_AIC->AIC_IMR));

    }
