/*
Copyright (c) 2015, Tomi Parviainen
*/

#include "includes.h"


/**
*/
void gpio_init(void)
    {
    }


/**
*/
sint32_t gpio_irq_bind(gpio_pin pin, gpio_fn func)
    {

    return 0;

    }


/**
*/
void gpio_irq_unbind(gpio_pin pin)
    {
    }


/**
*/
void gpio_irq_enable(gpio_pin pin)
    {
    }


/**
*/
void gpio_irq_disable(gpio_pin pin)
    {
    }


/**
*/
void gpio_mode_set(gpio_pin pin, uint32_t mode)
    {
    }


/**
*/
uint32_t gpio_value_get(gpio_pin pin)
    {

    return TRUE;

    }


/**
*/
void gpio_value_set(gpio_pin pin, boolean_t value)
    {
    }
