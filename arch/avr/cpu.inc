/*
Copyright (c) 2007-2008, Tomi Parviainen

Processor specific bits
*/

/* CPSR - Current Program Status Register
   NZCV_xxxx_xxxx_xxxx_xxxx_xxxx_IFTM_MMMM */

/* Processor modes (M-bits) */
.equ    mode_usr,   0x10
.equ    mode_fiq,   0x11
.equ    mode_irq,   0x12
.equ    mode_svc,   0x13
.equ    mode_abt,   0x17
.equ    mode_und,   0x1B
.equ    mode_sys,   0x1F
.equ    mode_mask,  0x1F

/* Interrupt disable bits */
.equ    i_bit,      0x80    /* When I bit is set, IRQ is disabled */
.equ    f_bit,      0x40    /* When F bit is set, FIQ is disabled */
