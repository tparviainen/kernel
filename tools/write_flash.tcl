# Command: script write_flash.tcl

# Ensure CPU is halted before reflashing
halt
wait_halt

# Write image to internal flash
flash write_bank 0 original/sam7-stop.bin 0x0
# flash write_bank 0 sam7-ex256-kernel.bin 0x0

# Resume from reset vector
reset
