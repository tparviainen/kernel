# qmake kernel.pro
TARGET = kernel

DEFINES += SAM7X256 DEBUG

# Kernel files
HEADERS += $$system(find ../../inc -iname "*.h")
HEADERS += $$system(find ../../arch/arm/ -maxdepth 1 -iname "*.h")
HEADERS += $$system(find ../../arch/arm/mach-at91/ -iname "*.h")

SOURCES += $$system(find ../../src -iname "*.c")
SOURCES += $$system(find ../../arch/arm/ -maxdepth 1 -iname "*.S")
SOURCES += $$system(find ../../arch/arm/mach-at91/ -iname "*.c")
SOURCES += $$system(find ../../bsp/at91 -iname "*.c")

# App files
HEADERS += $$system(find ../../app/at91 -maxdepth 2 -iname "*.h")
SOURCES += $$system(find ../../app/at91 -maxdepth 2 -iname "*.c")

OTHER_FILES += $$system(find ../../app/at91 -maxdepth 2 -iname "makefile")
OTHER_FILES += ../../arch/arm/mach-at91/makefile
OTHER_FILES += $$system(find ../../arch/arm/mach-at91/ -iname "*.ld")
OTHER_FILES += ../../readme.txt
OTHER_FILES += ../commands.txt
OTHER_FILES += $$system(find ../ -iname "*.tcl")

