# Command: script write_ram.tcl

halt
wait_halt
reset
halt
wait_halt

# Initialise SAM7-EX256 before executing code from RAM

# Disable watchdog
mww 0xfffffd44 0x00008000

# Enable user reset
mww 0xfffffd08 0xa5000001

# CKGR_MOR : enable the main oscillator (18,432MHz)
mww 0xfffffc20 0x00000601
sleep 100

# CKGR_PLLR: 96 MHz (DIV=24, MUL=124+1)
mww 0xfffffc2c 0x007c1c18
sleep 100

# PMC_MCKR : MCK = PLL/2 = 48 MHz
mww 0xfffffc30 0x00000007
sleep 100

# MC_FMR: flash mode (FWS=1, FMCN=73 >1,5uS)
mww 0xffffff60 0x00490100
sleep 100

# REMAP RAM from 0x00200000 to 0x00000000
mww 0xFFFFFF00 1
sleep 100

# The load address is RAM address before REMAP i.e. just to
# ensure that flash is not written in case of an error.
load_image sam7-ex256-kernel.bin 0x00200000 bin

# Resume at RESET vector
resume 0

