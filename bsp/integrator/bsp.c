/*
Copyright (c) 2007-2008, Tomi Parviainen

Board Support Package (BSP)
*/

#include "includes.h"


/*
Variable declaration(s)
*/


/*
BSP trace enable macro
*/
#define TR_BSP(a) a


/*
Function prototypes(s)
*/
static void bsp_tick_irq(void);


/*
Low level initialisation of HW
*/
void bsp_init_1()
    {

    }


/*
Second level initialisation of HW, now the kernel is initialised and the
services of it can be used, time to initialise scheduler interrupt
*/
void bsp_init_2()
    {

    irq_fn func;

    TR_BSP(TR_FUNCTION);

    func = irq_bind(TIMERINT1, bsp_tick_irq);
    ASSERT(func == NULL);

    TIMER_1_LOAD = 1000; /* Generate 1ms timer tick */
    //TIMER_1_LOAD = 10000; /* Generate 10ms timer tick */
    //TIMER_1_LOAD = 100000; /* Generate 100ms timer tick */
    //TIMER_1_LOAD = 1000000; /* Generate 1000ms timer tick */
    /* TIMER_1_BG_LOAD = 1000000; should this be used ??? */

    /* TIMERINT1 - Fixed frequency of 1MHz */
    /* - periodic, counts down zero and then reloads the period value */
    /* - enable, TIMERx_CTRL (prescale / mode) */
    /* - load, TIMERx_LOAD */
    /* - IRQ clear, TIMERx_CLR */
    TIMER_1_CONTROL = (1 << 7) | (1 << 6) | (1 << 5) | (1 << 1);

    irq_enable(TIMERINT1);

    }


/*
Third level initialisation, after this multitasking will be started
*/
void bsp_init_3()
    {

    tcb_t* tcb;

    TR_BSP(TR_FUNCTION);

    /* Creates IDLE task (lowest possible priority task in system), which is
       always ready to run */
    tcb = task_create(task_idle, NULL, task_idle_stk_size, task_idle_priority, task_idle_name);
    ASSERT(tcb != NULL);

    }


/*
Tick timer interrupt
*/
void bsp_tick_irq()
    {

    TIMER_1_INT_CLR = 0x0;

    system_tick();

    }
