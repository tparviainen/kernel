/*
Copyright (c) 2007-2010, Tomi Parviainen

Board Support Package (BSP) for Olimex SAM7-EX256 / SAM7-H256
*/

#include "includes.h"
#ifdef SAM7X256
#include "at91sam7x256.h"
#else /* SAM7S256 */
#include "at91sam7s256.h"
#endif


/*
Function prototypes(s)
*/
static void bsp_tick_irq(void);


/*
BSP trace enable macro
*/
#define TR_BSP(a)


/*
Low level initialisation of HW, performed as described in chapter 25.7 in
AT91SAM7X256 preliminary specification.
*/
void bsp_init_1()
    {

    /* Embedded flash controller (EFC), 1 FWS when MCK < 55 MHz. FMCN also
       programmed to correct value even though it is not needed unless actual
       write to flash will be performed (48 with 48MHz MCK). */
    AT91C_BASE_MC->MC_FMR = (AT91C_MC_FMCN & (48 << 16))
                          | AT91C_MC_FWS_1FWS;

    /* Enable main oscillator (18.432MHz) and wait for it to stabilize.
       Estimated startup time for oscillator is ~2ms = 8 SLCK */
    AT91C_BASE_PMC->PMC_MOR = (AT91C_CKGR_OSCOUNT & (8 << 8))
                            | AT91C_CKGR_MOSCEN;
    while ((AT91C_BASE_PMC->PMC_SR & AT91C_PMC_MOSCS) == 0)
        ; /* NOP */

    /* Enable/set phase lock loop (PLL) and wait for it to stabilize,
       used configuration is: DIV=24, MUL=125 --> 96MHz. Estimated PLL
       startup time is ~1ms = 32 SLCK */
    AT91C_BASE_PMC->PMC_PLLR = (AT91C_CKGR_PLLCOUNT & (32 << 8))
                             | (AT91C_CKGR_MUL & (124 << 16))
                             | (AT91C_CKGR_DIV & 24);
    while ((AT91C_BASE_PMC->PMC_SR & AT91C_PMC_LOCK) == 0)
        ; /* NOP */

    /* Set prescaling value (2) of the main clock --> 48MHz */
    AT91C_BASE_PMC->PMC_MCKR = AT91C_PMC_PRES_CLK_2;
    while ((AT91C_BASE_PMC->PMC_SR & AT91C_PMC_MCKRDY) == 0)
        ; /* NOP */

    /* Select PLL clock with the output of 48MHz for master clock (MCK)
       divider source */
    AT91C_BASE_PMC->PMC_MCKR |= AT91C_PMC_CSS_PLL_CLK;
    while ((AT91C_BASE_PMC->PMC_SR & AT91C_PMC_MCKRDY) == 0)
        ; /* NOP */

    /* Disable watchdog */
    AT91C_BASE_WDTC->WDTC_WDMR = AT91C_WDTC_WDDIS;

    /* Enable user reset i.e. low level on the pin NRST triggers reset */
    AT91C_BASE_RSTC->RSTC_RMR = (0xA5 << 24) | AT91C_RSTC_URSTEN;

    }


/*
Second level initialisation of HW, now the kernel is initialised and the
services of it can be used, time to initialise scheduler interrupt
*/
void bsp_init_2()
    {

    irq_bind(SYS, bsp_tick_irq);
    irq_enable(SYS);

    /* Initialisation for 1ms periodic interval timer, used formula is:
       (48MHz / 16) * 1mS - 1 = 2999 */
    AT91C_BASE_PITC->PITC_PIMR = 2999;

    /* Enable the interrupt generation, once PITS gets set */
    AT91C_BASE_PITC->PITC_PIMR |= AT91C_PITC_PITIEN;

    /* Enable periodic interval timer */
    AT91C_BASE_PITC->PITC_PIMR |= AT91C_PITC_PITEN;

    }


/*
Third level initialisation, after this multitasking will be started
*/
void bsp_init_3()
    {

    tcb_t* tcb;

    /* Creates IDLE task (lowest possible priority task in system), which is
       always ready to run */
    tcb = task_create(task_idle, NULL, task_idle_stk_size, task_idle_priority, task_idle_name);
    ASSERT(tcb != NULL);

    }


/*
Tick interrupt, called every ms to tick kernel
*/
void bsp_tick_irq()
    {

    uint32_t status;
    uint32_t value;

    status = AT91C_BASE_PITC->PITC_PISR;
    if (status & AT91C_PITC_PITS)
        {
        /* Acknowledge PIT IRQ */
        value = AT91C_BASE_PITC->PITC_PIVR;

        system_tick();
        }
    else
        {
        ASSERT(0);
        }

    }
