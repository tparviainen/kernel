/*
Copyright (c) 2015, Tomi Parviainen

Board Support Package (BSP) for Arduino Uno
*/

#include "includes.h"
#include <avr/io.h>
#include <avr/interrupt.h>


/*
BSP trace enable macro
*/
#define TR_BSP(a)


/*
Low level initialisation of HW
*/
void bsp_init_1()
    {

    // Set timer prescale factor to 64 (16MHz --> 250kHz)
    TCCR0B |= _BV(CS00);
    TCCR0B |= _BV(CS01);

    // Enable CTC mode (clear timer on compare match)
    TCCR0A = _BV(WGM01);

    // Give interrupt every 250 timer clocks (0 - 249). This
    // means that interrupt is fired 1000 times in a second.
    OCR0A = 0xF9;

    }


/*
Second level initialisation of HW, now the kernel is initialised and the
services of it can be used, time to initialise scheduler interrupt.
*/
void bsp_init_2()
    {

    // Enable output compare interrupt
    TIMSK0 |= _BV(OCIE0A);

    }


/*
Third level initialisation, after this multitasking will be started
*/
void bsp_init_3()
    {

    tcb_t* tcb;

    /* Creates IDLE task (lowest possible priority task in system), which is
       always ready to run */
    tcb = task_create(task_idle, NULL, task_idle_stk_size, task_idle_priority, task_idle_name);
    ASSERT(tcb != NULL);

    }


/*
Tick interrupt, called every ms to tick kernel
*/
ISR(TIMER0_COMPA_vect) {
    // system_tick();
}
