/*
Copyright (c) 2007-2010, Tomi Parviainen
*/

#include "includes.h"


/*
Variable declaration(s)
*/
static const sint8_t* const alphanumeric = "0123456789abcdef";


/*
Function prototype(s)
*/
static void trace_uint2base(uint32_t value, uint32_t base);


/**
Prints the string to trace port and appends newline characters to the end
of the string.

\param str The string to be printed
*/
void trace_print(const sint8_t* str)
    {

    while (*str != NULL)
        {
        trace_print_char(*str++);
        }

    /* Append newline to the end of string */
    trace_print_char(0xd);
    trace_print_char(0xa);

    }


/**
Prints the time difference between the previous usage and the current usage of
this function. The maximum difference between measurements can be 60 seconds
with the resolution of one millisecond. This function is meant to be used for
profiling purposes!
*/
void trace_print_diff()
    {

    time_t now;
    uint32_t diff;
    static time_t prev = {0, 0, 0, 0, 0};

    time_uptime(&now);

    /* Check whether minute boundary is crossed */
    if (now.sec < prev.sec)
        {
        diff = (now.sec + 60 - prev.sec) * 1000;
        }
    else
        {
        diff = (now.sec - prev.sec) * 1000;
        }

    diff += (now.ms - prev.ms);

    trace_print_hex("diff", diff);

    prev = now;

    }


/**
Prints the string and value (in hexadecimal format) to trace port and appends
newline characters to the end of the string.

\param str The string to be printed
\param value The value to be printed in hexadecimal format
*/
void trace_print_hex(const sint8_t* str, uint32_t value)
    {

    uint8_t cnt;

    while (*str != NULL)
        {
        trace_print_char(*str++);

        /* Space is printed here so that it is possible to print plain numbers
           without preceding space character i.e. str being NULL */
        if (*str == NULL)
            {
            trace_print_char(' ');
            }
        }

    trace_print_char('0');
    trace_print_char('x');

    cnt = 0;

    /* The length of HEX value is always 8 ie. 0x12ABCD78 */
    while (cnt++ < 8)
        {
        /* Take one number at a time and print it */
        uint8_t chr = 0xF & (value >> (32 - 4 * cnt));
        trace_print_char(alphanumeric[chr]);
        }

    /* Append newline to the end of string */
    trace_print_char(0xd);
    trace_print_char(0xa);

    }


/**
Prints the content of the TCB to trace port. All of the values of the TCB are
printed including registers from task's stack.

\note If the task is currently running then the register values might not show
correct values, because stack usage might have caused the values to be
overwritten!

\see tcb_t
\param task Task's TCB to be printed
*/
void trace_print_tcb(const tcb_t* task)
    {

    trace_print(task->name);
    trace_print_hex("this", (uint32_t)task);
    trace_print_hex("stck", (uint32_t)task->stk);
    trace_print_hex("   PC", (uint32_t)task->stk[15]);
    trace_print_hex("   LR", (uint32_t)task->stk[14]);
    trace_print_hex("  R12", (uint32_t)task->stk[13]);
    trace_print_hex("  R11", (uint32_t)task->stk[12]);
    trace_print_hex("  R10", (uint32_t)task->stk[11]);
    trace_print_hex("   R9", (uint32_t)task->stk[10]);
    trace_print_hex("   R8", (uint32_t)task->stk[9]);
    trace_print_hex("   R7", (uint32_t)task->stk[8]);
    trace_print_hex("   R6", (uint32_t)task->stk[7]);
    trace_print_hex("   R5", (uint32_t)task->stk[6]);
    trace_print_hex("   R4", (uint32_t)task->stk[5]);
    trace_print_hex("   R3", (uint32_t)task->stk[4]);
    trace_print_hex("   R2", (uint32_t)task->stk[3]);
    trace_print_hex("   R1", (uint32_t)task->stk[2]);
    trace_print_hex("   R0", (uint32_t)task->stk[1]);
    trace_print_hex(" CPSR", (uint32_t)task->stk[0]);

    trace_print_hex("next", (uint32_t)task->next);
    trace_print_hex("prev", (uint32_t)task->prev);
    trace_print_hex("tslp", (uint32_t)task->time_sleep);
    trace_print_hex("tslc", (uint32_t)task->time_slice);
    trace_print_hex("prio", (uint32_t)task->priority);

    }


/**
Prints all TCBs from all priority levels including the ones in a sleep list.
*/
void trace_print_tcb_all(void)
    {

    uint8_t priority;

    trace_print("<g_tcb_ready>");
    for (priority = 0; priority != TASK_PRIORITY_COUNT; priority++)
        {
        trace_print_hex("<priority>", priority);
        if (g_tcb_ready[priority] != NULL)
            {
            trace_print_tcb_list(g_tcb_ready[priority]);
            }
        }

    if (g_tcb_sleep != NULL)
        {
        trace_print("<g_tcb_sleep>");
        trace_print_tcb_list(g_tcb_sleep);
        }

    }


/**
Prints the list of the TCBs to trace port. List can be linearly or circularly
linked.

\see trace_print_tcb
\param list List to be printed
*/
void trace_print_tcb_list(const tcb_t* list)
    {

    /* First TCB to be saved so that the loop of circularly linked list is
       stopped correctly */
    const tcb_t* first = list;

    while (list != NULL)
        {
        trace_print_tcb(list);
        list = list->next; /* Can be NULL in linearly linked list */

        if (list == first)
            {
            list = NULL;
            }
        }

    }


/**
Prints the string and value (in decimal format) to trace port and appends
newline characters to the end of the string.

\param str The string to be printed
\param value The value to be printed in decimal format
*/
void trace_print_dec(const sint8_t* str, uint32_t value)
    {

    while (*str != NULL)
        {
        trace_print_char(*str++);

        /* Space is printed here so that it is possible to print plain numbers
           without preceding space character i.e. str being NULL */
        if (*str == NULL)
            {
            trace_print_char(' ');
            }
        }

    trace_uint2base(value, 10);

    /* Append newline to the end of string */
    trace_print_char(0xd);
    trace_print_char(0xa);

    }


/**
Prints the value in specified base (10, 16, etc.). Please note that because
this function uses recursion it consumes stack quite much!

\param value Value to be printed in specific base
\param base Number base (8, 10, 16, ...)
*/
void trace_uint2base(uint32_t value, uint32_t base)
    {

    uint32_t quotient = (value / base);

    if (quotient != 0)
        {
        trace_uint2base(quotient, base);
        }

    trace_print_char(alphanumeric[value % base]);

    }
