/*
Copyright (c) 2007-2008, Tomi Parviainen
*/

#include "includes.h"


/*
Memory pointer, which points to the current location of usable memory
*/
static stack_t* p_mem;


/**
Initialises memory management system
*/
void memory_init()
    {

    /* The base address for memory is divisible by 4 because of the definitions
       in linker script file */
    p_mem = &memory_free_beg;

    }


/**
Performs dynamic memory allocation from the kernel heap. Nothing is allocated
when the length argument has a value of zero.

\param length The amount of memory to be allocated (in bytes)
\return Pointer to the start of allocated memory
*/
any_t* memory_alloc(uint32_t length)
    {

    uint32_t irq_bits;
    stack_t* original;

    TR_MEM(TR_FUNCTION);
    TR_MEM(trace_print_hex("size", length));
    TR_MEM(trace_print_hex("free", 4 * (&memory_free_end - p_mem)));

    /* Ensure the length is divisible by 'stack_t' */
    if ((length % sizeof(stack_t)) != 0)
        {
        length += (sizeof(stack_t) - (length & 0x3));
        }

    /* From now on, length is the amount of 'stack_t' item to be allocated */
    length /= sizeof(stack_t);

    irq_bits = irq_disable_core();

    original = p_mem;
    p_mem += length;

    /* Rollback in case memory overflow detected */
    if (p_mem > &memory_free_end)
        {
        TR_MEM(trace_print("overflow"));
        p_mem -= length;
        original = NULL;
        }

    irq_enable_core(irq_bits);

    TR_MEM(trace_print_hex("start", (uint32_t)original));
    TR_MEM(trace_print_hex("stop", (uint32_t)p_mem));

    return original;

    }


/**
Deallocates the memory allocated by memory_alloc

\param ptr Pointer to the area to be deallocated
*/
void memory_free(any_t* ptr)
    {

    TR_MEM(TR_FUNCTION);

    ptr = ptr;

    }


/**
Fills memory with a constant value

\param dst Start address of memory set
\param chr Character to be set into address
\param length The amount of bytes to be set
*/
void memory_set(any_t* dst, uint8_t chr, uint32_t length)
    {

    /* Instead of using 32-bit void pointer, 8-bit pointer is used */
    uint8_t* p_dst = (uint8_t*)dst;

    /* Duff�s Device, see http://en.wikipedia.org/wiki/Duff%27s_device */
    uint32_t n = (length + 7) / 8;

    TR_MEM(TR_FUNCTION);
    ASSERT(length != 0);

    switch (length % 8)
        {
        case 0: do {    *(p_dst++) = chr;
        case 7:         *(p_dst++) = chr;
        case 6:         *(p_dst++) = chr;
        case 5:         *(p_dst++) = chr;
        case 4:         *(p_dst++) = chr;
        case 3:         *(p_dst++) = chr;
        case 2:         *(p_dst++) = chr;
        case 1:         *(p_dst++) = chr;
                } while (--n > 0);
        }

    }


/**
The memory_cpy function copies length characters from the buffer pointed to
by src into the buffer pointed to by dst.

\param dst Destination buffer
\param src Source buffer
\param length The amount of bytes to copy
\return The original value of destination buffer
*/
any_t* memory_cpy(any_t* dst, const any_t* src, uint32_t length)
    {

    /* Instead of using 32-bit void pointers, 8-bit pointers are used */
    uint8_t* p_dst = (uint8_t*)dst;
    const uint8_t* p_src = (const uint8_t*)src;

    /* Duff�s Device, see http://en.wikipedia.org/wiki/Duff%27s_device */
    uint32_t n = (length + 7) / 8;

    TR_MEM(TR_FUNCTION);
    ASSERT(length != 0);

    switch (length % 8)
        {
        case 0: do {    *(p_dst++) = *(p_src++);
        case 7:         *(p_dst++) = *(p_src++);
        case 6:         *(p_dst++) = *(p_src++);
        case 5:         *(p_dst++) = *(p_src++);
        case 4:         *(p_dst++) = *(p_src++);
        case 3:         *(p_dst++) = *(p_src++);
        case 2:         *(p_dst++) = *(p_src++);
        case 1:         *(p_dst++) = *(p_src++);
                } while (--n > 0);
        }

    return dst;

    }
