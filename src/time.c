/*
Copyright (c) 2007-2008, Tomi Parviainen
*/

#include "includes.h"


/*
Function prototype(s)
*/
static void time_tick_sleep(void);
static void timer_callback(void);


/**
Updates the system tick and goes through the sleep and timer lists if there
are tasks which needs to be awaken.
*/
void time_tick()
    {

    /* Update the uptime of the system */
    if (++g_uptime.ms == 1000)
        {
        g_uptime.ms = 0;

        PROF_SP(profiler_stack_usage());
        PROF_CPU_USAGE(profiler_cpu_usage());

        if (++g_uptime.sec == 60)
            {
            g_uptime.sec = 0;

            if (++g_uptime.min == 60)
                {
                g_uptime.min = 0;

                if (++g_uptime.hour == 24)
                    {
                    g_uptime.hour = 0;
                    g_uptime.day++;
                    }
                }
            }
        }

    /* Check the need to wake up task from the sleep list */
    if (g_tcb_sleep != NULL)
        {
        time_tick_sleep();
        }

    /* Check is there timer callback functions in the list */
    if (g_timer_callback != NULL)
        {
        timer_callback();
        }

    }


/**
Returns the uptime to the caller. The uptime starts from 0 when the powers
of the system are turned ON to the device and it is updated at the frequency
of timer tick (1ms).

\param uptime On return contains the system uptime
*/
void time_uptime(time_t* uptime)
    {

    ASSERT(uptime != NULL);

    *uptime = g_uptime;

    }


/*
Goes through the sleep queue by decrementing sleep time of first task and
wakes up the task in case the sleep time goes to 0.
*/
void time_tick_sleep()
    {

    tcb_t* tcb;

    TR_TME(TR_FUNCTION);

    g_tcb_sleep->time_sleep--;

    if (g_tcb_sleep->time_sleep == 0)
        {
        tcb = g_tcb_sleep;

        /* Need to go through the list until all tasks with same sleep time
           are moved into ready list */
        while (tcb != NULL)
            {
            g_tcb_sleep = tcb->next;

            task_add(tcb);

            if (g_tcb_sleep == NULL)
                {
                tcb = NULL;
                }
            else
                {
                if (g_tcb_sleep->time_sleep == 0)
                    {
                    /* Need to wake up also next task */
                    tcb = g_tcb_sleep;
                    }
                else
                    {
                    tcb = NULL;
                    }
                }
            }
        }

    }


/**
Initialises timer callback structure, must be called before timer can be used.

\param timer Timer callback to be initialised
\param func Callback function when timer expires
\param param Parameter for callback function
*/
void timer_init(timer_t* timer, timer_fn func, any_t* param)
    {

    TR_TME(TR_FUNCTION);
    TR_TME(trace_print_hex("timer", (uint32_t)timer));
    TR_TME(trace_print_hex("param", (uint32_t)param));
    ASSERT(timer != NULL);
    ASSERT(func != NULL);

    timer->milliseconds = 0;
    timer->next = NULL;
    timer->func = func;
    timer->param = param;

    }


/**
Starts a one-shot timer with specified amount of milliseconds. After the
timer expires the callback function is called in IRQ context. Function can be
used from IRQ context.

\param timer Timer to be started
\param milliseconds Time in milliseconds after the timer expires
*/
void timer_start(timer_t* timer, uint32_t milliseconds)
    {

    timer_t* prev;
    timer_t* current;
    uint32_t irq_bits;

    TR_TME(TR_FUNCTION);
    TR_TME(trace_print_hex("milliseconds", milliseconds));

    /* In order to cause delay, which is at least requested amount of ms */
    milliseconds++;

    irq_bits = irq_disable_core();

    if (g_timer_callback == NULL)
        {
        g_timer_callback = timer;
        g_timer_callback->next = NULL;
        }
    else
        {
        prev = NULL;
        current = g_timer_callback;

        while (current != NULL)
            {
            /* If longer delay than current then need to go to next item */
            if (milliseconds >= current->milliseconds)
                {
                milliseconds -= current->milliseconds;
                prev = current;
                current = current->next;

                /* Last item? */
                if (current == NULL)
                    {
                    prev->next = timer;
                    timer->next = NULL;
                    }
                }
            else
                {
                /* Correct the delay of next timer, because the delay for
                   that is now 'milliseconds' less because of this timer */
                current->milliseconds -= milliseconds;

                if (prev == NULL)
                    {
                    /* First item in the list */
                    timer->next = current;
                    g_timer_callback = timer;
                    }
                else
                    {
                    /* New timer will be put in the middle of the list */
                    prev->next = timer;
                    timer->next = current;
                    }

                current = NULL;
                }
            }
        }

    /* Timer has a new delta delay based on the location in the list */
    timer->milliseconds = milliseconds;

    irq_enable_core(irq_bits);

    }


/*
Decrements timer callback list's first item and in case delay becomes zero
then calls the callback function. This function is only called if there are
timers in the list.
*/
void timer_callback()
    {

    timer_fn func;
    timer_t* timer;

    ASSERT(g_timer_callback != NULL);

    g_timer_callback->milliseconds--;

    if (g_timer_callback->milliseconds == 0)
        {
        do
            {
            /* Set new head for timer list */
            timer = g_timer_callback;
            g_timer_callback = timer->next;

            /* Timer expired, callback function to be called */
            func = timer->func;
            ASSERT(func != NULL);
            (*func)(timer->param);

            /* Stop if there is no expiring timers in list */
            if (g_timer_callback == NULL)
                {
                timer = NULL;
                }
            else if (g_timer_callback->milliseconds != 0)
                {
                timer = NULL;
                }
            } while (timer != NULL);
        }

    }
