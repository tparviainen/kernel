/*
Copyright (c) 2007-2008, Tomi Parviainen
*/

#include "includes.h"


/**
\fn void system_spin_lock(uint32_t* lock)
Acquires given lock
\param lock Lock to be acquired

\fn void system_spin_unlock(uint32_t* lock)
Releases given lock
\param lock Lock to be released
*/

/**
Called periodically by BSP to update system tick.
*/
void system_tick()
    {

    time_tick();

    scheduler_tick();

    }
