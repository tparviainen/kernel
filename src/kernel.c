/*
Copyright (c) 2007-2015, Tomi Parviainen
*/

#include "includes.h"


/**
Kernel entrypoint, this is the first C-function which is called after
the bootstrap has performed it's operations.
*/
int kernel_main()
    {

    // C:\Program Files (x86)\Arduino\hardware\arduino\cores\arduino\wiring.c
    // C:\Program Files (x86)\Arduino\hardware\tools\avr\avr\include

    /* Initialise all kernel services, starting from BSP */
    bsp_init_1();
    globals_init();
    //memory_init();
    trace_init();
    irq_init();
    gpio_init();
/*
    TR_SYS(trace_print_hex("initial free memory", (uint32_t)&memory_free_end -
                                                  (uint32_t)&memory_free_beg));
    TR_SYS(trace_print_hex("irq stack", (uint32_t)&__stack_irq__));
    TR_SYS(trace_print_hex("svc stack", (uint32_t)&__stack_svc__));
*/

    /* After all services are initialised second init for BSP */
    bsp_init_2();

    /* Initialise application specific code */
    app_init();

    /* Last change for BSP to init services before scheduler is started */
    bsp_init_3();

    /* Start scheduler, this method never returns */
    scheduler_start();

    }
