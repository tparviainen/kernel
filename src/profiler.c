/*
Copyright (c) 2007-2009, Tomi Parviainen
*/

#include "includes.h"


/**
Prints the CPU utilization rate.
*/
void profiler_cpu_usage()
    {

    uint32_t cpu_usage;

    /* Re-calibrate maximum IDLE task count on the fly */
    if (g_task_idle_count > g_task_idle_count_max)
        {
        g_task_idle_count_max = g_task_idle_count;
        }

    /* Calculate CPU usage, based on IDLE task execution time */
    cpu_usage = (g_task_idle_count_max - g_task_idle_count) * 100;
    cpu_usage /= g_task_idle_count_max;

    g_task_idle_count = 0;

    trace_print(">PROF_CPU_USAGE");
    trace_print_hex("cpu_usage", cpu_usage);
    trace_print("<PROF_CPU_USAGE");

    }


/**
Prints the stack usage of the tasks in ready queue.
*/
void profiler_stack_usage()
    {

    tcb_t* tcb_sp;
    tcb_t* tcb_hi;
    uint32_t cnt;
    uint32_t all = 0;
    uint8_t priority = TASK_PRIORITY_COUNT;

    trace_print(">PROF_SP");

    trace_print("RDY");

    while (priority > 0)
        {
        priority--;

        /* Only the tasks in ready queue are printed */
        tcb_sp = g_tcb_ready[priority];
        tcb_hi = tcb_sp;

        while (tcb_sp != NULL)
            {
            all++;

            for (cnt = 0; cnt < tcb_sp->stk_size; cnt++)
                {
                /* Check the point SP has reached */
                if (tcb_sp->stk_base[cnt] != 0xA5)
                    {
                    trace_print(tcb_sp->name);
                    trace_print_hex("stk_base", (uint32_t)tcb_sp->stk_base);
                    trace_print_dec("stk_free", cnt);
                    trace_print_dec("stk_used", tcb_sp->stk_size - cnt);

                    cnt = tcb_sp->stk_size;
                    }
                }

            tcb_sp = tcb_sp->next;

            if (tcb_sp == tcb_hi)
                {
                tcb_sp = NULL;
                }
            }
        }

    trace_print("SLP");

    if (g_tcb_sleep != NULL)
        {
        tcb_sp = g_tcb_sleep;

        while (tcb_sp != NULL)
            {
            all++;

            for (cnt = 0; cnt < tcb_sp->stk_size; cnt++)
                {
                /* Check the point SP has reached */
                if (tcb_sp->stk_base[cnt] != 0xA5)
                    {
                    trace_print(tcb_sp->name);
                    trace_print_hex("stk_base", (uint32_t)tcb_sp->stk_base);
                    trace_print_dec("stk_free", cnt);
                    trace_print_dec("stk_used", tcb_sp->stk_size - cnt);

                    cnt = tcb_sp->stk_size;
                    }
                }

            tcb_sp = tcb_sp->next;
            }
        }

    trace_print("IRQ");

    /* \todo IRQ stack usage */

    trace_print_hex("ALL", all);

    trace_print("<PROF_SP");

    }
