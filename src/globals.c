/*
Copyright (c) 2007-2008, Tomi Parviainen
*/

#include "includes.h"


/*
Variable declaration(s)
*/

/**
Ready list for tasks which are ready to be executed. Ready list is
implemented as double circularly linked list.
\see TASK_PRIORITY_COUNT
*/
tcb_t* g_tcb_ready[TASK_PRIORITY_COUNT];

/**
Task which is currently being executed on CPU
*/
tcb_t* g_tcb_current;

/**
Task to be rescheduled immediately when appropriate
*/
tcb_t* g_tcb_reschedule;

/**
Sleep list for tasks, which are currently sleeping and waiting the sleep
period to elapse. Sleep list is implemented as single linearly linked list.
*/
tcb_t* g_tcb_sleep;

/**
Kernel uptime value, this is updated every timer tick and starts to count up
when the system starts.
*/
time_t g_uptime;

/**
Timer callback list
*/
timer_t* g_timer_callback;

#ifdef PROF_CPU_USAGE
/**
IDLE count value, increment as much as possible when IDLE task is executed.
*/
uint32_t g_task_idle_count;

/**
Maximum count value IDLE task has reached, re-calibrated automatically during
the execution time.
*/
uint32_t g_task_idle_count_max;
#endif


/**
Initialises global variables
*/
void globals_init()
    {

    uint32_t cnt;

    for (cnt = 0; cnt < (TASK_PRIORITY_COUNT - 1); cnt++)
        {
        g_tcb_ready[cnt] = NULL;
        }

    g_tcb_current = NULL;
    g_tcb_reschedule = NULL;
    g_tcb_sleep = NULL;

    /* Uptime calculation starts from zero */
    g_uptime.ms = 0;
    g_uptime.sec = 0;
    g_uptime.min = 0;
    g_uptime.hour = 0;
    g_uptime.day = 0;

    g_timer_callback = NULL;

#ifdef PROF_CPU_USAGE
    g_task_idle_count = 0;
    g_task_idle_count_max = 1;  /* Prevent divide by zero! */
#endif /* PROF_CPU_USAGE */

    }
