/*
Copyright (c) 2007-2015, Tomi Parviainen
*/

#include "includes.h"

/**
\fn void scheduler_initial_context(stack_t* stack)
Starts the initial task i.e. no previous context to be saved!
\param stack Pointer to stack of the task to be started

\fn void scheduler_context_switch(tcb_t* tcb_new)
Performs context switch from task context
\pre Must be called from task context
\param tcb_new TCB of the task to be scheduled to run

\fn void scheduler_context_switch_irq(tcb_t* tcb_new)
Performs context switch from IRQ context
\pre Must be called from IRQ context
\param tcb_new TCB of the task to be scheduled to run
*/

/**
Starts the scheduler with the highest priority task ready to run. Interrupts
are not yet enabled but will be enabled immediately after the first task is
moved into running state.

\note This function never returns
*/
void scheduler_start()
    {

    TR_SCH(TR_FUNCTION);
    ASSERT(g_tcb_current == NULL);

    g_tcb_current = task_highest_ready();

    scheduler_initial_context(g_tcb_current->stk);

    }


/**
Updates the timer tick and initiates context switch in round robin fashion in
case the current task has consumed it's time slice.

\pre Must be called from IRQ context
*/
void scheduler_tick()
    {

    tcb_t* tcb;

    g_tcb_current->time_slice--;

    /* Context switch in case task has consumed it's time slice */
    if (g_tcb_current->time_slice == 0)
        {
        g_tcb_current->time_slice = TIME_SLICE;

        tcb = g_tcb_current->next;

        if (tcb != g_tcb_current)
            {
            g_tcb_ready[tcb->priority] = tcb;
            scheduler_reschedule(tcb);
            }
        }

    }


/**
Performs reschedule, can be called from IRQ or from task context. If called
from IRQ context, the task context switch is delayed to the point where the
processor mode changes back to SVC.

\param tcb TCB of the new task to be scheduled to run
*/
void scheduler_reschedule(tcb_t* tcb)
    {

    TR_SCH(TR_FUNCTION);

    if (irq_context())
        {
        if (g_tcb_reschedule == NULL)
            {
            g_tcb_reschedule = tcb;
            }
        else
            {
            /* This function can be called several times during one IRQ (time
               slice, sleep list, etc.) thus need to ensure that the reschedule
               flag points to highest task */
            if (g_tcb_reschedule->priority < tcb->priority)
                {
                g_tcb_reschedule = tcb;
                }
            }
        }
    else
        {
        scheduler_context_switch(tcb);
        }

    }
