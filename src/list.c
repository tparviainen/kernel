/*
Copyright (c) 2007-2008, Tomi Parviainen
*/

#include "includes.h"


/**
Adds an item to linearly linked list.

\param list List to where the item will be added
\param p_tcb Item to be added into list
\return Pointer to the start of the list
*/
tcb_t* single_add_last(tcb_t* list, tcb_t* p_tcb)
    {

    tcb_t* tcb;

    /* Ensure there are items in list */
    if (list == NULL)
        {
        list = p_tcb;
        }
    else
        {
        tcb = list;

        while (tcb->next != NULL)
            {
            tcb = tcb->next;
            }

        tcb->next = p_tcb;
        }

    p_tcb->next = NULL;

    /* Return always pointer to start of the list */
    return list;

    }


/**
Adds an item after the current one to linearly linked list

\param list List to where the item will be added
\param tcb Item to be added into list
*/
void single_add_after(tcb_t* list, tcb_t* tcb)
    {

    tcb->next = list->next;
    list->next = tcb;

    }


/**
Adds an item to first in linearly linked list

\param list List to where the item will be added
\param tcb Item to be added into list
\return Pointer to the start of the list
*/
tcb_t* single_add_first(tcb_t* list, tcb_t* tcb)
    {

    tcb->next = list;
    list = tcb;

    return list;

    }


/**
Adds an item to 'last' in circularly linked list

\param list List to where the item will be added
\param tcb Item to be added into list
\return Pointer to the start of the list
*/
tcb_t* double_add_before(tcb_t* list, tcb_t* tcb)
    {

    tcb_t* prev;
    tcb_t* current;

    if (list == NULL)
        {
        list = tcb;

        list->next = list;
        list->prev = list;
        }
    else
        {
        current = list;
        prev = list->prev;

        /* Add new task to last in this list i.e. one before current! */
        current->prev = tcb;
        prev->next = tcb;

        tcb->prev = prev;
        tcb->next = current;
        }

    return list;

    }


/**
Removes the current item from circularly linked list

\param list List from where the current item is about to be removed
\return Pointer to the start of the list
*/
tcb_t* double_remove(tcb_t* list)
    {

    tcb_t* prev;
    tcb_t* next;

    if (list == list->next)
        {
        list = NULL;
        }
    else
        {
        prev = list->prev;
        next = list->next;

        prev->next = next;
        next->prev = prev;

        list = next;
        }

    return list;

    }
