/*
Copyright (c) 2007-2008, Tomi Parviainen
*/

#include "includes.h"
#include <stdlib.h>


/**
Initialises circular buffer for use, must be called before buffer can be used.

\param buffer Buffer to be initialised
\param length The amount of 32-bit elements full buffer can contain
*/
void buffer_init(buffer_t* buffer, uint32_t length)
    {

    TR_BUF(TR_FUNCTION);
    ASSERT(length != 0);

    /* Need to keep one element unallocated i.e. full buffer is "length - 1"
       and empty buffer is when start and length point to same location */
    length++;

    /* \todo return value to indicate out of memory situations */
    buffer->buffer = malloc(sizeof(uint32_t) * length);
    ASSERT(buffer->buffer != NULL);

    buffer->length = length;
    buffer->start = 0;
    buffer->end = 0;

    }


/**
Adds a data to circular buffer. If the buffer is full when new data element is
added the oldest data is overwritten.

\note Overflow trace is printed in DEBUG builds
\param buffer Buffer to where the data will be added
\param data Data to be added to the buffer
*/
void buffer_add(buffer_t* buffer, any_t* data)
    {

    const uint32_t length = buffer->length;

    TR_BUF(TR_FUNCTION);

    buffer->buffer[buffer->end++] = (uint32_t)(uintptr_t)data;

    if (buffer->end == length)
        {
        buffer->end = 0;
        }

    /* "Overwrite" the old data if the buffer becomes full */
    if (buffer->end == buffer->start)
        {
#if defined DEBUG
        trace_print_hex("overwriting", buffer->buffer[buffer->start]);
#endif

        buffer->start++;

        if (buffer->start == length)
            {
            buffer->start = 0;
            }
        }

    }


/**
Removes the data from circular buffer. The data is removed in FIFO order i.e.
the oldest data is removed first.

\pre There must be data elements in buffer
\see buffer_empty
\param buffer The buffer from where the data is to be removed
*/
any_t* buffer_remove(buffer_t* buffer)
    {

    any_t* data;

    TR_BUF(TR_FUNCTION);
    ASSERT(buffer->start != buffer->end);

    data = (any_t*)(uintptr_t)buffer->buffer[buffer->start++];

    if (buffer->start == buffer->length)
        {
        buffer->start = 0;
        }

    return data;

    }


/**
Checks whether the buffer is empty or not.

\param buffer Buffer to be checked
\return TRUE if the buffer is empty, FALSE if there is data in the buffer
*/
boolean_t buffer_empty(const buffer_t* buffer)
    {

    TR_BUF(TR_FUNCTION);

    return (buffer->start == buffer->end);

    }
