/*
Copyright (c) 2007-2015, Tomi Parviainen
*/

#include "includes.h"
#include <stdlib.h>


/**
Initialises mailbox for use, must be called before mailbox can be used.

\param mbx Mailbox to be initialised
\param length The amount of 32-bit elements full mailbox can contain
*/
void mailbox_init(mailbox_t* mbx, uint32_t length)
    {

    TR_MBX(TR_FUNCTION);

    /* \todo return value to indicate out of memory situations */
    mbx->msg = malloc(sizeof(buffer_t));
    ASSERT(mbx->msg != NULL);

    mbx->list = NULL;

    buffer_init(mbx->msg, length);

    }


/**
Posts a message to mailbox. If the mailbox is full then the oldest data
is overwritten. If the waiting task has a higher priority than current task,
reschedule will be performed. Can be called from task or from IRQ context.

\note Overflow trace is printed in DEBUG builds if old data is overwritten
\param mbx Mailbox to where the message will be posted
\param msg Message to be posted into mailbox
*/
void mailbox_post(mailbox_t* mbx, any_t* msg)
    {

    tcb_t* tcb;
    uint32_t irq_bits;

    TR_MBX(TR_FUNCTION);
    TR_MBX(trace_print_hex("mbx", (uint32_t)mbx));
    TR_MBX(trace_print_hex("msg", (uint32_t)msg));
    ASSERT(mbx != NULL);

    irq_bits = irq_disable_core();

    buffer_add(mbx->msg, msg);

    /* Wake-up the sleeping task */
    if (mbx->list != NULL)
        {
        tcb = mbx->list;
        mbx->list = mbx->list->next;

        /* Reschedule if the priority of TCB is higher than current task's
           priority */
        task_add(tcb);
        }

    irq_enable_core(irq_bits);

    }


/**
Reads a message from mailbox, if the mailbox is empty the caller is blocked
until the message is posted into this mailbox.

\pre Must be called from task context
\param mbx Mailbox from where the message will be read
\return The message read from the mailbox
*/
any_t* mailbox_pend(mailbox_t* mbx)
    {

    tcb_t* tcb;
    any_t* msg;
    uint32_t irq_bits;

    TR_MBX(TR_FUNCTION);
    ASSERT(mbx != NULL);

    irq_bits = irq_disable_core();

    /* Sleep if there are no messages */
    if (buffer_empty(mbx->msg))
        {
        task_remove(g_tcb_current);

        /* Each task must have fresh time slice when added back to ready
           list */
        g_tcb_current->time_slice = TIME_SLICE;

        mbx->list = single_add_last(mbx->list, g_tcb_current);
        tcb = task_highest_ready();
        scheduler_context_switch(tcb);
        }

    /* Message arrived to mailbox, time to continue! */
    msg = buffer_remove(mbx->msg);

    irq_enable_core(irq_bits);

    TR_MBX(trace_print_hex("mbx", (uint32_t)mbx));
    TR_MBX(trace_print_hex("msg", (uint32_t)msg));

    return msg;

    }
