/*
Copyright (c) 2007-2015, Tomi Parviainen
*/

#include "includes.h"
#include <stdlib.h>


/*
Function prototype(s)
*/
static void task_kill(void);


/**
Creates a task and adds it automatically to the ready list of the specific
priority tasks. If the scheduler is up and running and created task's priority
is higher than currently executed task's priority then context switch will be
performed immediately.

\note The size of the stack is the amount of 'stack_t' elements

\param func Task's function to be called
\param param Parameter for task function
\param stk_size Size of the stack to be allocated for task
\param priority The priority of the task
\param name The name of the task

\return Created task's TCB
*/
tcb_t* task_create(task_fn func, any_t* param, uint16_t stk_size, uint8_t priority, const sint8_t* name)
    {

    tcb_t* tcb;
    stack_t* stk;
    uint32_t irq_bits;

    TR_TSK(TR_FUNCTION);
    ASSERT(priority<TASK_PRIORITY_COUNT);

    tcb = (tcb_t*)malloc(sizeof(tcb_t));

    if (tcb != NULL)
        {
        tcb->stk = (stack_t*)malloc(stk_size * sizeof(stack_t));

        if (tcb->stk == NULL)
            {
            free(tcb);
            tcb = NULL;
            }
        }

    if (tcb != NULL)
        {
#ifdef PROF_SP_1
        tcb->stk_size = stk_size * sizeof(stack_t);
        tcb->stk_base = (uint8_t*)tcb->stk;
        memory_set(tcb->stk, 0xA5, tcb->stk_size);
#endif /* PROF_SP */

        /* Stack grows from higher address to lower address
           i.e. the stack is full-descending */
        tcb->stk += (stk_size - 1);

        tcb->name = name;
        tcb->time_sleep = 0;
        tcb->time_slice = TIME_SLICE;
        tcb->priority = priority;
        tcb->state = (uint8_t)READY;

#ifdef ARM
        /* SR = Scratch Register. SR registers hold the first four function
           arguments on a function call and the return value on a function
           return. A function may corrupt SR registers and use them as general
           scratch registers within the function.

           VR = Variable Register. The function must preserve the callee
           values of these registers. */
        stk = tcb->stk;
        *(stk--) = (stack_t)func;       /* R15 (PC) Program counter */
        *(stk--) = (stack_t)task_kill;  /* R14 (LR) Link register. On a 
                                                    function call this holds
                                                    the return address. */
        *(stk--) = (stack_t)0xcccccccc; /* R12 (IP) Intra-procedure call, SR */
        *(stk--) = (stack_t)0xbbbbbbbb; /* R11 (v8) VR 8 */
        *(stk--) = (stack_t)0xaaaaaaaa; /* R10 (v7) VR 7 */
        *(stk--) = (stack_t)0x99999999; /*  R9 (v6) VR 6, Platform specific:
                                                    SB - static base, 
                                                    TR - thread register */
        *(stk--) = (stack_t)0x88888888; /*  R8 (v5) VR 5 */
        *(stk--) = (stack_t)0x77777777; /*  R7 (v4) VR 4 */
        *(stk--) = (stack_t)0x66666666; /*  R6 (v3) VR 3 */
        *(stk--) = (stack_t)0x55555555; /*  R5 (v2) VR 2 */
        *(stk--) = (stack_t)0x44444444; /*  R4 (v1) VR 1 */
        *(stk--) = (stack_t)0x33333333; /*  R3 (a4) Argument / SR 4 */
        *(stk--) = (stack_t)0x22222222; /*  R2 (a3) Argument / SR 3 */
        *(stk--) = (stack_t)0x11111111; /*  R1 (a2) Argument / result / SR 2 */
        *(stk--) = (stack_t)param;      /*  R0 (a1) Argument / result / SR 1 */
        *(stk  ) = (stack_t)0x00000013; /* CPSR - SVC mode, IRQ/FIQ enabled */
        tcb->stk = stk;
#else // AVR
        /* Function call conventions:
           - Arguments: allocated left to right, r25 to r8
           - Return values: 8-bit in r24 (not r25!), 16-bit in r25:r24,
             up to 32 bits in r22-r25, up to 64 bits in r18-r25.

           FR = Fixed Registers. Never allocated by GCC for local data.

           CSR = Call-Saved Registers. May be allocated by GCC for local data.
                 Calling C subroutines leaves them unchanged. Assembler
                 subroutines are responsible for saving and restoring these
                 registers, if changed. r29:r28 (Y pointer) is used as a frame
                 pointer (points to local data on stack) if necessary.

           CUR = Call-Used Registers. May be allocated by GCC for local data.
                 You may use them freely in assembler subroutines. Calling C
                 subroutines can clobber any of them - the caller is 
                 responsible for saving and restoring. */
        stk = tcb->stk;

        // In case task function returns, safety mechanism!
        *(stk--) = (stack_t)((int)task_kill >> 0);
        *(stk--) = (stack_t)((int)task_kill >> 8);

        // Task function to execute, this should never return!
        *(stk--) = (stack_t)((int)func >> 0);
        *(stk--) = (stack_t)((int)func >> 8);

        *(stk--) = (stack_t)0x31;   /* R31 CUR, Indirect Address Register Z */
        *(stk--) = (stack_t)0x30;   /* R30 CUR, Indirect Address Register Z */
        *(stk--) = (stack_t)0x29;   /* R29 CSR, Indirect Address Register Y */
        *(stk--) = (stack_t)0x28;   /* R28 CSR, Indirect Address Register Y */
        *(stk--) = (stack_t)0x27;   /* R27 CUR, Indirect Address Register X */
        *(stk--) = (stack_t)0x26;   /* R26 CUR, Indirect Address Register X */
        *(stk--) = (stack_t)0x25;   /* R25 CUR, Argument / return value */
        *(stk--) = (stack_t)0x24;   /* R24 CUR, Argument / return value */
        *(stk--) = (stack_t)0x23;   /* R23 CUR, Argument / return value */
        *(stk--) = (stack_t)0x22;   /* R22 CUR, Argument / return value */
        *(stk--) = (stack_t)0x21;   /* R21 CUR, Argument / return value */
        *(stk--) = (stack_t)0x20;   /* R20 CUR, Argument / return value */
        *(stk--) = (stack_t)0x19;   /* R19 CUR, Argument / return value */
        *(stk--) = (stack_t)0x18;   /* R18 CUR, Argument / return value */
        *(stk--) = (stack_t)0x17;   /* R17 CSR, Argument */
        *(stk--) = (stack_t)0x16;   /* R16 CSR, Argument */
        *(stk--) = (stack_t)0x15;   /* R15 CSR, Argument */
        *(stk--) = (stack_t)0x14;   /* R14 CSR, Argument */
        *(stk--) = (stack_t)0x13;   /* R13 CSR, Argument */
        *(stk--) = (stack_t)0x12;   /* R12 CSR, Argument */
        *(stk--) = (stack_t)0x11;   /* R11 CSR, Argument */
        *(stk--) = (stack_t)0x10;   /* R10 CSR, Argument */
        *(stk--) = (stack_t)0x09;   /*  R9 CSR, Argument */
        *(stk--) = (stack_t)0x08;   /*  R8 CSR, Argument */
        *(stk--) = (stack_t)0x07;   /*  R7 CSR */
        *(stk--) = (stack_t)0x06;   /*  R6 CSR */
        *(stk--) = (stack_t)0x05;   /*  R5 CSR */
        *(stk--) = (stack_t)0x04;   /*  R4 CSR */
        *(stk--) = (stack_t)0x03;   /*  R3 CSR */
        *(stk--) = (stack_t)0x02;   /*  R2 CSR */
        *(stk--) = (stack_t)0x00;   /*  R1 FR, always contains zero */
        *(stk--) = (stack_t)0x00;   /*  R0 FR, temporary register */
        *(stk--) = (stack_t)0x00;   /* SREG – AVR Status Register */
        tcb->stk = stk;
#endif
        irq_bits = irq_disable_core();

        g_tcb_ready[priority] = double_add_before(g_tcb_ready[priority], tcb);

        TR_TSK(trace_print_tcb(tcb));

        /* If the scheduler is up and running, reschedule might be needed */
        if (g_tcb_current != NULL)
            {
            if (g_tcb_current->priority < priority)
                {
                scheduler_reschedule(tcb);
                }
            }

        irq_enable_core(irq_bits);
        }

    return tcb;

    }


/**
Blocks the current task and makes next ready task with the same priority
active. If there is no new tasks with same priority, time slice of current
task is resetted and it continues execution.
*/
void task_yield()
    {

    tcb_t* tcb;
    uint32_t irq_bits;

    TR_TSK(TR_FUNCTION);

    irq_bits = irq_disable_core();

    g_tcb_current->time_slice = TIME_SLICE;
    tcb = g_tcb_current->next;

    if (tcb != g_tcb_current)
        {
        g_tcb_ready[tcb->priority] = tcb;
        scheduler_context_switch(tcb);
        }

    irq_enable_core(irq_bits);

    }


/**
Adds the task into the list of tasks with the same priority, if added task
has higher priority than currently executing task then reschedule will be
initiated.

\param tcb Task to be added into ready list
*/
void task_add(tcb_t* tcb)
    {

    uint8_t priority;

    ASSERT(tcb != NULL);
    ASSERT(tcb->time_sleep == 0);
    ASSERT(tcb->time_slice == TIME_SLICE);

    priority = tcb->priority;
    g_tcb_ready[priority] = double_add_before(g_tcb_ready[priority], tcb);

    if (g_tcb_current->priority < priority)
        {
        scheduler_reschedule(tcb);
        }

    }


/**
Removes the task from the ready list

\param tcb Task to be removed from ready list
*/
void task_remove(tcb_t* tcb)
    {

    uint8_t priority;

    ASSERT(tcb != NULL);
    ASSERT(g_tcb_ready[tcb->priority] != NULL);

    priority = tcb->priority;

    g_tcb_ready[priority] = double_remove(tcb);

    }


/**
Changes the state of the task

\param state New state for current task
*/
void task_set_state(task_state state)
    {

    tcb_t* tcb;

    ASSERT(g_tcb_current != NULL);

    g_tcb_current->state = (uint8_t)state;

    tcb = task_highest_ready();

    scheduler_context_switch(tcb);

    }


/**
Finds the highest priority task ready to run

\todo Can be simplified if ready list contains only tasks with READY state
\todo RTOS = predictable, change algorithm to be predictable (ARM-CLZ ARMv5T)

\return Pointer to the TCB of the highest priority task ready to run
*/
tcb_t* task_highest_ready()
    {

    tcb_t* highest;
    tcb_t* current;
    uint8_t priority;

    TR_TSK(TR_FUNCTION);

    priority = TASK_PRIORITY_COUNT;

    /* Find the highest priority task ready to run */
    while (priority > 0)
        {
        priority--;

        current = g_tcb_ready[priority];
        highest = current;

        while (current != NULL)
            {
            if (current->state == (uint8_t)READY)
                {
                highest = current;
                current = NULL;
                priority = 0;
                }
            else
                {
                current = current->next;

                /* Check if circular buffer went through */
                if (current == highest)
                    {
                    highest = NULL;
                    current = NULL;
                    }
                }
            }
        }

    /* There should always be at least NULL task ready to be executed */
    ASSERT(highest != NULL);

    return highest;

    }


/**
Suspends the current task for a specified time. The sleep time is always at
least the requested amount of milliseconds. Because of the resolution the
sleep time might be milliseconds + 1 timer tick.

\param milliseconds The sleep time in milliseconds
*/
void task_sleep(uint32_t milliseconds)
    {

    tcb_t* tcb;
    tcb_t* tcb_prev;
    uint8_t priority;
    uint32_t irq_bits;

    /* In order to cause delay, which is at least requested amount of ms */
    milliseconds++;

    priority = g_tcb_current->priority;

    irq_bits = irq_disable_core();

    g_tcb_ready[priority] = double_remove(g_tcb_current);

    if (g_tcb_sleep == NULL)
        {
        g_tcb_sleep = g_tcb_current;
        g_tcb_sleep->next = NULL;
        }
    else
        {
        tcb_prev = NULL;
        tcb = g_tcb_sleep;

        /* Find the location where new task will be put in sleep list. The
           time to sleep will be relative to other tasks in sleep list. */
        while (tcb != NULL)
            {
            if (milliseconds >= tcb->time_sleep)
                {
                milliseconds -= tcb->time_sleep;
                tcb_prev = tcb;
                tcb = tcb->next;

                if (tcb == NULL)
                    {
                    /* tcb_prev was last task in sleep list */
                    single_add_after(tcb_prev, g_tcb_current);
                    }
                }
            else
                {
                /* Correct the sleep time of next task, because the sleep time
                   for that task is now 'millisecods' less because this task
                   is in front of it */
                tcb->time_sleep -= milliseconds;

                if (tcb_prev == NULL)
                    {
                    /* First item in the list */
                    g_tcb_sleep = single_add_first(tcb, g_tcb_current);
                    }
                else
                    {
                    /* New task will be put in the middle of sleep list */
                    single_add_after(tcb_prev, g_tcb_current);
                    }

                tcb = NULL;
                }
            }
        }

    g_tcb_current->time_sleep = milliseconds;
    g_tcb_current->time_slice = TIME_SLICE;

    tcb = task_highest_ready();

    scheduler_context_switch(tcb);

    irq_enable_core(irq_bits);

    }


/*
Kills the currently executing task ...
*/
void task_kill(void)
    {

    /* \todo task_kill */

    /* Who's STACK to use ??? Needs to be switched ... One option is to defer
       task deletion into IDLE task, which would use it's own STACK for all
       of the operations */

    /* Remove task from READY list */

    /* Release the memory allocated for TCB structures */

    /* Find the highest task ready to RUN */

    TR_TSK(trace_print_tcb(g_tcb_current));

    ASSERT(0);

    }
