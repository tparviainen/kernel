/*
Copyright (c) 2007-2009, Tomi Parviainen
*/

#include "includes.h"


/**
Initialises semaphore for use, must be called before semaphore can be used.

\param sem Semaphore to be initialised
\param count Non-negative initial value for the semaphore
*/
void semaphore_init(semaphore_t* sem, sint32_t count)
    {

    TR_SEM(TR_FUNCTION);
    ASSERT(sem != NULL);
    ASSERT(count >= 0);

    sem->list = NULL;
    sem->count = count;

    }


/**
Decrements the semaphore value, if the value becomes negative then the task
executing the wait is blocked.

\pre Must be called from task context
\param sem Semaphore to be decremented
*/
void semaphore_wait(semaphore_t* sem)
    {

    tcb_t* tcb;
    uint32_t irq_bits;

    TR_SEM(TR_FUNCTION);
    ASSERT(sem != NULL);

    irq_bits = irq_disable_core();

    sem->count--;

    if (sem->count < 0)
        {
        task_remove(g_tcb_current);

        /* Each task must have fresh time slice when added back to ready
           list */
        g_tcb_current->time_slice = TIME_SLICE;

        sem->list = single_add_last(sem->list, g_tcb_current);
        tcb = task_highest_ready();
        scheduler_context_switch(tcb);
        }

    irq_enable_core(irq_bits);

    }


/**
Increments the semaphore value, if the value is not positive then the
previously blocked task is unblocked.

\param sem Semaphore to be incremented
*/
void semaphore_signal(semaphore_t* sem)
    {

    tcb_t* tcb;
    uint32_t irq_bits;

    TR_SEM(TR_FUNCTION);
    ASSERT(sem != NULL);

    irq_bits = irq_disable_core();

    sem->count++;

    if (sem->count <= 0)
        {
        tcb = sem->list;
        sem->list = sem->list->next;

        /* Reschedule if the priority of tcb is higher than current task's
           priority */
        task_add(tcb);
        }

    irq_enable_core(irq_bits);

    }
